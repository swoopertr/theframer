exports.TheHeaderJson ={
    'Content-Type':'application/json; charset=utf-8',
    'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'

} ;
exports.TheHeaderXml = {
    'Content-Type':'application/xml',
    'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'
};
exports.TheHeaderHtml = {
  'Content-Type':'text/html',
  'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'
};
exports.TheHeaderCss = {
  'Content-Type':'text/css',
  'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'
};
exports.TheHeaderJavascript = {
  'Content-Type':'text/javascript',
  'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'
};
exports.TheHeaderPNG = {
  'Content-Type':'image/png',
  'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'
};
exports.TheHeaderJPG = {
  'Content-Type':'image/jpg',
  'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'
};
exports.TheHeaderJPEG = {
  'Content-Type':'image/jpeg',
  'X-Pd-By':'tuncking',
  'Accept-Ranges': 'bytes',
  'Cache-Control': 'private, max-age=0, no-cache, no-store'
};
exports.TheHeaderXLSX = {
    'Content-Type':'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
    'X-Pd-By':'tuncking',
    'Accept-Ranges': 'bytes',
    'Cache-Control': 'private, max-age=0, no-cache, no-store'
};

//Port section
exports.ServerPort = 8090;
exports.sessionExpireTime = 1800; //30 mins.
exports.pagingCount = 25;

//postgre configuration section
exports.postGreDb= {
  user:   'postgres',
  pass:   '1234',
  //ip:     '172.17.16.141',//laptop
  // ip:     '10.10.42.95',//mac
	//ip:     '192.168.0.18',//home
  ip:     'localhost',//desktop,mac
  //port:   '6000',//laptop
  port:   '5432',//home, desktop, mac
  dbname: 'sendika'
};
exports.PostGreConnection = 'postgres://'+this.postGreDb.user+':'+this.postGreDb.pass + '@' + this.postGreDb.ip +':'+this.postGreDb.port+'/'+this.postGreDb.dbname ;
exports.PostGre ={
    Query : {
    GetAllTables : 'SELECT table_schema, table_name FROM information_schema.tables;',
    createBackup: ''//todo: make back up query
  }
};

exports.email = {//todo: use their server and account
  from:'mailcikiral@gmail.com',
  host:'smtp.gmail.com',
  port:465,//587,//465,
  secure:false,
  auth:{
    user:'mailcikiral@gmail.com',
    pass:'bratislava'
  }
};

exports.Redis={
    IP:'localhost',
    Port:6379
};

//exports.excelFolder = '/home/sendika/eSendika/client/download/';
exports.excelFolder = '/Volumes/Macintosh HD/Dev/Dev/Free/theframer/client/download/';
exports.virtualExcelFolder = 'client/download/';

exports.proj = {
    Title: 'Birleşik Metal İş Sendika Otomasyonu',
    //Path:   '/home/sendika/eSendika/' //ubuntu enviroment
    //Path: 'D:\\Development\\nodejs\\theframer\\theframer' //windows enviroment
    Path: '/Volumes/Macintosh HD/Dev/Dev/Free/theframer/' //mac enviroment
};
var user ='sendika';
var password ='disk918273';
exports.ngrokUser= {
    username :user,
    password : password
};
exports.ngrok={
    proto: 'http',
    authtoken:'mH8nRa5QSZ6oYKMKTCm6_71rfa4GXzz4sDA2yD2ekb',
    auth:user+':'+password,
    addr:'8090',
    region: 'eu'
};

exports.ngmails=[
    'tunc.kiral@gmail.com',
    'ileri.suleyman17@gmail.com',
    'nyurtsewer@gmail.com'
];