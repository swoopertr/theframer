var work = require('./../bussines/sendika');

var sendika = {
    sendikalar : function (req, res) {
        work.sendikalar(req, res);
    },
    sendikadetay: function (req, res) {
        work.sendikadetay(req, res);
    },
    postsendikadetay: function (req, res) {
        work.postsendikadetay(req, res);
    },
    sendikatespit: function (req, res) {
        work.sendikatespit(req, res);
    },
    sendikatespitajax: function (req, res) {
        work.sendikatespitajax(req, res);
    },
    sendikaisyeriCountAjax: function (req, res) {
        work.sendikaisyeriCountAjax(req, res);
    },
    sendikaDeleteAjax: function (req, res) {
        work.sendikaDeleteAjax(req, res);
    }
};
module.exports = sendika;