var resetCache = {
  resetServerCache:function (req, res) {
    global.cache.resetServer(function () {
      res.end('Cache server reset!');
    });
  },
  refillServerCache:function (req, res) {
    global.cache.resetServer(function () {
      var tengine = require('./../middleware/templating/engi');
      tengine.init(function () {
        res.end('cleaned and refilled cache');
      });
    });
  }
};
module.exports = resetCache;