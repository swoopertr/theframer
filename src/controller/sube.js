var subeManager = require('./../DataAccess/sube');
var workobj = require('./../bussines/sube');

var sube = {
    subeListesi: function (req, res) {
        global.core.parseReq(req, function (reqData) {
            if (reqData.hasOwnProperty('detaysube')) {
                workobj.subeDetay(req, res, reqData);
            } else if (reqData.hasOwnProperty('editsube')) {
                workobj.subeEdit(req, res, reqData);
            } else if (reqData.hasOwnProperty('isyerleri')) {
                workobj.isyeri(req, res);
            } else {
                workobj.subeListesi(req, res);
            }
        });
    },
    subedetaypost: function (req, res) {
        req.on('end', function () {
            var fields = ['subeid', 'isim', 'telefonno', 'adres', 'description'];
            global.core.getFormData(req.formData, fields, function (data) {
                subeManager.save(data, function (result) {
                    var usr = req.sess.get('user');
                    //global.writeLog('Şube güncellendi : ' + JSON.stringify(data), usr.userid);
                    var subeDetayManager = require('./../DataAccess/subeDetay');
                    data.subeid = JSON.parse(result).insupdsube;
                    subeDetayManager.save(data, function (a) {//meaningless a variable :)
                        global.core.redirect(res, '/sube?editsube=' + data.subeid);
                    });
                });
            });
        });
    },
    subeIstatistik: function (req, res) {
        workobj.istatistik(req, res);
    },
    subeAjax: function (req, res) {
        workobj.requestAjax(req, res);
    },
    subeyonetim: function (req, res) {
        workobj.subeyonetim(req, res);
    },
    subeyonetimedit: function (req, res) {
        workobj.subeyonetimedit(req, res);
    },
    subeyonetimeditpost: function (req, res) {
        req.on('end', function () {
            var fields = ['subeteskilatid','subeid', 'uyeid','vazifeSelect'];
            global.core.getFormData(req.formData, fields, function (data) {

                if (data.subeteskilatid == 0){ // insert
                    subeManager.yonetimInsert(data,function (result) {
                        if(!result.hasOwnProperty('err')){
                            global.core.redirect(res, 'subeyonetim?subeid='+data.subeid);
                        }
                    });
                }else { // update
                    subeManager.yonetimUpdate(data,function (result) {
                        if(!result.hasOwnProperty('err')){
                            global.core.redirect(res, 'subeyonetim?subeid='+data.subeid);
                        }
                    });
                }
            });
        });
    },

    subeyonetimisil: function (req, res) {
        req.on('end', function () {
            var fields = ['subeteskilatiid'];
            global.core.getFormData(req.formData, fields, function (data) {

                subeManager.yonetimsil(data.subeteskilatiid,function (result) {
                    if (!result.hasOwnProperty('err')){
                        global.core.returnJson(res, {status:'ok'});
                    }
                });
            });
        });
    },
    subegiriscikis: function (req, res) {
        workobj.subegiriscikis(req, res);
    },
    subeAjaxSubegiriscikis: function (req, res) {
        workobj.subeAjaxSubegiriscikis(req, res);
    },
    giriscikisisimli: function (req, res) {
        workobj.giriscikisisimli(req, res);
    },
    subeAjaxSubegiriscikisisimli: function (req, res) {
        workobj.subeAjaxSubegiriscikisisimli(req, res);
    },
    subetespit: function (req, res) {
        workobj.subetespit(req, res);
    },
    subeAjaxtespit: function (req, res) {
        workobj.subeAjaxtespit(req, res);
    },
    subesendika: function (req, res) {
        workobj.subesendika(req, res);
    },
    subetemsilciler: function (req, res) {
        workobj.subetemsilciler(req, res);
    },
    subeuyelistesi: function (req, res) {
        workobj.subeuyelistesi(req, res);
    },
    ajaxsubeuyelistesi: function (req, res) {
        workobj.ajaxsubeuyelistesi(req, res);
    },
    ajaxdeletesube: function (req, res) {
        workobj.ajaxdeletesube(req, res);
    }
};

module.exports = sube;