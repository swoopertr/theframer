var tengine = require('./../middleware/templating/engi');
var userData = require('./../DataAccess/user');
var loginController = {};

loginController.checklogin = function(req,res){
  var isLogin = req.sess.get('isLogin');
  if(isLogin === true){
    global.core.redirect(res, '/anasayfa');
  }else{
    tengine.render(req, res, global.temps.login, function () {});
  }
};

loginController.checkloginpost = function(req, res){
  req.on('end',function () {
    var fields = ['email','password'];
    global.core.getFormData(req.formData, fields, function (data) {
      userData.checkUser(data[fields[0]],data[fields[1]], function (user) {
        if(user){
          if(user.isActive === 0 && typeof user.isActive !== "undefined") {
            global.core.redirect(res, '/login?error=2');
            //global.writeLog("Login başarısız, kullanıcı pasif durumda", user.userid);
          }else{
            req.sess.set('isLogin', true, function (result) {});
            req.sess.set('user', user, function(result){});
            //global.writeLog('login oldu, anasayfaya yönleniyor.', user.userid);
            global.core.redirect(res, '/anasayfa');
          }
        }
        else{
          global.core.redirect(res, '/login?error=1');
	        //global.writeLog("login başarısız", 0);
        }
      });
    });
  });
};

loginController.sendMailPassword = function (req, res) {
  global.core.parseReq(req, function (result) {
    if(typeof result.email !== "undefined"){
      var email = result.email;
      var userManager = require('./../DataAccess/user');
      userManager.getByEmail(email,function (user) {
        if(user === null){
          res.end('0');
        }else {
          global.core.sendMailer(email, 'Şifre gönerim', 'mail adresine bağlı şifreniz : ' + user.passwrd);
          res.end('1');
        }
      });
    }
  });
};

loginController.logout = function (req, res) {
  req.sess.remove('isLogin');
  req.sess.remove('user');
  global.core.redirect(res, "/login");
};

module.exports = loginController;