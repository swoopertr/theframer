var hareketBussnes = require('./../bussines/hareket');
var hareketManager = require('./../DataAccess/hareket');
var uyeManager = require('./../DataAccess/uye');

var hareket = {
    getHareket: function (req, res) {
        hareketBussnes.getHareket(req, res);
    },
    postHareketDetay: function (req, res) {
        req.on('end', function () {
            var fields = ['uyeid', 'hareketid', 'tarih', 'saat', 'isyeriDom',
                'kararno', 'hareketSelect', 'noter', 'noterTarih', 'noterYevmiye',
                'kayitSiraNo', 'dogrulamaKodu'];
            global.core.getFormData(req.formData, fields, function (data) {
                if (data.hareketid === "0") {
                    hareketManager.insertHareket(data.uyeid, data.hareketSelect, data.tarih, data.kararno,
                        data.isyeriDom, data.noter, data.noterTarih, data.noterYevmiye, null,
                        null, null, null, data.kayitSiraNo, data.dogrulamaKodu, data.saat, function () {
                            uyeManager.updateSonHareket(data.uyeid, function () {
                                global.core.redirect(res, "/uyedetay?uyeid=" + data.uyeid);
                            });
                        });
                } else {
                    hareketManager.updateHareket(data.hareketid, data.uyeid, data.hareketSelect, data.tarih,
                        data.kararno, data.isyeriDom, data.noter, data.noterTarih, data.noterYevmiye,
                        data.kayitSiraNo, data.dogrulamaKodu, data.saat, function () {
                            uyeManager.updateSonHareket(data.uyeid, function () {
                                global.core.redirect(res, "/uyedetay?uyeid=" + data.uyeid);
                            });
                        });
                }

            });

        });
    },
    postAjaxHareketDetay: function (req, res) {
        req.on('end', function () {
            var fields = ['uyeid', 'hareketid', 'tarih', 'saat', 'isyeriDom',
                'kararno', 'hareketSelect', 'noter', 'noterTarih', 'noterYevmiye',
                'kayitSiraNo', 'dogrulamaKodu', 'aciklama'];
            global.core.getFormData(req.formData, fields, function (data) {
                if (data.hareketid === "0") {
                    hareketManager.insertHareket(data.uyeid, data.hareketSelect, data.tarih, data.kararno,
                        data.isyeriDom, data.noter, data.noterTarih, data.noterYevmiye, null,
                        null, null, null, data.kayitSiraNo, data.dogrulamaKodu, data.saat, data.aciklama, function (hareketid) {
                            if (hareketid[0]) {
                                hareketid = JSON.parse(hareketid[0]).hareketid;
                                global.core.returnJson(res, {status: 'ok', hareketid: hareketid});
                            } else {
                                global.core.returnJson(res, {status: 'ok'});
                            }
                        });
                } else {
                    hareketManager.updateHareket(data.hareketid, data.uyeid, data.hareketSelect, data.tarih,
                        data.kararno, data.isyeriDom, data.noter, data.noterTarih, data.noterYevmiye,
                        data.kayitSiraNo, data.dogrulamaKodu, data.saat, data.aciklama, function () {
                            global.core.returnJson(res, {status: 'ok', hareketid: data.hareketid});
                        });
                }

            });

        });
    },
    checkforclevel: function (req, res) {
        hareketBussnes.checkforclevel(req, res);
    },
    checkforclevelsube: function (req, res) {
        hareketBussnes.checkforclevelsube(req, res);
    },
    deletemovementhareketid: function (req, res) {
        hareketBussnes.deletemovementhareketid(req, res);
    }

};


module.exports = hareket;