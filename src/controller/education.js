var work = require('./../bussines/education');
var educationManager = require('./../DataAccess/education');


var education = {
    main: function (req, res) {
        work.main(req, res);
    },
    educationTypes: function (req, res) {
        work.listeducationTypes(req, res);
    },
    editeducationCategory: function (req, res) {
        work.editeducationCategory(req, res);
    },
    egitimTipiDetay:function(req, res){
        work.egitimTipiDetay(req, res);
    },
    educationList: function (req, res){
        work.educationList(req, res);
    },
    teachers: function (req, res){
        work.teachers(req, res);
    },
    posteducationCategory: function (req, res) {
        req.on('end', function () {
            var fields = ['uyeid', 'hareketid', 'tarih', 'saat', 'isyeriDom',
                'kararno', 'hareketSelect', 'noter', 'noterTarih', 'noterYevmiye',
                'kayitSiraNo', 'dogrulamaKodu'];

            global.core.getFormData(req.formData, fields, function (data) {
                if (data.educationCategoryId === "0") {
                    educationManager.insert(data, function () {
                        global.core.redirect(res, "/uyedetay?uyeid=" + data.uyeid);
                    });
                } else {
                    educationManager.update(data, function () {
                        global.core.redirect(res, "/uyedetay?uyeid=" + data.uyeid);
                    });
                }

            });
        });
    },
    educationTeacherDetail: function (req, res){
        work.educationTeacherDetail(req, res);
    },
    educationTeacherDetailPost: function (req, res){
        req.on('end', function () {
            var fields = ['id','isim', 'soyad','telefon', 'tcid', 'statusSelect', 'description', 'statusEducatorType'];
            global.core.getFormData(req.formData, fields, function (data) {
                var tmp = {
                    Id: data.id,
                    Name: data.isim,
                    Surname: data.soyad,
                    TcNo: data.tcid,
                    Status: data.statusSelect,
                    Phone: data.telefon,
                    Description: data.description,
                    EducatorType : data.statusEducatorType
                };
                educationManager.teacher.upsertTeacher(tmp, function (result){
                    global.core.redirect(res, 'egitimcilistesi');
                });
            });
        });
    },
    educationTeacherDelete: function (req, res){
        req.on('end', function () {
            var fields = ['id'];
            global.core.getFormData(req.formData, fields, function (data) {
                educationManager.teacher.deleteTeacher(data.id, function (result){
                    global.core.returnJson(res, {status:"ok"});
                });
            });
        });
    },
    postEditeducationCategory: function (req, res){
        req.on('end', function () {
            var fields = ['id', 'eduName', 'statusSelect', 'selectRoof','description'];
            global.core.getFormData(req.formData, fields, function (data) {
                educationManager.types.upsert(data, function (result){
                    global.core.redirect(res, 'egitimtipleri');
                });
            });
        });
    },
    educationItemDetail: function (req, res) {
        work.educationItemDetail(req, res);
    },
    educationCreate: function (req, res){
        work.educationCreate(req, res);
    },
    educationAddCalendarAjax: function(req, res){
        req.on('end', function () {
            global.core.getFormData(req.formData, 'all', function (data) {
                //todo insert db
                educationManager.educationClass.upsertClass()
                console.log('formData : ', data);
            });
        
        });
    }
};

module.exports = education;