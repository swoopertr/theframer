var tengine = require('./../middleware/templating/engi');
var homepageController = {};

//var html = html.replace(new RegExp('', g),);
var renderHomePage = function (req, res, user) {
  var defTemp = global.temps.homepage;
  //username placed
  var html = defTemp.replace(new RegExp('{{obj.user.name}}', 'g'), user.firstname +' '+ user.lastname);
  var userManager = require('./../DataAccess/user');
  userManager.getPermissions(user.userid,function (perms) {
    var htmlMenuItems = '';
    for (var i =0 ; i< perms.length ; i++){
      var menuItem = JSON.parse(perms[i]);
      htmlMenuItems+= "<li><a href='" +menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>"+menuItem.menuname+"</span></a></li>";
    }
    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
    tengine.render(req,res,html);
  });
};

//homepage
homepageController.main = function(req, res){
  try {
    var userData =req.sess.get("user");
    var isLogin = req.sess.get("isLogin");

    if(userData === null) {
      global.core.redirect(res,"/login");
    }else {
      if(isLogin === true) {
        renderHomePage(req, res, userData);
      }else {
        global.core.redirect(res,"/login");
      }
    }

  }catch (err){
    console.log(err, req.sess.user.userid);
    //global.writeErr(err,req.sess.user.userid);
  }
};
module.exports = homepageController;