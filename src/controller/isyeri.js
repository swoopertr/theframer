var isyeriManager = require('./../DataAccess/isyeri');
var temsilciManager = require('./../DataAccess/temsilci');
var work = require('./../bussines/isyeri');
var isyeri = {
    main: function (req, res) {
        work.isyeriAllList(req, res);
    },
    isyeriAjaxListCount: function(req, res){
        work.isyeriAjaxListCount(req, res);
    },
    isyeriAjaxlist:function(req, res){
        work.isyeriAjaxList(req, res);
    },
    isyeriAjaxTotalCount:function (req, res) {
        work.isyeriAjaxTotalCount(req, res);
    },
    isyeriAjaxTotalPagingCount:function (req, res) {
        work.isyeriAjaxTotalPagingCount(req, res);
    },
    isyeriAjaxPagingData:function (req, res) {
        work.isyeriAjaxPagingData(req, res);
    },
    isyeridetay: function (req, res) {
        work.isyeriDetay(req, res);
    },
    isyeriedit: function (req, res) {
        work.isyeriEdit(req, res);
    },
    istatistik:function (req, res) {
        work.istatistik(req, res);
    },
    isyerigiriscikis:function (req, res) {
        work.isyerigiriscikis(req, res);
    },
    isyeriAjaxisyerigiriscikis:function (req, res) {
        work.isyeriAjaxisyerigiriscikis(req, res);
    },
    isyerigiriscikisisimli:function (req, res) {
        work.isyerigiriscikisisimli(req, res);
    },
    isyeriAjaxgirisCikisIsimli:function (req, res) {
      work.isyeriAjaxgirisCikisIsimli(req, res);
    },
    uyelistesi:function (req, res) {
        work.uyelistesi(req, res);
    },
    isyeritemsilci:function(req, res){
        work.isyeritemsilci(req, res);
    },
    isyeriorgut: function (req, res) {
        work.isyeriorgut(req, res);
    },
    isyeritespit:function (req, res) {
        work.isyeritespit(req, res);
    },
    isyeriAjaxtespit: function (req, res) {
        work.isyeriAjaxtespit(req ,res);
    },
    isyeritemsilciEdit: function(req, res){
        work.isyeritemsilciEdit(req, res);
    },
    isyeriByIdAjax: function(req, res){
        work.isyeriByIdAjax(req, res);
    },
    isyeriisverentemsilcileri: function (req, res) {
        work.isyeriisverentemsilcileri(req, res);
    },
    isyeriAjaxDelete: function (req, res) {
        work.isyeriAjaxDelete(req, res);
    },
    checksgkforisyeri:function (req, res) {
        work.checkBcmDosyanoForIsyeri(req, res);
    },
    isyeritemsilciSilPost:function(req, res){
        req.on('end',function () {
            var fields=['temsilciid'];
            global.core.getFormData(req.formData, fields, function (data) {
                try {
                    temsilciManager.deleteTemsilci(data.temsilciid, function () {
                        res.end('OK');
                    });
                } catch (e) {
                    console.log("error : "+ e);
                }
            });
        });
    },
    isyeritemsilciEklePost:function (req, res) {
        req.on('end', function () {
            var fields = ['temsilciSira','calisanSelect','vazifeSelect','isyeriid','temsilciid','temsilciSira', 'uyeid', 'secimTarihi', 'atamaTarihi' ,'temsilciStatusu'];

            global.core.getFormData(req.formData, fields, function (data) {
                if (data.temsilciid === "0"){
                    temsilciManager.insertTemsilci(data.uyeid, data.isyeriid, data.vazifeSelect, data.temsilciSira, data.secimTarihi, data.atamaTarihi, data.temsilciStatusu, function (){
                        global.core.redirect(res,'/isyeritemsilci?isyeriid='+data.isyeriid);
                    });
                }else{
                    temsilciManager.updateTemsilci(data.temsilciid, data.uyeid, data.isyeriid, data.vazifeSelect, data.temsilciSira, data.secimTarihi, data.atamaTarihi, data.temsilciStatusu, function () {
                        global.core.redirect(res,'/isyeritemsilci?isyeriid='+data.isyeriid);
                    });
                }
            });
        });
    },
    isyeridetaypost: function (req, res) {
        req.on('end', function () {
            var fields = ['isyeriid', 'isim', 'adres', 'sendikaSelect', 'selectSube', 'selectKamu',
                'selectIsverenSendika', 'selectGrpDahilmi', 'selectOrgut', 'telefonno',
                'selectTaseron', 'bcmdosyano', 'sskno', 'tisBaslangic', 'tisBitis', 'tisImza',
            'ustkurulus', 'uretimkonusu', 'calisansayisi', 'sendikaSelect','email','faxno'];

            global.core.getFormData(req.formData, fields, function (data) {
                data.selectIsverenSendika = data.selectIsverenSendika == '0'?'': data.selectIsverenSendika;
                if (data.isyeriid =="0"){
                    isyeriManager.save(data, function (result) {
                        if(result.msg =="error"){
                            global.core.redirect(res,'hata');
                        }else {
                            global.console.log('isyeri inserted redirect...');
                            global.core.redirect(res,'isyeri?isyeriid=' + JSON.parse(result[0]).isyeriid);
                        }
                    });
                }else{
                    isyeriManager.update(data, function (result) {
                        if(result.msg == 'error'){
                            global.core.redirect(res,'hata');
                        }else{
                            global.console.log('isyeri updated redirect...');
                            global.core.redirect(res,'isyeri?isyeriid=' + data.isyeriid);
                        }
                    });
                }
            });
        });
    },
    isyeriisverentemsilcileripost:function (req, res) {
        req.on('end', function () {
            var fields = ['isyeriid','dkisverentems1','dkisverentems2','dkisverentemsydk1','dkisverentemsydk2'];
            global.core.getFormData(req.formData, fields, function (data) {
                isyeriManager.updateIsyeriTemsilcisi(data, function (result) {
                    global.console.log('isyeri isveren temsilcileri updated redirect to ==> '+'isyeriisverentemsilcileri?isyeriid=' + data.isyeriid);
                    global.core.redirect(res, 'isyeriisverentemsilcileri?isyeriid=' + data.isyeriid);
                });
            });
        });
    }
};
module.exports = isyeri;