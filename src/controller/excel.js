var excelWork = require('./../bussines/excel');

var excel = {
    isyeriuyelistesiexcel: function (req, res) {
        excelWork.isyeriuyelistesiexcel(req, res);
    },
    isyerigiriscikisexcel: function (req, res) {
        excelWork.isyerigiriscikisexcel(req, res);
    },
    isyerigiriscikisisimliexcel: function (req, res) {
        excelWork.isyerigiriscikisisimliexcel(req, res);
    },
    isyeriorgutexcel: function (req, res) {
        excelWork.isyeriorgutexcel(req, res);
    },
    isyeriuyeexcel: function(req, res){
        excelWork.isyeriuyeexcel(req, res);
    },
    isyeritespitexcel: function (req, res) {
        excelWork.isyeritespitexcel(req, res);
    },
    isyeritemsilciexcel: function (req, res) {
        excelWork.isyeritemsilciexcel(req, res);
    },
    isyeriisverentemsilcileriexcel: function (req, res) {
        excelWork.isyeriisverentemsilcileriexcel(req, res);
    },
    subeexcel: function (req, res) {
        excelWork.subeexcel(req, res);
    },
    subeisyerleriexcel: function (req, res) {
        excelWork.subeisyerleriexcel(req, res);
    },
    subeistatistikexcel: function (req, res) {
        excelWork.subeistatistikexcel(req, res);
    },
    subeyonetimexcel: function (req, res) {
        excelWork.subeyonetimexcel(req, res);
    },
    subegiriscikisexcel: function (req, res) {
        excelWork.subegiriscikisexcel(req, res);
    },
    subegiriscikisisimliexcel: function (req, res) {
        excelWork.subegiriscikisisimliexcel(req, res);
    },
    subetespitexcel: function (req, res) {
        excelWork.subetespitexcel(req, res);
    },
    subesendikaexcel: function (req, res) {
        excelWork.subesendikaexcel(req, res);
    },
    subetemsilcilerexcel: function (req, res) {
        excelWork.subetemsilcilerexcel(req, res);
    },
    subeuyeexcel:function (req, res) {
        excelWork.subeuyeexcel(req, res);
    },
    tespitExel: function (req, res) {
        excelWork.tespitExel(req ,res);
    },
    genelraporlarsubeExcel:function (req, res) {
        excelWork.genelraporlarsubeExcel(req, res);
    },
    genelraporlaristatistikExcel: function (req, res) {
        excelWork.genelraporlaristatistikExcel(req, res);
    },
    genelraporlarisimligiriscikisExcel:function (req, res) {
        excelWork.genelraporlarisimligiriscikisExcel(req, res);
    },
    genelraporlargiriscikisExcel: function (req, res) {
        excelWork.genelraporlargiriscikisExcel(req, res);
    },
    genekraporlarkayitlistesiExcel: function (req, res) {
        excelWork.genekraporlarkayitlistesiExcel(req, res);
    },
    genekraporlarorgbaslangicExcel: function (req, res) {
        excelWork.genekraporlarorgbaslangicExcel(req, res);
    },
    genekraporlaruyelistesiExcel: function (req, res) {
        excelWork.genekraporlaruyelistesiExcel(req, res);
    },
    genekraporlarsonhareketlerExcel: function (req, res) {
        excelWork.genekraporlarsonhareketlerExcel(req, res);
    },
    sendikatespitExcel: function (req, res) {
        excelWork.sendikatespitExcel(req, res);
    }

};
module.exports = excel;