var genelraporManager = require('./../DataAccess/genelraporlar');
var work = require('./../bussines/genelraporlar');

var genelRapor = {
    subeler: function (req, res) {
        work.subeler(req, res);
    },
    istatistik: function (req, res) {
        work.istatistik(req, res);
    },
    istatistikAjax:function (req, res) {
        work.istatistikAjax(req, res);
    },
    genelraporlarisimligiriscikis:function (req, res) {
        work.genelraporlarisimligiriscikis(req, res);
    },
    genelraporlarisimligiriscikisAjax:function (req, res) {
        work.genelraporlarisimligiriscikisAjax(req, res);
    },
    genelraporlargiriscikis:function (req, res) {
        work.genelraporlargiriscikis(req, res);
    },
    genelraporlargiriscikisAjax: function (req, res) {
        work.genelraporlargiriscikisAjax(req, res);
    },
    genelraporlarkayitlistesi:function (req, res) {
        work.genelraporlarkayitlistesi(req, res);
    },
    genelraporlarkayitlistesiAjax:function (req, res) {
        work.genelraporlarkayitlistesiAjax(req, res);
    },
    genelraporlarkayitdefteri:function (req, res) {
        work.genelraporlarkayitdefteri(req, res);
    },
    genelraporlarkayitdefteriAjax:function (req, res) {
        work.genelraporlarkayitdefteriAjax(req, res);
    },
    genelraporlarorgbaslangic:function (req, res) {
        work.genelraporlarorgbaslangic(req, res);
    },
    genelraporlarorgbaslangicAjax: function (req, res) {
        work.genelraporlarorgbaslangicAjax(req, res);
    },
    genelraporlaruyeliklistesi:function (req, res) {
        work.genelraporlaruyeliklistesi(req, res);
    },
    genelraporlaruyeliklistesiAjax:function (req, res) {
        work.genelraporlaruyeliklistesiAjax(req, res);
    },
    genelraporlarsonhareketler: function (req, res) {
        work.genelraporlarsonhareketler(req, res);
    },
    genelraporlartilldayCountAjax: function (req, res) {
        work.genelraporlartilldayCountAjax(req, res);
    }

};

module.exports = genelRapor;