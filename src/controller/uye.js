var uyeBussnes = require('./../bussines/uye');

var uye = {
	getUyeler: function (req, res) {
		uyeBussnes.uyeListesi(req, res);
	},
	postexcel: function(req, res){
		uyeBussnes.getexcelFile(req, res);
	},
	getTotalUyeCount: function (req, res) {
		uyeBussnes.getTotalCount(req, res);
	},
	getTotalUyePagingCount: function (req, res) {
		uyeBussnes.getTotalUyePagingCount(req, res);
	},
	getUyePagingData: function (req, res) {
		uyeBussnes.getUyePagingData(req, res);
	},
	uyeAjaxSearch: function (req, res) {
		uyeBussnes.uyeAjaxSearch(req, res);
	},
	uyeAjaxSearchCount: function (req, res) {
		uyeBussnes.uyeAjaxSearchCount(req, res);
	},
	searchinuye: function(req, res){
		uyeBussnes.searchinuye(req, res);
	},
	uyedetay: function (req, res) {
		uyeBussnes.uyedetay(req, res);
	},
	uyedetaypost: function (req, res) {
		uyeBussnes.saveUyeDetay(req, res);
	},
	updateLastMove: function(req, res){
		uyeBussnes.updateLastMove(req, res);
	},
	/*ajaxUyeCheckSskNo: function (req, res) {
		uyeBussnes.CheckSskNo(req, res);
	},*/
	ajaxUyeCheckVatNo: function (req, res) {
		uyeBussnes.CheckVatNo(req, res);
	},
	yeniuye: function (req, res) {
		uyeBussnes.yeniuye(req, res);
	},
	yeniuyepost: function (req, res) {
		uyeBussnes.saveUyeDetay(req, res);
	},
	yeniuyepostajax: function (req, res) {
		uyeBussnes.yeniuyepostajax(req, res);
	},
	deleteuye: function (req, res) {
		uyeBussnes.deleteUye(req, res);
	},
    ajaxUyeById:function (req, res) {
		uyeBussnes.ajaxUyeById(req, res);
    },
	makeCorrectRecords: function (req, res){
		uyeBussnes.makeCorrectRecords(req, res);
	}
};

module.exports = uye;