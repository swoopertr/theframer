var setting = require('./setting');
const controller = require("./controller/hata");

function funcRoute(req, res) {
    var routeName = req.url.split('/');
    var routePath = routeName[1].split('?')[0]; // this extract querystring section
    var methName = req.method.toLowerCase();
    if (routeName[1] == "") {
        routePath = "anasayfa";
    }
    if (routes.hasOwnProperty(routePath)) {
        var item = routes[routePath][methName];
        var contName;
        if (item.hasOwnProperty('controller')){
            contName = item.controller;
        }else {
            var controller = require('./controller/hata');
            controller.hata(req, res);
        }

        if (item.isCached) {
            global.cache.get(req.url, function (isAvailable) {
                if (isAvailable !== undefined ) {
                    res.end('' + isAvailable);
                } else {
                    var controller = require('./controller/' + contName);
                    req.isCached = true;
                    if (typeof controller[item.func] == 'function'){
                        controller[item.func](req, res);
                    }else{
                        var err_controller = require('./controller/hata');
                        err_controller.hata(req, res);
                    }
                }
            });
        } else {
            var controller = require('./controller/' + contName);
            controller[item.func](req, res);
        }
    } else {
        //if path not found page displays this.
        var controller = require('./controller/hata');
        controller.hata(req, res);
    }
}

//controller name is the js file name in "src/controller" folder
var routes = {
    'izinyok': {
        get: {
            controller: 'hata', func: 'notauth', isCached: false
        }
    },
    'hata': {
        get: {
            controller: 'hata', func: 'tekrar', isCached: false
        }
    },
    'anasayfa': {
        get: {
            controller: 'homepage', func: 'main', isCached: false
        },
        post: {
            controller: 'homepage', func: 'mainPost', isCached: false
        }
    },
    'list': {
        get: {
            controller: 'list', func: 'main', isCached: true
        },
        post: {
            controller: 'list', func: 'mainPost', isCached: false
        }
    },
    'login': {
        get: {
            controller: 'login', func: 'checklogin', isCached: true
        },
        post: {
            controller: 'login', func: 'checkloginpost', isCached: false
        }
    },
    'logout': {
        get: {
            controller: 'login', func: 'logout', isCached: false
        }
    },
    'recoverUser': {
        get: {
            controller: 'login', func: 'sendMailPassword', isCached: false
        }
    },
    'sube': {
        get: {
            controller: 'sube', func: 'subeListesi', isCached: false
        }
    },
    'subedetay': {
        post: {
            controller: 'sube', func: 'subedetaypost', isCached: false
        }
    },
    'subeistatistik': {
        get: {
            controller: 'sube', func: 'subeIstatistik', isCached: false
        }
    },
    'subeAjaxIstatistik': {
        get: {
            controller: 'sube', func: 'subeAjax', isCached: false
        }
    },
    'subeyonetim': {
        get: {
            controller: 'sube', func: 'subeyonetim', isCached: false
        }
    },
    'subeyonetimedit':{
        get:{
            controller: 'sube', func:'subeyonetimedit', isCached: false
        },
        post:{
            controller:'sube', func: 'subeyonetimeditpost', isCached:false
        }
    },
    'searchinuye': {
        get:{
            controller: 'uye', func: 'searchinuye', isCached: false
        }
    },
    'subeyonetimisil':{
        post:{
            controller: 'sube', func: 'subeyonetimisil', isCached:false
        }
    },
    'subegiriscikis': {
        get: {
            controller: 'sube', func: 'subegiriscikis', isCached: false
        }
    },
    'subeAjaxSubegiriscikis': {
        get: {
            controller: 'sube', func: 'subeAjaxSubegiriscikis', isCached: false
        }
    },
    'subegiriscikisisimli': {
        get: {
            controller: 'sube', func: 'giriscikisisimli', isCached: false
        }
    },
    'subeAjaxSubegiriscikisisimli': {
        get: {
            controller: 'sube', func: 'subeAjaxSubegiriscikisisimli', isCached: false
        }
    },
    'subetespit': {
        get: {
            controller: 'sube', func: 'subetespit', isCached: false
        }
    },
    'subeAjaxtespit': {
        get: {
            controller: 'sube', func: 'subeAjaxtespit', isCached: false
        }
    },
    'subesendika': {
        get: {
            controller: 'sube', func: 'subesendika', isCached: false
        }
    },
    'subetemsilciler': {
        get: {
            controller: 'sube', func: 'subetemsilciler', isCached: false
        }
    },
    'subeuyelistesi': {
        get: {
            controller: 'sube', func: 'subeuyelistesi', isCached: false
        }
    },
    'ajaxsubeuyelistesi': {
        get: {
            controller: 'sube', func: 'ajaxsubeuyelistesi', isCached: false
        }
    },
    'reset': {
        get: {
            controller: 'resetter', func: 'resetServerCache', isCached: false
        }
    },
    'resetCache': {
        get: {
            controller: 'resetter', func: 'refillServerCache', isCached: false
        }
    },
    'uye': {
        get: {
            controller: 'uye', func: 'getUyeler', isCached: false
        }
    },
    'uyeExcelYukle':{
        file:true,
        post:{
            controller:'uye', func:'postexcel', isCached: false
        }
    },
    'deleteuye': {
        post: {
            controller: 'uye', func: 'deleteuye', isCached: false
        }
    },
    'uyeAjaxTotalCount': {
        get: {
            controller: 'uye', func: 'getTotalUyeCount', isCached: false
        }
    },
    'getTotalUyePagingCount': {
        get: {
            controller: 'uye', func: 'getTotalUyePagingCount', isCached: false
        }
    },
    'getUyePagingData': {
        get: {
            controller: 'uye', func: 'getUyePagingData', isCached: false
        }
    },
    'uyeAjaxSearch': {
        get: {
            controller: 'uye', func: 'uyeAjaxSearch', isCached: false
        }
    },
    'uyeAjaxSearchCount': {
        get: {
            controller: 'uye', func: 'uyeAjaxSearchCount', isCached: false
        }
    },
    "makeCorrectRecords":{
        get : {
            controller: 'uye', func: 'makeCorrectRecords', isCached: false
        }
    },
    'uyedetay': {
        get: {
            controller: 'uye', func: 'uyedetay', isCached: false
        }
    },
    'uyedetaypost': {
        post: {
            controller: 'uye', func: 'uyedetaypost', isCached: false
        }
    },
    'unknown': {
        get: {
            controller: 'unknown', func: 'updateAllLastMoveRecords', isCached: false
        }
    },
    'updateLastMove':{
        get: {
            controller: 'uye', func: 'updateLastMove', isCached: false
        }
    },
    'yeniuye': {
        get: {
            controller: 'uye', func: 'yeniuye', isCached: false
        }
    },
    'ajaxuyebyid': {
        get: {
            controller: 'uye', func: 'ajaxUyeById', isCached: false
        }
    },
    'yeniuyepost': {
        post: {
            controller: 'uye', func: 'yeniuyepost', isCached: false
        }
    },
    'yeniuyepostajax': {
        post: {
            controller: 'uye', func: 'yeniuyepostajax', isCached: false
        }
    },
    'hareketdetay': {
        get: {
            controller: 'hareket', func: 'getHareket', isCached: false
        }
    },
    'postAjaxHareketDetay': {
        post: {
            controller: 'hareket', func: 'postAjaxHareketDetay', isCached: false
        }
    },
    'hareketdetaypost': {
        post: {
            controller: 'hareket', func: 'postHareketDetay', isCached: false
        }
    },
    'hareketsaveajax': {
        post: {
            controller: 'hareket', func: 'hareketsaveajax', isCached: false
        }
    },
    'getUyeCheckSskNo': {
        get: {
            controller: 'uye', func: 'ajaxUyeCheckSskNo', isCached: false
        }
    },
    'getUyeCheckVatNo': {
        get: {
            controller: 'uye', func: 'ajaxUyeCheckVatNo', isCached: false
        }
    },
    'isyeri': {
        get: {
            controller: 'isyeri', func: 'main', isCached: true
        }
    },
    isyeriByIdAjax: {
        get: {
            controller: 'isyeri', func: 'isyeriByIdAjax', isCached: false
        }
    },
    'isyeridetay': {
        get: {
            controller: 'isyeri', func: 'isyeridetay', isCached: false
        },
        post: {
            controller: 'isyeri', func: 'isyeridetaypost', isCached: false
        }
    },
    'isyeriedit': {
        get: {
            controller: 'isyeri', func: 'isyeriedit', isCached: false
        }
    },
    'isyeriAjaxlist': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxlist', isCached: false
        }
    },
    'isyeriAjaxListCount': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxListCount', isCached: false
        }
    },
    'isyeriAjaxTotalCount': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxTotalCount', isCached: true
        }
    },
    'isyeriAjaxTotalPagingCount': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxTotalPagingCount', isCached: false
        }
    },
    'isyeriAjaxPagingData': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxPagingData', isCached: false
        }
    },
    'isyeriistatistik': {
        get: {
            controller: 'isyeri', func: 'istatistik', isCached: false
        }
    },
    isyeriAjaxDelete: {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxDelete', isCache: false
        }
    },
    'uyelistesi': {
        get: {
            controller: 'isyeri', func: 'uyelistesi', isCached: false
        }
    },
    'isyeritemsilci': {
        get: {
            controller: 'isyeri', func: 'isyeritemsilci', isCached: false
        }
    },
    'isyerigiriscikis': {
        get: {
            controller: 'isyeri', func: 'isyerigiriscikis', isCached: false
        }
    },
    'isyeriAjaxisyerigiriscikis': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxisyerigiriscikis', isCached: false
        }
    },
    'isyeriisimligiriscikis': {
        get: {
            controller: 'isyeri', func: 'isyerigiriscikisisimli', isCached: false
        }
    },
    'isyeriAjaxgirisCikisIsimli': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxgirisCikisIsimli', isCached: false
        }
    },
    'isyeriorgut': {
        get: {
            controller: 'isyeri', func: 'isyeriorgut', isCached: false
        }
    },
    'isyeritemsilci': {
        get: {
            controller: 'isyeri', func: 'isyeritemsilci', isCached: false
        }
    },
    'isyeritemsilciedit': {
        get: {
            controller: 'isyeri', func: 'isyeritemsilciEdit', isCache: false
        }
    },
    'isyeritemsilciEklePost': {
        post: {
            controller: 'isyeri', func: 'isyeritemsilciEklePost', isCache: false
        }
    },
    'isyeritemsilcisil': {
        post: {
            controller: 'isyeri', func: 'isyeritemsilciSilPost', isCache: false
        }
    },
    'isyeritespit': {
        get: {
            controller: 'isyeri', func: 'isyeritespit', isCached: false
        }
    },
    'isyeriAjaxtespit': {
        get: {
            controller: 'isyeri', func: 'isyeriAjaxtespit', isCached: false
        }
    },
    'isyeriisverentemsilcileri': {
        get: {
            controller: 'isyeri', func: 'isyeriisverentemsilcileri', isCache: false
        },
        post: {
            controller: 'isyeri', func: 'isyeriisverentemsilcileripost', isCache: false
        }
    },
    'tespit': {
        get: {
            controller: 'tespit', func: 'main', isCached: false
        }
    },
    'tespitdetay': {
        get: {
            controller: 'tespit', func: 'tespitdetay', isCached: false
        },
        post: {
            controller: 'tespit', func: 'tespitpost', isCached: false
        }
    },
    'deleteptespit': {
        post: {
            controller: 'tespit', func: 'deleteptespit', isCached: false
        }
    },
    'genelraporlar': {
        get: {
            controller: 'genelraporlar', func: 'subeler', isCached: false
        }
    },
    'genelraporlaristatistik': {
        get: {
            controller: 'genelraporlar', func: 'istatistik', isCached: false
        }
    },
    'genelraporlarAjaxistatistik': {
        get: {
            controller: 'genelraporlar', func: 'istatistikAjax', isCache: false
        }
    },
    'genelraporlarisimligiriscikis': {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarisimligiriscikis', isCache: false
        }
    },
    'genelraporlarisimligiriscikisAjax': {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarisimligiriscikisAjax', isCache: false
        }
    },
    'genelraporlargiriscikis': {
        get: {
            controller: 'genelraporlar', func: 'genelraporlargiriscikis', isCache: false
        }
    },
    'genelraporlargiriscikisAjax': {
        get: {
            controller: 'genelraporlar', func: 'genelraporlargiriscikisAjax', isCache: false
        }
    },
    genelraporlarkayitlistesi: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarkayitlistesi', isCache: false
        }
    },
    genelraporlarkayitlistesiAjax: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarkayitlistesiAjax', isCache: false
        }
    },
    genelraporlarkayitdefteri: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarkayitdefteri', isCache: false
        }
    },
    genelraporlarkayitdefteriAjax: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarkayitdefteriAjax', isCache: false
        }
    },
    genelraporlarorgbaslangic: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarorgbaslangic', isCache: false
        }
    },
    genelraporlarorgbaslangicAjax: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarorgbaslangicAjax', isCache: false
        }
    },
    genelraporlaruyeliklistesi: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlaruyeliklistesi', isCache: false
        }
    },
    genelraporlaruyeliklistesiAjax: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlaruyeliklistesiAjax', isCache: false
        }
    },
    genelraporlarsonhareketler: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlarsonhareketler', isCache: false
        }
    },
    genelraporlartilldayCountAjax: {
        get: {
            controller: 'genelraporlar', func: 'genelraporlartilldayCountAjax', isCache: false
        }
    },
    checkforclevel: {
        get: {
            controller: 'hareket', func: 'checkforclevel', isCached: false
        }
    },
    checkforclevelsube: {
        get: {
            controller: 'hareket', func: 'checkforclevelsube', isCached: false
        }
    },
    deletemovementhareketid: {
        get: {
            controller: 'hareket', func: 'deletemovementhareketid', isCached: false
        }
    },
    checksgkforisyeri: {
        get: {
            controller: 'isyeri', func: 'checksgkforisyeri', isCached: false
        }
    },
    ajaxdeletesube: {
        get: {
            controller: 'sube', func: 'ajaxdeletesube', isCached: false
        }
    },
    sendika: {
        get: {
            controller: 'sendika', func: 'sendikalar', isCached: false
        }
    },
    sendikadetay: {
        get: {
            controller: 'sendika', func: 'sendikadetay', isCached: false
        },
        post: {
            controller: 'sendika', func: 'postsendikadetay', isCached: false
        }
    },
    sendikatespit: {
        get: {
            controller: 'sendika', func: 'sendikatespit', isCached: false
        }
    },
    sendikatespitajax: {
        get: {
            controller: 'sendika', func: 'sendikatespitajax', isCached: false
        }
    },
    sendikaisyeriCountAjax: {
        get: {
            controller: 'sendika', func: 'sendikaisyeriCountAjax', isCached: false
        }
    },
    sendikaDeleteAjax: {
        get: {
            controller: 'sendika', func: 'sendikaDeleteAjax', isCached: false
        }
    },
    isyeriuyelistesiexcel: {
        get: {
            controller: 'excel', func: 'isyeriuyelistesiexcel', isCached: false
        }
    },
    isyerigiriscikisexcel: {
        get: {
            controller: 'excel', func: 'isyerigiriscikisexcel', isCached: false
        }
    },
    isyerigiriscikisisimliexcel: {
        get: {
            controller: 'excel', func: 'isyerigiriscikisisimliexcel', isCached: false
        }
    },
    isyeriorgutexcel: {
        get: {
            controller: 'excel', func: 'isyeriorgutexcel', isCached: false
        }
    },
    isyeritespitexcel: {
        get: {
            controller: 'excel', func: 'isyeritespitexcel', isCached: false
        }
    },
    isyeritemsilciexcel: {
        get: {
            controller: 'excel', func: 'isyeritemsilciexcel', isCached: false
        }
    },
    isyeriisverentemsilcileriexcel: {
        get: {
            controller: 'excel', func: 'isyeriisverentemsilcileriexcel', isCached: false
        }
    },
    subeexcel: {
        get: {
            controller: 'excel', func: 'subeexcel', isCached: false
        }
    },
    subeisyerleriexcel: {
        get: {
            controller: 'excel', func: 'subeisyerleriexcel', isCached: false
        }
    },
    subeistatistikexcel: {
        get: {
            controller: 'excel', func: 'subeistatistikexcel', isCached: false
        }
    },
    subeyonetimexcel: {
        get: {
            controller: 'excel', func: 'subeyonetimexcel', isCached: false
        }
    },
    subegiriscikisexcel: {
        get: {
            controller: 'excel', func: 'subegiriscikisexcel', isCached: false
        }
    },
    subegiriscikisisimliexcel: {
        get: {
            controller: 'excel', func: 'subegiriscikisisimliexcel', isCached: false
        }
    },
    subetespitexcel: {
        get: {
            controller: 'excel', func: 'subetespitexcel', isCached: false
        }
    },
    subesendikaexcel: {
        get: {
            controller: 'excel', func: 'subesendikaexcel', isCached: false
        }
    },
    subetemsilcilerexcel: {
        get: {
            controller: 'excel', func: 'subetemsilcilerexcel', isCached: false
        }
    },
    subeuyeexcel: {
        get: {
            controller: 'excel', func: 'subeuyeexcel', isCached: false
        }
    },
    tespitExel: {
        get: {
            controller: 'excel', func: 'tespitExel', isCached: false
        }
    },
    genelraporlarsubeExcel: {
        get: {
            controller: 'excel', func: 'genelraporlarsubeExcel', isCached: false
        }
    },
    genelraporlaristatistikExcel: {
        get: {
            controller: 'excel', func: 'genelraporlaristatistikExcel', isCached: false
        }
    },
    genelraporlarisimligiriscikisExcel: {
        get: {
            controller: 'excel', func: 'genelraporlarisimligiriscikisExcel', isCached: false
        }
    },
    genelraporlargiriscikisExcel: {
        get: {
            controller: 'excel', func: 'genelraporlargiriscikisExcel', isCached: false
        }
    },
    genekraporlarkayitlistesiExcel: {
        get: {
            controller: 'excel', func: 'genekraporlarkayitlistesiExcel', isCached: false
        }
    },
    genekraporlarorgbaslangicExcel: {
        get: {
            controller: 'excel', func: 'genekraporlarorgbaslangicExcel', isCached: false
        }
    },
    genekraporlaruyelistesiExcel: {
        get: {
            controller: 'excel', func: 'genekraporlaruyelistesiExcel', isCached: false
        }
    },
    genekraporlarsonhareketlerExcel: {
        get: {
            controller: 'excel', func: 'genekraporlarsonhareketlerExcel', isCached: false
        }
    },
    sendikatespitExcel: {
        get: {
            controller: 'excel', func: 'sendikatespitExcel', isCached: false
        }
    },
    isyeriuyeexcel: {
        get: {
            controller: 'excel', func: 'isyeriuyeexcel', isCached: false
        }
    },
    deneme: {
        get: {
            controller: 'deneme', func:'mail', isCached: false
        }
    },
    'egitim': {
        get: {
            controller: 'education', func: 'main', isCached: false
        }
    },
    'egitimolustur': {
        get: {
            controller: 'education', func: 'educationCreate', isCached: false
        }
    },
    'egitimtipleri': {
        get: {
            controller: 'education', func: 'educationTypes', isCached: false
        }
    },
    'egitimTipiEdit': {
        get: {
            controller: 'education', func: 'editeducationCategory', isCached: false
        },
        post: {
            controller: 'education', func: 'postEditeducationCategory', isCached: false
        }
    },
    'egitimTipiDetay':{
        get:{
            controller:'education', func: 'egitimTipiDetay', isCached: false
        }
    },
    'egitimcilistesi':{
        get:{
            controller: 'education', func: 'teachers', isCached: false
        }
    },
    'egitimcidetay':{
        get:{
            controller: 'education', func: 'educationTeacherDetail', isCached: false
        },
        post:{
            controller: 'education', func: 'educationTeacherDetailPost', isCached: false
        }
    },
    'egitimcisil': {
        post:{
            controller : 'education', func: 'educationTeacherDelete', isCached:false
        }
    },
    'egitimlistesi': {
        get:{
            controller:'education', func: 'educationList', isCached:false
        }
    },
    'egitimdetay': {
        get:{
            controller : 'education', func: 'educationItemDetail', isCached: false
        }
    },
    'egitimEkleCalendarAjax':{
        post: {
            controller:'education', func:'educationAddCalendarAjax', isCached: false
        }
    }
};
var routeObj = {
    callRoute: funcRoute,
    routes: routes
};
module.exports = routeObj;