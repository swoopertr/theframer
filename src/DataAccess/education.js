var pg = require('./../Work/Data/Postgre');

var education = {
    education:{
        getAll: function(cb){
            var q = "SELECT * FROM orgut.egitimler";
            pg.query(q, function(result){
                cb && cb(result);
            });
        },
        getAllForPosing : function(cb){
            var q = `SELECT et."educationType", e.*  FROM orgut.egitimler e
            inner join orgut."egitimTipleri" et on et."Id" = e."EgitimTipId"  where e."Status" = 1;`;
            pg.query(q, function(result){
                cb && cb(result);
            });
        },
        getOne: function(id, cb){
            var q = "SELECT * FROM orgut.egitimler where id = $1";
            pg.query(q, [id], function(result){
                cb && cb(result);
            });
        }
    },
    teacher: {
        getTeachers: function (cb) {
            var q = 'select "Id", "Name", "Surname", "Phone", "TcNo", "Status", "EducatorType" from orgut."egitimciler" where "Status"!=3';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        getTeacher: function (id, cb) {
            var q = 'select "Id", "Name", "Surname", "Phone", "TcNo", "Status", "Description", "EducatorType" from orgut."egitimciler" where "Id"= ' + id;
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        upsertTeacher: function (teacher, cb) {
            var q = '';
            if (teacher.Id == "0") {
                q = `insert into orgut."egitimciler" ("Name", "TcNo", "Status", "Surname", "Phone", "Description", "EducatorType")
                        values ('${teacher.Name}', '${teacher.TcNo}', ${teacher.Status}, '${teacher.Surname}', '${teacher.Phone}', '${teacher.Description}', ${teacher.EducatorType})`;
            } else {
                q = `update orgut.egitimciler set "Name" ='${teacher.Name}', "Surname"= '${teacher.Surname}', "Phone"='${teacher.Phone}', 
                        "TcNo"='${teacher.TcNo}', "Description"='${teacher.Description}', "Status" ='${teacher.Status}', "EducatorType"=${teacher.EducatorType} where "Id" = ${teacher.Id}`;
            }
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        deleteTeacher: function (id, cb ){
            var q = `update orgut."egitimciler" set "Status" =3 where "Id" = ${id}`;
            pg.query(q, function (result) {
                cb && cb(result);
            });
        }
    },
    types:{
        getAll: function (cb) {
            var q = 'select "Id","educationType",(select "educationType" from orgut."egitimTipleri" e2 where e2."Id"=e1."parentId" ) parentname, "parentId", status from orgut."egitimTipleri" e1 where status!=3';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        getOne: function (id, cb) {
            var q = 'select * from orgut."egitimTipleri" where "Id"=' + id + '';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        upsert: function (data, cb) {
            var q = '';
            if (data.id ==0){   //insert
                q = `insert into orgut."egitimTipleri" ( "educationType", "parentId", "status", "ex_props", "description")
                    values ('${data.eduName}', ${data.selectRoof}, ${data.statusSelect}, null, '${data.description}')`;
            }else{              //update
                q = `update orgut."egitimTipleri"
                set  "educationType"='${data.eduName}', "parentId"=${data.selectRoof}, status=${data.statusSelect}, ex_props=null, description='${data.description}'
                where "Id" =${data.id}`;
            }

            pg.query(q, function (result) {
                cb && cb(result);
            });
        }

    },
    educationClass: {
        insert: function(data, cb){
            /* var q = `INSERT INTO orgut.egitimler
            ("Name", egitimtipid, "start", "end", "location", state, title, calendarid, isallday, isprivate)
            VALUES( 'ders', 0, '', '', '', '', '', '', 0, 0);`;
            pg.query(); */
        }
    }
};

module.exports = education;