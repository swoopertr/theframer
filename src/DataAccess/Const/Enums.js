var enums = {
    init: function (cb) {
        global.dataenums = {
            enums: {
                vazife: enums.vazife,
                kadinerkek: enums.kadinerkek,
                hareketCinsi: enums.hareketCinsi,
                orgutlenmeTipi: enums.orgutlenmeTipi,
                taseron: enums.taseron,
                egitim: enums.egitim,
                kangrubu: enums.kangubu,
                vazifeTemsilci: enums.vazifeTemsilciler,
                vazifeTemsilciArr: enums.vazifeTemsilcilerArr,
                ACCESSTYPE: enums.ACCESSTYPE,
                isverenSendikasi: enums.isverenSendikasi,
                kamuTipi: enums.kamuTipi,
                gruplama: enums.gruplama,
                status: enums.status,
                ogretmenTipi: enums.ogretmenTipi
            }
        }
        if (typeof cb === "function") {
            cb();
        }
    },
    isverenSendikasi: {
        'MESS': 'MESS',
        'EMİS': 'EMİS'
    },
    kangubu: {
        1: '0+',
        2: 'A+',
        3: 'B+',
        4: 'AB+',
        5: '0-',
        6: 'A-',
        7: 'B-',
        8: 'AB-'
    },
    egitim: {
        1: 'Eğitimsiz',
        2: 'Okuryazar',
        3: 'İlkokul',
        4: 'Ortaokul',
        5: 'Lise',
        6: 'Lisans',
        7: 'Lisansüstü',
        8: 'Doktora'
    },
    vazife: {
        1: 'Baskan',
        2: 'sekreter',
        3: 'MaliSekreter',
        4: 'OrgutlenmeSekreteri',
        5: 'EgitimSekreteri',
        23: 'BasYaySek',
        24: 'SosyalIsSek',
        6: 'YKYdk1',
        7: 'YKYdk2',
        8: 'YKYdk3',
        9: 'YKYdk4',
        10: 'YKYdk5',
        25: 'YKYdk6',
        26: 'YKYdk7',
        11: 'DenK1',
        12: 'DenK2',
        13: 'DenK3',
        14: 'DenKYdk1',
        15: 'DenKYdk2',
        16: 'DenKYdk3',
        17: 'DisK1',
        18: 'DisK2',
        19: 'DisK3',
        20: 'DisKYdk1',
        21: 'DisKYdk2',
        22: 'DisKYdk3'
    },
    vazifeTemsilciler: {
        1: 'BasTemsilci',
        2: 'Temsilci',
        15: 'Temsilci2',
        16: 'Temsilci3',
        17: 'Temsilci4',
        18: 'Temsilci5',
        19: 'Temsilci6',
        3: 'Disiplin1',
        4: 'Disiplin2',
        5: 'DisiplinY1',
        6: 'DisiplinY2',
        8: 'ISG_Kurulu1',
        12: 'ISG_Kurulu2',
        13: 'ISG_KuruluY1',
        14: 'ISG_KuruluY2',
        7: 'Izin_Kurulu1',
        9: 'Izin_Kurulu2',
        10: 'Izin_KuruluY1',
        11: 'Izin_KuruluY2'
    },
    vazifeTemsilcilerArr: [
        {key: 1, val: 'BasTemsilci'},
        {key: 2, val: 'Temsilci'},
        {key: 3, val: 'Disiplin1'},
        {key: 4, val: 'Disiplin2'},
        {key: 5, val: 'DisiplinY1'},
        {key: 6, val: 'DisiplinY2'},
        {key: 8, val: 'ISG_Kurulu1'},
        {key: 12, val: 'ISG_Kurulu2'},
        {key: 13, val: 'ISG_KuruluY1'},
        {key: 14, val: 'ISG_KuruluY2'},
        {key: 7, val: 'Izin_Kurulu1'},
        {key: 9, val: 'Izin_Kurulu2'},
        {key: 10, val: 'Izin_KuruluY1'},
        {key: 11, val: 'Izin_KuruluY2'},
        {key: 15, val: 'Temsilci2'},
        {key: 16, val: 'Temsilci3'},
        {key: 17, val: 'Temsilci4'},
        {key: 18, val: 'Temsilci5'},
        {key: 19, val: 'Temsilci6'}
    ],
    kadinerkek: {
        1: 'Erkek',
        2: 'Kadın'
    },
    hareketCinsi: {
        1: 'Kayıt',
        2: 'İstifa',
        3: 'Madde 17',
        4: 'Madde 25',
        5: 'Emekli',
        6: 'İhraç',
        7: 'Kapsam Dışı'
    },
    orgutlenmeTipi: {
        1: 'Devam',
        2: 'İstifa',
        3: 'Kapandı',
        4: 'Karşı Sendika',
        5: 'Örgütsüz',
        6: 'İşlem Dışı',
        7: 'Dosya Kalktı',
        8: 'Örgütleniyor',
        9: 'Mahkemede'
    },
    taseron: {
        1: 'Yok',
        2: 'Var'
    },
    kamuTipi: {
        1: 'Kamu',
        2: 'Özel'
    },
    gruplama: {
        1: 'Hayır',
        2: 'Evet'
    },
    ACCESSTYPE: {
        ACCESS: 1,
        DENY: 2
    },
    status: {
        0: 'Pasif',
        1: 'Aktif'
    },
    ogretmenTipi:{
        1:"Kurum içi",
        2:"Sözleşmeli"
    }
};

module.exports = enums;