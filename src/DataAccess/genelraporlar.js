var pg = require('./../Work/Data/Postgre');
var subeMan = require('./sube');


var genelrapor = {
    subeler: function (cb) {
        subeMan.getList(function (result) {
            cb && cb(result);
        });
    },
    istatistik: function (orgutlenmeDurum, cb) {
        var q = 'select s.isim, count(*) as calisan,\n' +
            '  sum(case when u.kadinerkek=1 then 1 else 0 end) as erkek,\n' +
            '  sum(case when u.kadinerkek=2 then 1 else 0 end) as kadin\n' +
            ' from orgut.isyeri i\n' +
            '  inner join orgut.hareket h on h.isyeriid = i.isyeriid\n' +
            '  inner join orgut.uye u on h.hareketid = u.sonhareket\n' +
            '  INNER JOIN orgut.sube s on s.subeid =i.subeid\n' +
            ' where h.cins=1 and i.orgutlenmedurum=' + orgutlenmeDurum + 'group by s.isim;';

        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    genelraporlarisimligiriscikis: function (startDate, endDate, hareketType, cb) {


        var q = 'select h.tarih, i.isim  as isyeriisim, u.isim as uyeisim , u.soyisim, u.vatandaslik_no, s.isim as subeisim from orgut.hareket h' +
            ' join orgut.isyeri i using(isyeriid) ' +
            ' join orgut.uye u using(uyeid) ' +
            ' join orgut.sube s using(subeid) ' +
            ' where h.tarih between \'' + startDate + '\' and \'' + endDate +  ' 23:59\' ' +
            ' and (' + hareketType + '::smallint is null or ' + hareketType + '::smallint =h.cins) ' +
            ' order by h.tarih, i.isim, u.soyisim, u.isim';


        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    genelraporlargiriscikisObj: {
        untillDate: function (startDate, cb) {
            var q ='';
            if(startDate.indexOf('-') == -1){
                startDate = startDate.split('.');
                var startDay = startDate[0];
                var startMonth = startDate[1];
                var startYear = startDate[2];
                var newStartDate = startMonth + '.' + startDay + '.' + startYear;
                var q = 'select count(*) from orgut.genelsonhareketler_new(1, \'' + newStartDate + '\')';
            }else{
                startDate = startDate.split('-');
                var startDay = startDate[2];
                var startMonth = startDate[1];
                var startYear = startDate[0];
                var newStartDate = startMonth + '.' + startDay + '.' + startYear;
                var q = 'select count(*) from orgut.genelsonhareketler_new(1, \'' + newStartDate + '\')';
            }
       

            pg.query(q, function (result) {
                cb && cb(result);
            });

        },
        givenInterval: function (startDate, endDate, cb) {
            startDate = startDate.split('-');
            var startDay = startDate[2];
            var startMonth = startDate[1];
            var startYear = startDate[0];
            var newStartDate = startMonth + '.' + startDay + '.' + startYear;
            endDate = endDate.split('-');
            var endDay = endDate[2];
            var endMonth = endDate[1];
            var endYear = endDate[0];
            var newEndDate = endMonth + '.' + endDay + '.' + endYear;

            var q = 'with \n' +
                '\taylikHareketler as (\n' +
                '\t\twith\n' +
                '\t\taylar as (select extract(\'year\' from zaman) as yil,extract(\'month\' from zaman) as ay from generate_series(\'' + newStartDate + ' 00:00:00\'::date, \'' + newEndDate + ' 23:59:59\'::date, \'1 month\') as zaman),\n' +
                '\t\tcins as (select * from generate_series(1,5) cins),\n' +
                '                            adetler as (\n' +
                '                                select\n' +
                '                                    extract(year from tarih) as yil,\n' +
                '                                    extract(month from tarih) as ay,\n' +
                '                                    cins,\n' +
                '                                    count(*) as adet,\n' +
                '                                    s.subeid\n' +
                '                                from\n' +
                '                                    orgut.hareket as h\n' +
                '                                    inner join orgut.isyeri as i on (h.isyeriid = i.isyeriid)\n' +
                '                                    inner join orgut.sube as s on (s.subeid = i.subeid)\n' +
                '                                where\n' +
                '                                    tarih between \'' + newStartDate + ' 00:00:00\'::date and \'' + newEndDate + ' 23:59:59\'::date\n' +
                '                                group by\n' +
                '                                    yil,ay,cins,s.subeid\n' +
                '                                order by\n' +
                '                                    extract(year from tarih),extract(month from tarih),cins\n' +
                '                            ),\n' +
                '                            aycins as (select * from aylar,cins)\n' +
                '                          \n' +
                '                        select\n' +
                '                                yil,ay,aycins.cins as cins , adet, subeid\n' +
                '                        from\n' +
                '                                aycins\n' +
                '                                left join adetler using (yil,ay,cins)\n' +
                '                         \n' +
                '                    )\n' +
                '                    select\n' +
                '                        yil as year,ay as month, s.isim,\n' +
                '                        SUM(CASE cins WHEN 1 THEN adet ELSE 0 END) AS kayit,\n' +
                '                        SUM(CASE cins WHEN 2 THEN adet ELSE 0 END) AS istifa,\n' +
                '                        SUM(CASE cins WHEN 3 THEN adet ELSE 0 END) AS madde17,\n' +
                '                        SUM(CASE cins WHEN 4 THEN adet ELSE 0 END) AS madde25,\n' +
                '                        SUM(CASE cins WHEN 5 THEN adet ELSE 0 END) AS emekli\n' +
                '                    from\n' +
                '                        aylikHareketler as a \n' +
                '                        inner join orgut.sube as s on (a.subeid = s.subeid)\n' +
                '                    group by\n' +
                '                        yil, ay, s.subeid\n' +
                '                    order by yil,ay, s.isim';

            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        genelraporlargiriscikis: function (startDate, endDate, cb) {
            var result = {};
            genelrapor.genelraporlargiriscikisObj.untillDate(startDate, function (result1) {
                genelrapor.genelraporlargiriscikisObj.givenInterval(startDate, endDate, function (result2) {
                    genelrapor.genelraporlargiriscikisObj.untillDate(endDate, function (result3) {
                        result.tillStart = result1;
                        result.interval = result2;
                        result.tillEnd = result3;
                        cb && cb(result);
                    });
                });
            });
        }
    },
    genelraporlarkayitlistesi: function (startDate, endDate, cb) {
        startDate = startDate.split('-');
        var startDay = startDate[2];
        var startMonth = startDate[1];
        var startYear = startDate[0];
        var newStartDate = startMonth + '.' + startDay + '.' + startYear;
        endDate = endDate.split('-');
        var endDay = endDate[2];
        var endMonth = endDate[1];
        var endYear = endDate[0];
        var newEndDate = endMonth + '.' + endDay + '.' + endYear;
        var q = 'select u.vatandaslik_no, u.isim, u.soyisim, u.kadinerkek, u.telefon, h.kararno, h.noter,\n' +
            '  h.noteryevmiye, h.tarih, h.notertarih, i.isim as isyeriisim, i.bcmdosyano,\n' +
            '  s.isim as subeisim from orgut.hareket as h\n' +
            '  INNER JOIN orgut.isyeri as i ON h.isyeriid = i.isyeriid\n' +
            '  INNER JOIN orgut.uye as u ON u.uyeid = h.uyeid\n' +
            '  INNER JOIN orgut.sube as s ON i.subeid = s.subeid\n' +
            'where tarih > \'' + newStartDate + '\' and tarih < \'' + newEndDate + ' 23:59:59\' and cins =1 order by h.tarih asc;';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    genelraporlarkayitdefteri: function (startDate, endDate, cb) {
        startDate = startDate.split('-');
        var startDay = startDate[2];
        var startMonth = startDate[1];
        var startYear = startDate[0];
        var newStartDate = startMonth + '.' + startDay + '.' + startYear;
        endDate = endDate.split('-');
        var endDay = endDate[2];
        var endMonth = endDate[1];
        var endYear = endDate[0];
        var newEndDate = endMonth + '.' + endDay + '.' + endYear;
        var q = 'select u.isim, u.soyisim, u.dogumyeri, u.dogumyili, u.anaismi, u.babaismi, u.nkil, u.nkilce,\n' +
            '  u.nkmahalle,u.nkkoy, u.nkhane, u.nkcilt, u.nksayfa, i.isim as isyeriisim, i.adres, h.tarih, h.kararno from orgut.uye as u\n' +
            'INNER JOIN orgut.hareket as h on u.uyeid = h.uyeid\n' +
            'INNER JOIN orgut.isyeri as i on h.isyeriid = i.isyeriid\n' +
            'WHERE h.tarih > \'' + newStartDate + '\' and h.tarih < \'' + newEndDate + '\' and h.cins =1;';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    genelraporlarorgbaslangicListBefores: function (startDate, cb) {
        var q = 'SELECT isyeriid from (SELECT DISTINCT isyeriid from orgut.hareket WHERE tarih < \'' + startDate + '\' and cins =1) as firsts;';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    genelraporlarorgbaslangicListInterval: function (startDate, endDate, cb) {
        var q = 'select isyeriid from (SELECT isyeriid from orgut.hareket WHERE tarih > \'' + startDate + '\' and tarih < \'' + endDate + ' 23:59:59\' and cins =1) as interval';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    genelraporlaruyeliklistesi: function (endDate, isyeriOrgutlenmedurum, cb) {

        endDate = endDate.split('-');
        var endDay = endDate[2];
        var endMonth = endDate[1];
        var endYear = endDate[0];
        var newEndDate = endMonth + '.' + endDay + '.' + endYear;
        if (isyeriOrgutlenmedurum == 0){
            isyeriOrgutlenmedurum = Object.keys(global.dataenums.enums.orgutlenmeTipi).join(',');
        }
        var q = 'select u.isim as calisanisim, u.soyisim, u.telefon, u.vatandaslik_no, sonhareketler.dogrulama_kodu, sonhareketler.tarih, i.isim as isyeriisim, s.isim as subeisim ' +
            'from orgut.genelsonhareketler_new(1, \'' + newEndDate + '\') as sonhareketler\n' +
            'JOIN orgut.isyeri as i USING(isyeriid)\n' +
            'JOIN orgut.uye as u using(uyeid)\n' +
            'JOIN orgut.sube as s using(subeid)'+
        'where i.orgutlenmedurum in (' + isyeriOrgutlenmedurum + ') order by i.isim, tarih';
        /*var _q = 'select u.isim as calisanisim, u.soyisim, u.vatandaslik_no, h.dogrulama_kodu, h.tarih, i.isim as isyeriisim, s.isim as subeisim ' +
            'from orgut.hareket as h ' +
            'INNER JOIN (SELECT DISTINCT ON (uyeid) *,tarih as mdate  FROM orgut.hareket where tarih<\'' + newEndDate + '\' ORDER BY uyeid,hareketid DESC ) hg on h.uyeid = hg.uyeid and h.tarih = hg.mdate \n' +
            'INNER JOIN orgut.isyeri as i on h.isyeriid = i.isyeriid \n' +
            'INNER JOIN orgut.uye as u on h.uyeid = u.uyeid \n' +
            'INNER JOIN orgut.sube as s on i.subeid = s.subeid \n' +
            'WHERE \n' +
            'h.cins =1 and h.tarih< \'' + newEndDate + '\' and i.orgutlenmedurum =' + isyeriOrgutlenmedurum;*/
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    genelraporlarsonhareketler: function (songunsayisi, cb) {
        if (typeof songunsayisi == 'undefined') {
            songunsayisi = 0;
        }
        var q = 'select h.hareketid, h.tarih, ' +
            '  u.uyeid, u.isim , u.soyisim, u.vatandaslik_no,' +
            '  i.isim as isyeriisim, i.isyeriid, i.bcmdosyano,' +
            '  s.isim as subeisim, s.subeid, h.cins ' +
            'from orgut.hareket as h ' +
            '  inner join orgut.isyeri as i on h.isyeriid = i.isyeriid ' +
            '  inner join orgut.uye as u on h.uyeid = u.uyeid ' +
            '  inner join orgut.sube as s on s.subeid = i.subeid ' +
            'where tarih > now()- INTERVAL \'' + songunsayisi + ' DAY\' order by tarih desc;';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    }
};
module.exports = genelrapor;