var pg = require('./../Work/Data/Postgre');
var temsilci = {
    getOne: function(temsilciId, cb){
        var q = 'select * from orgut.temsilci where temsilciid='+temsilciId;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    deleteTemsilci: function (temsilciid, cb) {
        var q = 'delete from orgut.temsilci where temsilciid='+temsilciid;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    insertTemsilci: function (uyeid, isyeriid, vazifeid, sira, secimTarihi, atamaTarihi, temsilciStatusu, cb) {
        var q = 'INSERT INTO orgut.temsilci(uyeid, isyeriid, vazife, sira, "secimTarihi", "atamaTarihi", "temsilciStatusu") ' +
            //"VALUES ("+uyeid+", "+isyeriid+", "+vazifeid+", 0,'"+secimTarihi+"', '"+atamaTarihi+"', "+temsilciStatusu+")";
            'VALUES ('+uyeid+', '+isyeriid+', '+vazifeid+', 0,null, null, null)';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    updateTemsilci: function (temsilciid, uyeid, isyeriid, vazifeid, sira, secimTarihi, atamaTarihi, temsilciStatusu, cb) {
        var q = 'UPDATE orgut.temsilci SET ' +
            'uyeid='+uyeid+', ' +
            'isyeriid='+isyeriid+', ' +
            'vazife='+vazifeid+', ' +
            'sira='+sira+' ' +
            //'secimTarihi=\''+secimTarihi+'\', '+
            //'atamaTarihi=\''+atamaTarihi+'\', '+
            //'temsilciStatusu='+temsilciStatusu+', '+
            'WHERE temsilciid='+temsilciid;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getOneByUyeid: function (uyeid, cb) {
        var q = 'select * from orgut.temsilci where uyeid='+uyeid;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    }
};



module.exports = temsilci;