var pg = require('./../Work/Data/Postgre');
var subeMan = {
    getList: function (cb) {
        var q = 'select * from orgut.sube order by isim';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    getOne: function (id, cb) {
        var q = 'select s.*, sd.detay from orgut.sube as s LEFT JOIN orgut.subedetay as sd ON (s.subeid = sd.subeid) WHERE s.subeid=' + id;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    save: function (subeItem, cb) {
        pg.query("select public.insupdsube(" + subeItem.subeid + ", '" + subeItem.isim + "', '" + subeItem.adres + "', '" + subeItem.telefonno + "', '')", function (result) {
            cb && cb(result);
        });
    },
    yonetimsil: function(subeteskilatiid, cb){
        var q = 'delete from orgut.subeteskilati where subeteskilatiid=' + subeteskilatiid;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    deletebyid: function (subeid, cb) {
        var q = 'DELETE FROM orgut.sube where subeid =' + subeid;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    getTeskilatById: function(subeteskilatiid, cb){
        var q = 'select * from orgut.subeteskilati where subeteskilatiid=' + subeteskilatiid;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    yonetimInsert: function(data, cb){
        var q = 'INSERT INTO orgut.subeteskilati(subeid, vazife, uyeid) ' +
        'VALUES ('+data.subeid+', '+data.vazifeSelect+', '+data.uyeid+');';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    yonetimUpdate :function(data, cb){
        var q = 'UPDATE orgut.subeteskilati\n' +
            ' SET subeid='+data.subeid+ ', ' +
            ' vazife='+data.vazifeSelect+', ' +
            ' uyeid='+data.uyeid +
            ' WHERE subeteskilatiid='+data.subeteskilatid+' ;';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    rapor: {
        yonetim: function (subeid, cb) {

            var q = 'select uye.uyeid,isim, uye.soyisim, subeteskilati.vazife,\n' +
                ' (select isim from orgut.isyeri where isyeriid = ((select isyeriid from orgut.hareket where uyeid = subeteskilati.uyeid order by hareketid desc limit 1))) as isyeriisim,\n' +
                ' (select isyeriid from orgut.isyeri where isyeriid = ((select isyeriid from orgut.hareket where uyeid = subeteskilati.uyeid order by hareketid desc limit 1))) as isyeriid,\n'+
                ' (select cins from orgut.hareket where uyeid = subeteskilati.uyeid order by tarih desc limit 1),\n' +
                ' subeteskilatorder.sortorder \n' +
                'from orgut.subeteskilati join orgut.subeteskilatorder using(vazife)\n' +
                'join orgut.uye using(uyeid)\n' +
                'where subeid = ' + subeid + ' order by sortorder';

            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        yonetimEx: function (subeid, cb) {

            var q = 'select subeteskilati.subeteskilatiid, uye.uyeid,isim, uye.soyisim, uye.vatandaslik_no, subeteskilati.vazife,\n' +
                ' (select isim from orgut.isyeri where isyeriid = ((select isyeriid from orgut.hareket where uyeid = subeteskilati.uyeid order by hareketid desc limit 1))) as isyeriisim,\n' +
                ' (select cins from orgut.hareket where uyeid = subeteskilati.uyeid order by tarih desc limit 1),\n' +
                ' subeteskilatorder.sortorder \n' +
                'from orgut.subeteskilati join orgut.subeteskilatorder using(vazife)\n' +
                'join orgut.uye using(uyeid)\n' +
                'where subeid = ' + subeid + ' order by sortorder';

            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        Isyerleri: function (cb) {
            var q = 'select isim, adres, orgutlenmedurum, tasaronvar from orgut.isyeri;';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        Isyerleri: function (subeid, cb) {
            var q = 'select isim, adres, orgutlenmedurum, tasaronvar, calisansayisi from orgut.isyeri where subeid=' + subeid;
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        Istatistik: function (subeid, orgutlenmeDurumId, cb) {
            //var q = 'select i.isim, count(*) as calisan,sum(case when u.kadinerkek=1 then 1 else 0 end) as erkek,sum(case when u.kadinerkek=2 then 1 else 0 end) as kadin from orgut.isyeri i inner join orgut.hareket h on h.isyeriid = i.isyeriid inner join orgut.uye u on h.hareketid = u.sonhareket where i.subeid = ' + subeid + ' and i.orgutlenmedurum = ' + orgutlenmeDurumId + ' and h.cins=1 group by i.isim';

            var q = 'with\n' +
                'sonhareketler as \n' +
                ' (select * from orgut.subesonhareketler_new(' + subeid + ',1,\'01.01.2027\'::date)),\n' +
                'istatistik as\n' +
                '\t(\n' +
                '\tselect \n' +
                '\t\tSUM(CASE kadinerkek WHEN 1 THEN 1 ELSE 0 END) AS erkek,\n' +
                '\t\tSUM(CASE kadinerkek WHEN 2 THEN 1 ELSE 0 END) AS kadin,\n' +
                '\t\tSUM(1) AS sayi,\n' +
                '\t\tisyeriid\n' +
                '\tfrom \n' +
                '\t\tsonhareketler\n' +
                '\t\t\tjoin orgut.uye using(uyeid)\n' +
                '\tgroup by\n' +
                '\t\tisyeriid\n' +
                '\t)\n' +
                '\n' +
                'select \n' +
                '\tisyeri.isim as isyeriisim,istatistik.*\n' +
                'from \n' +
                '\tistatistik\n' +
                '\t\tjoin orgut.isyeri using(isyeriid)\n' +
                'WHERE orgutlenmedurum = ' + orgutlenmeDurumId + '\n' +
                'order by isyeriisim;';

            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        giriscikisobj: {
            countTillDate: function (startDate, subeid, cb) {
                startDate = startDate.split('-');
                var startDay = startDate[2];
                var startMonth = startDate[1];
                var startYear = startDate[0];
                var newStartDate = startMonth + '.' + startDay + '.' + startYear;

                var q = 'select count(*) as sayi from orgut.subesonhareketler_new(' + subeid + ',1,\'' + newStartDate + '\')';

                pg.query(q, function (result) {
                    cb && cb(result);
                });
            },

            untillDate: function (startDate, subeid, cb) {
                startDate = startDate.split('-');
                var startDay = startDate[2];
                var startMonth = startDate[1];
                var startYear = startDate[0];
                var newStartDate = startMonth + '.' + startDay + '.' + startYear;
                var q = 'select \n' +
                    '    sum(case when h.cins =1 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =2 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =3 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =4 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =5 then 1 else 0 end) as sayi\n' +
                    '    from orgut.hareket as h    \n' +
                    '    inner join orgut.isyeri as i on (h.isyeriid = i.isyeriid)\n' +
                    '    inner join orgut.sube as s on (s.subeid = i.subeid)\n' +
                    'where s.subeid= ' + subeid + ' and h.tarih < \'' + newStartDate + '\'';
                pg.query(q, function (result) {
                    cb && cb(result);
                });

            },
            givenInterval_old: function (startDate, endDate, subeid, cb) {
                startDate = startDate.split('.');
                var newStartDate = startDate[1] + '.' + startDate[0] + '.' + startDate[2];
                endDate = endDate.split('.');
                var newEndDate = endDate[1] + '.' + endDate[0] + '.' + endDate[2];
                var q = 'select  extract(year from h.tarih) as yil,extract(month from h.tarih) as ay, i.isim,\n' +
                    '\tsum(case when h.cins =1 then 1 else 0 end) as kayit,\n' +
                    '    sum(case when h.cins =2 then 1 else 0 end) as Istifa,\n' +
                    '    sum(case when h.cins =3 then 1 else 0 end) as "Madde 17",\n' +
                    '    sum(case when h.cins =4 then 1 else 0 end) as "Madde 25",   \n' +
                    '    sum(case when h.cins =5 then 1 else 0 end) as Emekli,\n' +
                    '    \n' +
                    '    sum(case when h.cins =1 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =2 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =3 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =4 then 1 else 0 end) \n' +
                    '    - sum(case when h.cins =5 then 1 else 0 end) as sayi\n' +
                    '    from orgut.hareket as h    \n' +
                    '    inner join orgut.isyeri as i on (h.isyeriid = i.isyeriid)\n' +
                    '    inner join orgut.sube as s on (s.subeid = i.subeid)\n' +
                    'where s.subeid= ' + subeid + ' and h.tarih > \'' + newStartDate + '\' and h.tarih < \'' + newEndDate + ' 23:59:00\'\n' +
                    'group by i.isim, extract(year from h.tarih), extract(month from h.tarih)\n' +
                    'order by extract(year from h.tarih)';

                pg.query(q, function (result) {
                    cb && cb(result);
                });
            },
            givenInterval: function (startDate, endDate, subeid, cb) {

                var q = 'with\n' +
                    '    aylikHareketler as (\n' +
                    '\n' +
                    '    with\n' +
                    '        aylar as (\n' +
                    '            select extract(\'year\' from zaman) as yil,extract(\'month\' from zaman) as ay from generate_series(\'' + startDate + '\'::date, \'' + endDate + '\'::date, \'1 month\') as zaman\n' +
                    '        ),\n' +
                    '        cins as (\n' +
                    '            select * from generate_series(1,5) cins\n' +
                    '        ),\n' +
                    '        adetler as (\n' +
                    '            select\n' +
                    '                extract(year from tarih) as yil,\n' +
                    '                extract(month from tarih) as ay,\n' +
                    '                cins,\n' +
                    '                count(*) as adet,\n' +
                    '                isyeriid\n' +
                    '            from\n' +
                    '                orgut.hareket\n' +
                    '\t\t\tjoin orgut.isyeri using(isyeriid)\n' +
                    '            where\n' +
                    '                subeid=' + subeid + ' and tarih between \'' + startDate + '\'::date and \'' + endDate + '\'::date\n' +
                    '            group by\n' +
                    '                yil,\n' +
                    '                ay,\n' +
                    '                cins,\n' +
                    '                isyeriid\n' +
                    '            order by\n' +
                    '                extract(year from tarih),\n' +
                    '                extract(month from tarih),\n' +
                    '                cins\n' +
                    '        ),\n' +
                    '        aycins as (\n' +
                    '            select * from aylar,cins\n' +
                    '        ),\n' +
                    '        uyesayilari as (\n' +
                    '            select\n' +
                    '                (select count(*) from\n' +
                    '                    orgut.subesonhareketler_new(' + subeid + ',1,  (date( yil::text ||\'-\' || ay::text || \'-01\') + interval \'1 month\' - interval \'1 day\')::date  )) as uyesayisi,\n' +
                    '                    (date( yil::text ||\'-\' || ay::text || \'-01\') + interval \'1 month\' - interval \'1 day\') as  uyesayisigunu,\n' +
                    '                    yil,\n' +
                    '                    ay\n' +
                    '            from\n' +
                    '                aylar\n' +
                    '        )\n' +
                    '\n' +
                    '    select\n' +
                    '            yil,ay,aycins.cins as cins , adet,uyesayisi,uyesayisigunu,isyeriid\n' +
                    '    from\n' +
                    '            aycins\n' +
                    '                     left join adetler using (yil,ay,cins)\n' +
                    '                     join uyesayilari using (yil,ay)\n' +
                    '\n' +
                    ')\n' +
                    '\n' +
                    'select\n' +
                    '    yil as year,ay as month,\n' +
                    '    isyeriid,\n' +
                    '    SUM(CASE cins WHEN 1 THEN adet ELSE 0 END) AS kayit,\n' +
                    '    SUM(CASE cins WHEN 2 THEN adet ELSE 0 END) AS istifa,\n' +
                    '    SUM(CASE cins WHEN 3 THEN adet ELSE 0 END) AS madde17,\n' +
                    '    SUM(CASE cins WHEN 4 THEN adet ELSE 0 END) AS madde25,\n' +
                    '    SUM(CASE cins WHEN 5 THEN adet ELSE 0 END) AS emekli,\n' +
                    '    uyesayisi as kalan,\n' +
                    '    uyesayisigunu,\n' +
                    '    orgut.isyeri.isim as isyeri\n' +
                    '\n' +
                    'from\n' +
                    '    aylikHareketler\n' +
                    '\tjoin orgut.isyeri using(isyeriid)\n' +
                    'group by\n' +
                    '    yil, ay, isyeriid, isim, uyesayisi, uyesayisigunu\n' +
                    'order by\n' +
                    '    yil,ay, isim\n';

                pg.query(q, function (result) {
                    cb && cb(result);
                });
            }
        },
        giriscikisisimli: function (startDate, endDate, subeid, hareketcins, cb) {

            var q = 'select   h.tarih, u.isim as uyeisim , u.soyisim, i.isim, u.vatandaslik_no  from orgut.hareket as h\n' +
                '              inner join orgut.isyeri as i on (h.isyeriid = i.isyeriid)\n' +
                '              inner join orgut.sube as s on (s.subeid = i.subeid)\n' +
                '              left join orgut.uye as u on (h.uyeid  =  u.uyeid)\n' +
                '          where s.subeid=' + subeid + ' and h.tarih > \'' + startDate + '\' and h.tarih < \'' + endDate + ' 23:59:00\' and h.cins =' + hareketcins + '\n' +
                '          order by h.tarih';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        giriscikis: function (startDate, endDate, subeid, cb) {
            var result = {};
            subeMan.rapor.giriscikisobj.givenInterval(startDate, endDate, subeid, function (resultData) {
                subeMan.rapor.giriscikisobj.countTillDate(startDate, subeid, function (startCount) {
                    subeMan.rapor.giriscikisobj.countTillDate(endDate, subeid, function (endCount) {
                        if (typeof cb !== "undefined") {
                            result.tillStart = startCount;
                            result.interval = resultData;
                            result.tillEnd = endCount;
                            cb(result);
                        }
                    });
                });
            });
        },
        tespit: function (startDate, endDate, subeid, cb) {
            startDate = startDate.split('-');
            var startDay = startDate[2];
            var startMonth = startDate[1];
            var startYear = startDate[0];
            var newStartDate = startMonth + '.' + startDay + '.' + startYear;
            endDate = endDate.split('-');
            var endDay = endDate[2];
            var endMonth = endDate[1];
            var endYear = endDate[0];
            var newEndDate = endMonth + '.' + endDay + '.' + endYear;
            var q = 'select i.isim as isyeriisim, s.isim as sendikaisim, tc.tarih, tc.basvurutarihi, tc.iscisayisi, tc.uyesayisi, tc.evraksayi from orgut.tespitcevap as tc\n' +
                'inner join orgut.sendika as s on (s.sendikaid = tc.sendikaid)\n' +
                'inner join orgut.isyeri as i on (i.isyeriid = tc.isyeriid)\n' +
                'inner join orgut.sube as sb on (sb.subeid = i.subeid)\n' +
                'where tarih > \'' + newStartDate + '\' and  tarih < \'' + newEndDate + '\' and sb.subeid = ' + subeid;
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        sendika: function (subeid, cb) {
            var q = 'select distinct on (i.isim)\n' +
                ' i.isim as isyeriisim, s.isim as sendikaisim, tc.tarih,  tc.iscisayisi, tc.uyesayisi from orgut.tespitcevap as tc\n' +
                'inner join orgut.sendika as s on (s.sendikaid = tc.sendikaid)\n' +
                'inner join orgut.isyeri as i on (i.isyeriid = tc.isyeriid)\n' +
                'inner join orgut.sube as sb on (sb.subeid = i.subeid)\n' +
                'where sb.subeid = ' + subeid +
                'order by i.isim, tc.tarih';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        temsilciler: function (subeid, cb) {
            var q = 'select\n' +
                '    u.uyeid, i.isim as isyeriisim, t.vazife, u.isim, u.soyisim, u.vatandaslik_no, i.orgutlenmedurum,\n' +
                '    (select h.cins from orgut.hareket h where h.uyeid = u.uyeid order by tarih desc limit 1) as cins \n'+
                'from\n' +
                '    orgut.temsilci as t\n' +
                '        join orgut.isyeri as i using(isyeriid)\n' +
                '        join orgut.isyeritemsilciorder using(vazife)\n' +
                '        join orgut.uye as u using(uyeid)\n' +
                'where\n' +
                '    subeid='+subeid+'\n' +
                '    and orgutlenmedurum=1\n' +
                'order by\n' +
                '\ti.isim, sortorder, sira';

            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
        subeuyelistesi: function (subeid, cb) {
            var q = 'select u.uyeid, u.soyisim, u.isim, u.babaismi, u.kadinerkek, u.vatandaslik_no,i.isim as isyeriIsim, i.isyeriid, h.tarih, h.kararno, h.dogrulama_kodu, ' +
                '(select cins from orgut.hareket where uyeid = u.uyeid and hareketid=h.hareketid order by tarih desc limit 1) as sonharekettipi, ' +
                'i.orgutlenmedurum ' +
                'from orgut.uye as u ' +
                'inner join orgut.hareket as h on (h.hareketid = u.sonhareket) ' +
                'inner join orgut.isyeri as i on (h.isyeriid=i.isyeriid) ' +
                'inner join orgut.sube as s on (i.subeid = s.subeid)' +
                'where s.subeid =' + subeid + ' and (select cins from orgut.hareket where uyeid = u.uyeid and hareketid=h.hareketid order by tarih desc limit 1) =1 ' +
                'order by i.isyeriid desc';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },

    }
};
module.exports = subeMan;