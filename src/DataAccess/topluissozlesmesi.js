var pg = require('./../Work/Data/Postgre');


var tis = {
    getListByIsyeriId : function (isyeriid, cb) {
        var q = 'select * from orgut."TopluIsSozlesme" where isyeriid = ' + isyeriid;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    insert: function (tis, cb) {
        var q = 'INSERT INTO orgut."TopluIsSozlesme"(' +
            '"Title", ' +
            '"SezonNumarasi", ' +
            '"Startdate", ' +
            '"Enddate", ' +
            '"Document", ' +
            '"Description", ' +
            'isyeriid)\n' +
            ' VALUES (' +
            '\''+tis.title+'\', ' +
            ''+tis.sezonno+', ' +
            '\''+tis.startdate+'\', ' +
            '\''+tis.enddate+'\', ' +
            '\''+tis.document+'\', \''+tis.description+'\', ' +
            ' '+tis.isyeriid+');';
        pg.query(q, function (result) {
            cb && cb(result);
        });

    },
    update: function (tis, cb) {
        var q= 'UPDATE orgut."TopluIsSozlesme"\n' +
            ' SET ' +
            '"Title"=\''+tis.title+'\', ' +
            '"SezonNumarasi"=\''+tis.sezonno+'\', ' +
            '"Startdate"=\''+tis.startdate+'\', ' +
            '"Enddate"=\''+tis.enddate+'\', ' +
            '"Document"=\''+tis.document+'\', ' +
            '"Description"=\''+tis.description+'\', ' +
            'isyeriid='+tis.isyeriid+' \n' +
            ' WHERE id= '+tis.id+';';

        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    delete: function (tisid, cb) {
        var q = 'DELETE FROM orgut."TopluIsSozlesme" WHERE id= '+tisid+';';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    }

};
module.exports = tis;