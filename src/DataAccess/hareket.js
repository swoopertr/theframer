var pg = require('./../Work/Data/Postgre');

var hareket = {
    getById: function (hareketid, cb) {
        var q = 'select * from orgut.hareket where hareketid = ' + hareketid;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    updateHareket:
        function (hareketid, uyeid, hareketcinsi, tarih, kararno, isyeriid, noter, notertarih,
                  noteryevmiye, kayitsirano, dogrulama_kodu, saat, aciklama, cb) {
            //var arrSaat = saat.split(':');
            /*   if (saat==='00:00'){
                   tarih = tarih + ' 04:00';
               } else {
                   if (parseInt(arrSaat[0]) < 4){
                       tarih = tarih + ' 00:00';
                   }else {
                       var hour = parseInt(arrSaat[0]);
                       hour+=4;
                       tarih = tarih + ' '+hour+':00';
                   }
               }*/

            tarih = tarih + ' ' + saat + ':00';
            if (notertarih == "") {
                notertarih = "null";
            } else {
                var notertarihArr = notertarih.split('.');
                notertarih = "'" + notertarihArr[1] + '.' + notertarihArr[0] + '.' + notertarihArr[2] + "'";
            }

            aciklama = aciklama.replace(new RegExp("'", 'g'), " ");
            noter = noter.replace(new RegExp("'", 'g'), " ");

            var q = "UPDATE orgut.hareket " +
                "SET " +
                "uyeid=" + uyeid + "," +
                "cins=" + hareketcinsi + ", " +
                "tarih='" + tarih + "', " +
                "kararno='" + kararno + "', " +
                "isyeriid=" + isyeriid + ", " +
                "noter='" + noter + "', " +
                "notertarih=" + notertarih + ", " +
                "dogrulama_kodu='" + dogrulama_kodu + "', " +
                "aciklama='" + aciklama + "' " +
                "WHERE hareketid =" + hareketid;
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
    insertHareket: function (uyeid, hareketcinsi, tarih, kararno, isyeriid, noter, notertarih, noteryevmiye,
                             tf_kaysirno, tf_onay, tf_oncekiisyeriid, tf_sendikaid, kayitsirano, dogrulama_kodu, saat, aciklama, cb) {

            if (notertarih == "") {
                notertarih = "null";
            } else {
                var notertarihArr = notertarih.split('.');
                notertarih = "'" + notertarihArr[1] + '.' + notertarihArr[0] + '.' + notertarihArr[2] + "'";
            }
            var tarihArr = tarih.split('-');
            tarih = tarihArr[1] + '.' + tarihArr[2] + '.' + tarihArr[0];//todo: before send push, change month and day order
            tarih = tarih + ' ' + saat + ':00';

            aciklama = aciklama.replace(new RegExp("'", 'g'), " ");
            noter = noter.replace(new RegExp("'", 'g'), " ");
            var q = "INSERT INTO orgut.hareket(uyeid, cins, tarih, kararno, isyeriid, dogrulama_kodu, noter, notertarih, aciklama) " +
                "VALUES (" + uyeid + ", " + hareketcinsi + ", '" + tarih + "', '" + kararno + "', " + isyeriid + ", '" + dogrulama_kodu + "', '" + noter + "', " + notertarih + ", '" + aciklama + "') returning hareketid;";
            pg.query(q, function (result) {
                if (result.hasOwnProperty('err')) {
                    cb(-1);
                } else {
                    cb && cb(result);
                }

            });
        },
    getLastHareketByUyeID: function (uyeid, cb) {
            var q = 'select * from orgut.hareket where uyeid = ' + uyeid + ' order by tarih desc limit 1';
            pg.query(q, function (result) {
                cb && cb(result);
            });
        },
    deleteOneById: function (hareketid, cb) {
        var q = 'delete from orgut.hareket where hareketid =' + hareketid;
        pg.query(q, function (result) {
            if (typeof cb !== "undefined") {
                cb(!result.hasOwnProperty('err'));
            }
        });
    },
    updateNullSonHareket: function (hareketid, cb) {
        var q = 'update orgut.uye set sonhareket =null where sonhareket =' + hareketid;
        pg.query(q, function (result) {
            if (typeof cb !== "undefined") {
                cb(!result.hasOwnProperty('err'));
            }
        });
    }
};

module.exports = hareket;