var pg  = require('./../Work/Data/Postgre');

var tespit = {
    getAll: function (cb) {
        var q = 'select i.isim, tc.* from orgut.tespitcevap as tc \n' +
            ' inner join orgut.isyeri i on (tc.isyeriid = i.isyeriid) order by tc.tespitcevapid desc';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getone:function (tespitid, cb) {
        var q = 'select * from orgut.tespitcevap where tespitcevapid ='+tespitid;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    insert: function (tespit, cb) {
        var q = 'INSERT INTO orgut.tespitcevap(sendikaid, isyeriid, basvurutarihi, iscisayisi, uyesayisi, tarih, evraksayi, tsbaslangictarih, tsbitistarih) ' +
            ' VALUES ('+tespit.sendikaselect+', '+tespit.isyeriid+', \''+tespit.basvuruTarihi+'\', ' +
            ''+tespit.calisansayisi+', '+tespit.uyesayisi+', \''+tespit.evraktarihi+'\', \''+tespit.evraksayisi +'\', null, null);';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    delete: function(tespitid, cb){
        var q = 'DELETE FROM orgut.tespitcevap WHERE tespitcevapid='+tespitid;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    update: function (tespit, cb) {
        var q = 'UPDATE orgut.tespitcevap ' +
            'SET sendikaid='+tespit.sendikaselect+', ' +
            'isyeriid='+tespit.isyeriid+', ' +
            'basvurutarihi=\''+tespit.basvuruTarihi+'\', ' +
            'iscisayisi='+tespit.calisansayisi+', ' +
            'uyesayisi='+tespit.uyesayisi+', ' +
            'tarih=\''+tespit.evraktarihi+'\', ' +
            'evraksayi=\''+tespit.evraksayisi+'\', ' +
            'tsbaslangictarih=null, ' +
            'tsbitistarih=null ' +
            ' WHERE tespitcevapid='+tespit.tespitid+';';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    }
};
module.exports = tespit;