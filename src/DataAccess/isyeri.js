var pg = require('./../Work/Data/Postgre');
var uyeMan = require('./uye');
var isyeri = {
    getTotalCount: function (cb) {
        pg.query('SELECT count(*) from orgut.isyeri', function (data) {
            cb && cb(data);
        });
    },
    getlist: function (cb) {
        pg.query('SELECT * from orgut.isyeri', function (data) {
            cb && cb(data);
        });
    },
    getAllListWithSubeInfo: function (cb) {
        var q = "select i.isyeriid, i.isim as isyeriisim, s.subeid, s.isim as subeisim from orgut.isyeri as i inner join orgut.sube as s on (i.subeid = s.subeid) order by i.isyeriid";
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getAllListWithorgulenmeDurumAndsubeInfo: function (cb) {
        var q = 'select i.isyeriid, i.orgutlenmedurum,  i.isim as isyeriisim, s.subeid, s.isim as subeisim from orgut.isyeri as i inner join orgut.sube as s on (i.subeid = s.subeid) order by i.isyeriid';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getListForScreen: function (isim, orgutDurum, taseron, subeid, pageNo, pageCount, cb) {

        var whereClause = 'where ';
        var useWhere = false;
        if (isim !== '') {
            useWhere = true;
            whereClause += "isim like '" + isim + "%' ";
        }
        if (orgutDurum !== '') {
            useWhere = true;
            (isim !== '') ? whereClause += "and " : "";
            whereClause += "orgutlenmedurum = " + orgutDurum + " ";
        }
        if (taseron !== '') {
            useWhere = true;
            (isim !== '' || orgutDurum !== '') ? whereClause += "and " : "";
            whereClause += "tasaronvar =" + taseron + " ";
        }
        if (subeid !== '') {
            useWhere = true;
            (isim !== '' || orgutDurum !== '' || taseron !== '') ? whereClause += "and " : "";
            whereClause += "subeid=" + subeid + " ";
        }

        var q = 'select isim, isyeriid, subeid, orgutlenmedurum, tasaronvar from orgut.isyeri ' +
            (useWhere ? whereClause : '') +
            ' order by isim offset ' + ((pageNo - 1) * pageCount) + ' limit ' + pageCount;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getListCountForScreen: function (isim, orgutDurum, taseron, subeid, pageNo, cb) {

        var whereClause = 'where ';
        var useWhere = false;
        if (isim !== '') {
            useWhere = true;
            whereClause += "isim like '" + isim + "%' ";
        }
        if (orgutDurum !== '') {
            useWhere = true;
            (isim !== '') ? whereClause += "and " : "";
            whereClause += "orgutlenmedurum = " + orgutDurum + " ";
        }
        if (taseron !== '') {
            useWhere = true;
            (isim !== '' || orgutDurum !== '') ? whereClause += "and " : "";
            whereClause += "tasaronvar =" + taseron + " ";
        }
        if (subeid !== '') {
            useWhere = true;
            (isim !== '' || orgutDurum !== '' || taseron !== '') ? whereClause += "and " : "";
            whereClause += "subeid=" + subeid + " ";
        }

        var q = 'select count(isim) from orgut.isyeri ' +
            (useWhere ? whereClause : '');
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getAllPagingData: function (pageNo, itemCount, cb) {
        var q = 'select isim, isyeriid, subeid, orgutlenmedurum, tasaronvar from orgut.isyeri ' +
            ' order by isim offset ' + ((pageNo - 1) * itemCount) + ' limit ' + itemCount;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getListBySubeId: function (subeid, cb) {
        var q = 'SELECT * from orgut.isyeri where subeid=' + subeid;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getListById: function (id, cb) {
        var q = 'SELECT * from orgut.isyeri where subeid=' + id;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getListByIds: function (ids, startDate, endDate, cb) {

        var q = 'SELECT  h.tarih, s.isim as subeisim, i.* from orgut.isyeri as i ' +
            'INNER JOIN orgut.hareket as h on i.isyeriid = h.isyeriid ' +
            'INNER JOIN orgut.sube as s on i.subeid = s.subeid where i.isyeriid in(' + ids + ') ' +
            'and h.tarih > \'' + startDate + '\' and h.tarih < \'' + endDate + ' 23:59:59\';';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getone: function (id, cb) {
        var q = 'SELECT * from orgut.isyeri where isyeriid = ' + id;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getUyebyisyerid: function (id, cb) {
        uyeMan.getUyelerByIsyeriid(id, function (data) {
            cb && cb(data);
        });
    },
    isyeriistatistikByisyeriid: function (isyeriid, cb) {
        var q = 'select i.isim, count(*) as calisan, ' +
            'sum(case when u.kadinerkek=1 then 1 else 0 end) as erkek, ' +
            'sum(case when u.kadinerkek=2 then 1 else 0 end) as kadin from orgut.isyeri i  ' +
            'inner join orgut.hareket h on h.isyeriid = i.isyeriid  ' +
            'inner join orgut.uye u on h.hareketid = u.sonhareket  ' +
            'where i.isyeriid = ' + isyeriid + ' and h.cins = 1 group by i.isim';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    rapor: {
        isyerigiriscikisObj: {
            untillDate: function (startDate, isyeriid, cb) {
                startDate = startDate.split('-');
                var day = startDate[2];
                var month = startDate[1];
                var year = startDate[0];
                var newStartDate = month + '.' + day + '.' + year;
                var q = 'SELECT count(*) from orgut.isyerisonhareketler_new(' + isyeriid + ', 1, \'' + newStartDate + '\');';
                pg.query(q, function (result) {
                    cb && cb(result);
                });
            },
            givenInterval: function (startDate, endDate, isyeriid, cb) {
                startDate = startDate.split('-');
                var day = startDate[2];
                var month = startDate[1];
                var year = startDate[0];
                var newStartDate = month + '.' + day + '.' + year;
                endDate = endDate.split('-');
                var endDay = endDate[2];
                var endMonth = endDate[1];
                var endYear = endDate[0];
                var newEndDate = endMonth + '.' + endDay + '.' + endYear;
                var q = 'with\n' +
                    '    aylikHareketler as (\n' +
                    '    with\n' +
                    '        aylar as (\n' +
                    '            select extract(\'year\' from zaman) as yil,extract(\'month\' from zaman) as ay from generate_series(\'' + newStartDate + '\'::date, \'' + newEndDate + '\'::date, \'1 month\') as zaman\n' +
                    '        ),\n' +
                    '        cins as (\n' +
                    '            select * from generate_series(1,5) cins\n' +
                    '        ),\n' +
                    '        adetler as (\n' +
                    '            select\n' +
                    '                extract(year from tarih) as yil,\n' +
                    '                extract(month from tarih) as ay,\n' +
                    '                cins,\n' +
                    '                count(*) as adet\n' +
                    '            from\n' +
                    '                orgut.hareket\n' +
                    '            where\n' +
                    '                isyeriid=' + isyeriid + ' and tarih between \'' + newStartDate + '\' and \'' + newEndDate + '\'\n' +
                    '            group by\n' +
                    '                extract(year from tarih),\n' +
                    '                extract(month from tarih),\n' +
                    '                cins\n' +
                    '            order by\n' +
                    '                extract(year from tarih),\n' +
                    '                extract(month from tarih),\n' +
                    '                cins\n' +
                    '        ),\n' +
                    '        aycins as (\n' +
                    '            select * from aylar,cins\n' +
                    '        ),\n' +
                    '        uyesayilari as (\n' +
                    '            select\n' +
                    '                (select count(*) from\n' +
                    '                    orgut.isyerisonhareketler_new(' + isyeriid + ',1,  (date( yil ||\'-\' || ay || \'-01\') + interval \'1 month\' - interval \'1 day\')::date  )) as uyesayisi,\n' +
                    '                    (date( yil ||\'-\' || ay || \'-01\') + interval \'1 month\' - interval \'1 day\') as  uyesayisigunu,\n' +
                    '                    yil,\n' +
                    '                    ay\n' +
                    '            from\n' +
                    '                aylar\n' +
                    '        )\n' +
                    '\n' +
                    '    select\n' +
                    '            yil,ay,aycins.cins as cins , adet,uyesayisi,uyesayisigunu\n' +
                    '    from\n' +
                    '            aycins\n' +
                    '                     left join adetler using (yil,ay,cins)\n' +
                    '                     join uyesayilari using (yil,ay)\n' +
                    '\n' +
                    ')\n' +
                    '\n' +
                    'select\n' +
                    '    yil as year, ay as month,\n' +
                    '    SUM(CASE cins WHEN 1 THEN adet ELSE 0 END) AS kayit,\n' +
                    '    SUM(CASE cins WHEN 2 THEN adet ELSE 0 END) AS istifa,\n' +
                    '    SUM(CASE cins WHEN 3 THEN adet ELSE 0 END) AS madde17,\n' +
                    '    SUM(CASE cins WHEN 4 THEN adet ELSE 0 END) AS madde25,\n' +
                    '    SUM(CASE cins WHEN 5 THEN adet ELSE 0 END) AS emekli,\n' +
                    '    uyesayisi as kalan,\n' +
                    '    uyesayisigunu\n' +
                    '\n' +
                    'from\n' +
                    '    aylikHareketler\n' +
                    'group by\n' +
                    '    yil,ay,uyesayisi,uyesayisigunu\n' +
                    'order by\n' +
                    '    yil,ay\n';

                pg.query(q, function (result) {
                    cb && cb(result);
                });
            }
        },
        isyerigiriscikis: function (startDate, endDate, isyeriid, cb) {
            var result = {};
            startDate = startDate;
            endDate = endDate;
            isyeri.rapor.isyerigiriscikisObj.untillDate(startDate, isyeriid, function (result1) {
                isyeri.rapor.isyerigiriscikisObj.givenInterval(startDate, endDate, isyeriid, function (result2) {
                    isyeri.rapor.isyerigiriscikisObj.untillDate(endDate, isyeriid, function (result3) {
                        result.tillStart = result1;
                        result.interval = result2;
                        result.tillEnd = result3;
                        cb(result);
                    });
                });
            });
        },
        isyeriGirisCikisIsimli: function (isyeriid, startDate, endDate, hareketTipi, cb) {

            var q = 'select h.tarih, u.isim as uyeisim , u.soyisim, u.vatandaslik_no, ' +
                '(select cins from orgut.hareket where uyeid=u.uyeid order by tarih desc limit 1) as hareketcinsi ' +
                'from orgut.hareket as h\n' +
                'inner join orgut.isyeri as i on (h.isyeriid = i.isyeriid)\n' +
                'inner join orgut.sube as s on (s.subeid = i.subeid)\n' +
                'left join orgut.uye as u on (h.uyeid  =  u.uyeid)\n' +
                'where i.isyeriid=' + isyeriid + ' and h.tarih > \'' + startDate + '\' and h.tarih < \'' + endDate + ' 23:59:00\' and h.cins =' + hareketTipi + '\n' +
                'order by h.tarih';
            pg.query(q, function (data) {
                cb && cb(data);
            });
        },
        isyeriorgut: function (isyeriid, cb) {
            var q = 'select\n' +
                '    u.uyeid,t.temsilciid, i.isim as isyeriisim, t.vazife, u.isim, u.soyisim, u.vatandaslik_no,\n' +
                '(select cins from orgut.hareket where uyeid=u.uyeid ORDER BY tarih desc LIMIT 1) as cins, t.sira \n'+
                'from \n' +
                '    orgut.temsilci as t\n' +
                '        join orgut.isyeritemsilciorder using(vazife)\n' +
                '        join orgut.uye as u using(uyeid)\n' +
                '        join orgut.isyeri as i using(isyeriid)\n'+
                'where\n' +
                '    isyeriid = ' + isyeriid + '\n' +
                'order by\n' +
                '    sortorder,vazife,sira';
            /*var q = 'select u.uyeid,t.temsilciid, i.isim as isyeriisim, t.vazife, u.isim, u.soyisim, u.vatandaslik_no,\n' +
                '  (select cins from orgut.hareket where uyeid=u.uyeid ORDER BY hareketid DESC LIMIT 1) as cins , t.sira \n' +
                'from orgut.temsilci t\n' +
                ' inner join orgut.isyeri i on (i.isyeriid = t.isyeriid)\n' +
                ' inner join orgut.sube s on (s.subeid = i.subeid)\n' +
                ' inner join orgut.uye u on (t.uyeid = u.uyeid)\n' +
                ' inner join orgut.isyeritemsilciorder ito on (ito.vazife= t.vazife) \n' +
                ' where i.isyeriid=' + isyeriid + ' order by ito.vazife, ito.sortorder, t.sira;';*/
            pg.query(q, function (data) {
                cb && cb(data);
            });
        },
        tespit: function (isyeriid, startDate, endDate, cb) {
            //startDate = startDate.split('.');
            //var newStartDate = startDate[1] + '.' + startDate[0] + '.' + startDate[2];
            //endDate = endDate.split('.');
            //var newEndDate = endDate[1] + '.' + endDate[0] + '.' + endDate[2];
            var q = 'select t.tespitcevapid, s.isim, t.tarih, t.iscisayisi, t.uyesayisi, t.basvurutarihi from orgut.tespitcevap t\n' +
                'inner join orgut.sendika s on (t.sendikaid = s.sendikaid)\n' +
                'where isyeriid=' + isyeriid + ' and tarih > \'' + startDate + '\' and tarih < \'' + endDate + '\' order by t.basvurutarihi';
            pg.query(q, function (data) {
                cb && cb(data);
            });
        }

    },
    save: function (data, cb) {
        data.tisBaslangic = data.tisBaslangic == '' ? 'infinity' : data.tisBaslangic;
        data.tisBitis = data.tisBitis == '' ? 'infinity' : data.tisBitis;
        data.tisImza = data.tisImza == '' ? 'infinity' : data.tisImza;
        data.calisansayisi = (data.calisansayisi == "") ? "0" : data.calisansayisi;
        var q = 'INSERT INTO orgut.isyeri(' +
            'subeid, isim, kamuozel, bcmdosyano, sskno, telefonno, ' +
            'isverensendikasi, ustkurulus, grpdahilmi, uretimkonusu, adres, ' +
            'orgutlenmedurum, tasaronvar, sendikaid, email, faxno, calisansayisi) ' +
            'VALUES(' + data.selectSube + ', \'' + data.isim + '\', ' + data.selectKamu + ', \'' + data.bcmdosyano + '\', \'' + data.sskno + '\', \'' + data.telefonno + '\',' +
            ' \'' + data.selectIsverenSendika + '\', \'' + data.ustkurulus + '\', ' + data.selectGrpDahilmi + ', \'' + data.uretimkonusu + '\', \'' + data.adres + '\',' +
            ' ' + data.selectOrgut + ', ' + data.selectTaseron + ', ' + data.sendikaSelect + ', ' +
            ' \'' + data.email + '\', \'' + data.faxno + '\', ' + data.calisansayisi + ') returning isyeriid;';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    update: function (data, cb) {
        data.calisansayisi = (data.calisansayisi == '') ? 0 : data.calisansayisi;
        var q = 'UPDATE orgut.isyeri SET ' +
            ' subeid=' + data.selectSube + ', ' +
            ' isim=\'' + data.isim + '\', ' +
            ' kamuozel=' + data.selectKamu + ', ' +
            ' bcmdosyano=\'' + data.bcmdosyano + '\', ' +
            ' sskno=\'' + data.sskno + '\', ' +
            ' telefonno=\'' + data.telefonno + '\', ' +
            ' isverensendikasi=\'' + data.selectIsverenSendika + '\', ' +
            ' ustkurulus=\'' + data.ustkurulus + '\', ' +
            ' grpdahilmi=' + data.selectGrpDahilmi + ', ' +
            ' uretimkonusu=\'' + data.uretimkonusu + '\', ' +
            ' adres=\'' + data.adres + '\', ' +
            ' orgutlenmedurum=' + data.selectOrgut + ', ' +
            ' tasaronvar=' + data.selectTaseron + ', ' +
            ' sendikaid=' + ((data.sendikaSelect == "0") ? 'null' : data.sendikaSelect) + ', ' +
            ' calisansayisi=' + data.calisansayisi + '  ,' +
            ' email=\'' + data.email + '\',' +
            ' faxno=\'' + data.faxno + '\'' +
            ' WHERE isyeriid=' + data.isyeriid + ';';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    updateIsyeriTemsilcisi: function (data, cb) {
        var q = 'UPDATE orgut.isyeri SET ' +
            'dkisverentems1=\'' + data.dkisverentems1 + '\', ' +
            'dkisverentems2=\'' + data.dkisverentems2 + '\', ' +
            'dkisverentemsydk1=\'' + data.dkisverentemsydk1 + '\', ' +
            'dkisverentemsydk2=\'' + data.dkisverentemsydk2 + '\' ' +
            ' WHERE isyeriid=' + data.isyeriid + ';';
        pg.query(q, function (result) {
            cb && cb();
        });
    },
    updateCalisanSayisiByIsyeriId: function (data, cb) {
        var q = 'UPDATE orgut.isyeri SET calisansayisi=' + data.calisansayisi + ' where isyeriid=' + data.isyeriid;
        pg.query(q, function (result) {
            cb && cb();
        });
    },
    isyeriTemsilcileri: function (isyeriid, cb) {
        var q = 'select dkisverentems1, dkisverentems2, dkisverentemsydk1, dkisverentemsydk2 from orgut.isyeri WHERE isyeriid=' + isyeriid + ';';
        pg.query(q, function (data) {
            cb && cb(data);

        });
    },
    deleteByid: function (isyeriid, cb) {
        var q = 'delete from orgut.isyeri WHERE isyeriid=' + isyeriid + ';';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    tespitCevapNullerbyIsyeriid: function (isyeriid, cb) {
        var q = 'update orgut.tespitcevap set isyeriid = null where isyeriid =' + isyeriid
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    checkBcmDosyanoForIsyeri: function (bcmdosyano, cb) {
        var q = 'select * from orgut.isyeri where bcmdosyano=\'' + bcmdosyano + '\'';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    }
};

module.exports = isyeri;