var hareketManager = require('./hareket');
var pg = require('./../Work/Data/Postgre');
var uye = {
    getAll: function (cb) {
        var q = 'SELECT * from orgut.uye;';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    getById: function (id, cb) {
        var q = 'select * from orgut.uye where uyeid=' + id;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    updateLastMove: function(uyeid, cb){
        hareketManager.getLastHareketByUyeID(uyeid, function (lastRow) {
            if (lastRow){
                try {
                    lastRow = JSON.parse(lastRow);
                    var lastHareketId = lastRow.hareketid;
                    var lastcins = lastRow.cins;
                    var q = 'update orgut.uye set sonhareket=' + lastHareketId + ', sonharekettipi=' + lastcins + ' where uyeid=' + uyeid;
                    pg.query(q, function (result) {
                        cb && cb(result);
                    });
                }catch (e) {
                    console.log('error : '+ e.toString());
                }finally {
                    cb && cb("ok");
                }
            }

        });
    },
    getUyelerByIsyeriid: function (isyeriid, cb) {
        var q = 'select u.uyeid, u.soyisim, u.isim, u.telefon, u.babaismi, u.kadinerkek, u.vatandaslik_no,i.isim as isyeriIsim, i.isyeriid, h.tarih, h.kararno, h.dogrulama_kodu, ' +
            '(select cins from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) as sonharekettipi, ' +
            'u.telefon ' +
            'from orgut.uye as u ' +
            'inner join orgut.hareket as h on (h.hareketid = u.sonhareket) ' +
            'inner join orgut.isyeri as i on (h.isyeriid=i.isyeriid) ' +
            'where i.isyeriid =' + isyeriid + ' and (select cins from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) =1 ' +
            'order by u.isim, u.soyisim collate "tr_TR"';

        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    getTotalCount: function (cb) {
        var q = 'select count(*) from orgut.uye';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    getAllForScreen: function (pageIndex, itemCount, cb) {
        if (!itemCount){
            itemCount = 25;
        }
        var q = 'select u.uyeid, u.soyisim, u.isim, u.babaismi,u.vatandaslik_no, ' +
            '(select isim from orgut.isyeri where isyeriid= ((select isyeriid from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1)) ) as isyeriIsim, ' +
            '(select cins from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) as sonharekettipi, ' +
            '(select tarih from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) as sonharekettarihi ' +
            'from orgut.uye as u order by u.soyisim  offset ' + ((pageIndex - 1) * 25) + ' limit ' + itemCount + ';';

        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    searchinuye: function(isim, soyisim, isyeriisim, cb){
        isim = isim.toUpperCase();
        soyisim = soyisim.toUpperCase();
        isyeriisim = isyeriisim.toUpperCase();

        var q = "select uyeid, isim, soyisim, \n" +
            "(select isim from orgut.isyeri where isyeriid= ((select isyeriid from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1)) ) as isyeriIsim \n" +
            "from orgut.uye as u where \n" +
            "isim like '" + isim + "%' and soyisim like '" + soyisim + "%' and\n" +
            "(select isim from orgut.isyeri where isyeriid= ((select isyeriid from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1)) ) like '" + isyeriisim + "%' \n" +
            "limit 10";

        pg.query(q, function (result) {
            cb && cb(result);
        });

    },
    searchAllForScreen: function (isim, soyisim, babaismi, vatNo, isyeriiism, sonharekettipi, pageIndex, itemCount, cb) {
        isim = isim.toUpperCase();
        soyisim = soyisim.toUpperCase();
        babaismi = babaismi.toUpperCase();
        vatNo = vatNo.toUpperCase();
        isyeriiism = isyeriiism.toUpperCase();


        var whereClause = 'where ';

        //'where (prmIsim is null or u.isim = prmIsim) ' +
        if (isim !== '') {
            whereClause += "isim like '" + isim + "%' ";
        }
        if (soyisim !== '') {
            (isim !== '') ? whereClause += "and " : "";
            whereClause += "soyisim like '" + soyisim + "%' ";
        }
        if (babaismi !== '') {
            (isim !== '' || soyisim !== '') ? whereClause += "and " : "";
            whereClause += "babaismi like '" + babaismi + "%' ";
        }
        if (vatNo !== '') {
            (isim !== '' || soyisim !== '' || babaismi !== '') ? whereClause += "and " : "";
            whereClause += "vatandaslik_no like '%" + vatNo + "%' ";
        }
        if (isyeriiism !== '') {
            (isim !== '' || soyisim !== '' || babaismi !== '' || vatNo !== '') ? whereClause += "and " : "";
            whereClause += "isyeriIsim like '" + isyeriiism + "%' ";
        }
        if (sonharekettipi != 0) {
            (isim !== '' || soyisim !== '' || babaismi !== '' || vatNo !== '' || isyeriiism !== '') ? whereClause += "and " : "";
            whereClause += "sonharekettipi =" + sonharekettipi;
        }
        if (whereClause === 'where ') {
            whereClause = '';
        }
        //taking last move for memeber.
        var q = 'select * from (select u.uyeid, u.soyisim, u.isim, u.babaismi,u.vatandaslik_no,\n' +
            '  (select isim from orgut.isyeri where isyeriid= ((select isyeriid from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1)) ) as isyeriIsim,\n' +
            '  (select cins from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) as sonharekettipi,\n' +
            '  (select tarih from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) as sonharekettarihi ' +
            'from orgut.uye as u) tbl ' +
            whereClause +
            ' order by soyisim offset ' + ((pageIndex - 1) * itemCount) + ' limit ' + itemCount;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    searchAllForScreenCount: function (isim, soyisim, babaismi, vatNo, isyeriiism, sonharekettipi, cb) {
        isim = isim.toUpperCase();
        soyisim = soyisim.toUpperCase();
        babaismi = babaismi.toUpperCase();
        vatNo = vatNo.toUpperCase();
        isyeriiism = isyeriiism.toUpperCase();


        var whereClause = 'where ';

        //'where (prmIsim is null or u.isim = prmIsim) ' +
        if (isim !== '') {
            whereClause += "isim like '" + isim + "%' ";
        }
        if (soyisim !== '') {
            (isim !== '') ? whereClause += "and " : "";
            whereClause += "soyisim like '" + soyisim + "%' ";
        }
        if (babaismi !== '') {
            (isim !== '' || soyisim !== '') ? whereClause += "and " : "";
            whereClause += "babaismi like '" + babaismi + "%' ";
        }
        if (vatNo !== '') {
            (isim !== '' || soyisim !== '' || babaismi !== '') ? whereClause += "and " : "";
            whereClause += "vatandaslik_no like '%" + vatNo + "%' ";
        }
        if (isyeriiism !== '') {
            (isim !== '' || soyisim !== '' || babaismi !== '' || vatNo !== '') ? whereClause += "and " : "";
            whereClause += "isyeriIsim like '" + isyeriiism + "%' ";
        }
        if (sonharekettipi != 0) {
            (isim !== '' || soyisim !== '' || babaismi !== '' || vatNo !== '' || isyeriiism !== '') ? whereClause += "and " : "";
            whereClause += "sonharekettipi =" + sonharekettipi;
        }
        if (whereClause === 'where ') {
            whereClause = '';
        }
        //taking last move for memeber.
        var q = 'select count(isim) from (select u.uyeid, u.soyisim, u.isim, u.babaismi,u.vatandaslik_no,\n' +
            '  (select isim from orgut.isyeri where isyeriid= ((select isyeriid from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1)) ) as isyeriIsim,\n' +
            '  (select cins from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) as sonharekettipi,\n' +
            '  (select tarih from orgut.hareket where uyeid = u.uyeid order by tarih desc limit 1) as sonharekettarihi ' +
            'from orgut.uye as u) tbl ' + whereClause ;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    getUyeHareketbyId: function (uyeid, cb) {
        var q = 'select h.hareketid, h.tarih, h.cins, h.kararno, i.isim,i.isyeriid, h.noter, h.notertarih, ' +
            'h.noteryevmiye, h.kayitsirano, h.dogrulama_kodu, h.aciklama from orgut.hareket h  ' +
            'inner join orgut.isyeri i on (h.isyeriid = i.isyeriid) ' +
            'where uyeid = ' + uyeid + ' order by tarih';
        pg.query(q, function (result) {
            cb && cb(result);

        });
    },
    updateSonHareket: function (uyeid, hareketid, hareketCins, cb) {
        var q = 'update orgut.uye set sonhareket =' + hareketid + ', sonharekettipi=' + hareketCins + ' where uyeid =' + uyeid;
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    update: function (data, cb) {
        var strDtYil = data.dogumyili.slice(0, 4);
        for (let dataKey in data) {
            if(typeof data[dataKey] === "string"){
                if(data[dataKey].indexOf("'") != -1){
                    data[dataKey] =data[dataKey].replace(new RegExp("'", 'g'), " ");
                }
            }
        }
        var q = 'UPDATE orgut.uye SET isim=\'' + data.isim + '\', soyisim=\'' + data.soyisim + '\', sskno=\'' + data.sskno + '\','
            + 'dogumyeri=\'' + data.dogumyeri + '\', dogumyili=\'' + strDtYil + '\', '
            + 'nkil=\'' + data.nkil + '\', nkilce=\'' + data.nkilce + '\','
            + 'nkkoy=\'' + data.nkkoy + '\', nksayfa=\'' + data.nksayfa + '\', nkcilt=\'' + data.nkcilt + '\', babaismi=\'' + data.babaisim + '\','
            + 'anaismi=\'' + data.anaisimi + '\', nkhane=\'' + data.nkhane + '\', kadinerkek=' + data.sexSelect + ', '
            + 'nkmahalle=\'' + data.nkmahalle + '\', ucret=null, cocuksayisi=null, uyebild=null, '
            + 'kangrubu=' + data.bloodSelect + ', egitim=' + data.eduSelect + ', tf_dtar=\'' + data.dogumyili + '\', vatandaslik_no=\'' + data.vatNo + '\', '
            + '"aciklama"=\'' + data.aciklama + '\', '
            + 'telefon=\'' + data.telefon + '\' WHERE uyeid=' + data.uyeid + ';';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    ""},
    updateUyeEmail : function(data, cb, errcb){
        var q = 'UPDATE orgut.uye SET email=\'' + data.eposta + '\' where vatandaslik_no=\''+data.tcId+'\'';
        pg.query(q, function (result) {
            cb && cb(result);
        }, function () {
            errcb && errcb();
        });
    },
    updateUyeTelefon: function(data, cb, errcb){
        var q = 'UPDATE orgut.uye SET telefon=\'' + data.tel + '\' where vatandaslik_no=\''+data.tcId+'\'';
        pg.query(q, function (result) {
            cb && cb(result);
        }, function () {
            errcb && errcb();
        });
    },
    insert: function (data, cb) {
        var Now = new Date();
        var year = Now.getFullYear();
        var month = Now.getMonth() + 1;
        month = (month < 9) ? ('0' + month) : month;
        var day = Now.getDate();
        day = (day < 9) ? ('0' + day) : day;
        var girisTarihi = year + '-' + month + '-' + day;
        var birthYear = data.dogumyili.slice(0, 4)
        var q = 'INSERT INTO orgut.uye(' +
            'isim, soyisim, sskno, dogumyeri, dogumyili, ' +
            'nkil, nkilce, nkkoy, nksayfa, nkcilt, giristarihi, ' +
            'babaismi, anaismi, nkhane, kadinerkek, nkmahalle, ' +
            'kangrubu, egitim, vatandaslik_no, telefon,tf_dtar,aciklama) ' +
            ' VALUES (\'' + data.isim + '\', \'' + data.soyisim + '\', \'' + data.vatNo + '\', \'' + data.dogumyeri + '\', \'' + birthYear + '\',' +
            ' \'' + data.nkil + '\', \'' + data.nkilce + '\', \'' + data.nkkoy + '\', \'' + data.nksayfa + '\', \'' + data.nkcilt + '\', \'' + girisTarihi + '\', ' +
            ' \'' + data.babaisim + '\', \'' + data.anaisimi + '\', \'' + data.nkhane + '\', ' + data.sexSelect + ', \'' + data.nkmahalle + '\', ' +
            ' ' + data.bloodSelect + ', ' + data.eduSelect + ', \'' + data.vatNo + '\', \'' + data.telefon + '\', \'' + data.dogumyili + '\', \'' + data.aciklama + '\') returning uyeid;';
        pg.query(q, function (result) {
            if (typeof cb !== "undefined") {
                if (result.hasOwnProperty('err')) {
                    cb(-1);
                } else {
                    var retVal = JSON.parse(result).uyeid;
                    cb(retVal);
                }

            }
        });
    },
    checkVatNo: function (uyeid, vatNo, cb) {
        var q = 'select * from orgut.uye where uyeid != ' + uyeid + ' and vatandaslik_no=\'' + vatNo + '\'';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    checkUyeExistByTcId: function(vatNos, cb, errcb){
        var str_vatNos = '\''+ vatNos.join('\',\'') + '\'';
        var q = 'select vatandaslik_no from orgut.uye where vatandaslik_no in ('+str_vatNos+')';
        pg.query(q, function (result) {
            cb && cb(result);
        }, function (){
            errcb && errcb();
        });
    },
    delete: function (uyeid, cb) {
        var preq = 'update orgut.uye set sonhareket=null where uyeid =' + uyeid;
        pg.query(preq, function (preresult) {
            var prepq = 'delete from orgut.hareket where uyeid=' + uyeid;
            pg.query(prepq, function (prepresult) {
                var q = 'delete from orgut.uye where uyeid = ' + uyeid;
                pg.query(q, function (result) {
                    cb && cb(result);
                });
            });
        });
    },
    getAllSonhareketInfo: function(cb){
        var q = 'SELECT * FROM ORGUT.uye WHERE sonhareket IS NULL';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    getAllSonharekettipiInfo: function(cb){
        var q = 'SELECT * FROM ORGUT.uye WHERE sonharekettipi IS NULL';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    runQuery: function (q, cb, cbErr){
        pg.query(q,function (result){
            cb && cb(result);
        }, function (){
            cbErr && cbErr(result);
        })
    },
    makeCorrectRecords: function (cb){
        var q = 'update orgut.uye u set sonharekettipi = (select h.cins from orgut.hareket h where h.uyeid = u.uyeid order by h.tarih desc limit 1) , sonhareket = (select h.hareketid from orgut.hareket h where h.uyeid = u.uyeid order by h.tarih desc limit 1);';
        pg.query(q, function (result) {
            cb && cb(result);
        });
    },
    updateUyeTelefonAsync: updateUyeTelefonAsync,
    updateUyeEpostaAsync:updateUyeEpostaAsync,
    updateLastMoveAsync:updateLastMoveAsync,
    checkUyeIdsExistAsync:checkUyeIdsExistAsync,
    runQueryWithAsync:runQueryWithAsync
};
module.exports = uye;


function updateUyeTelefonAsync(data) {
    data.tcId = data.tcid;
    return new Promise(function (resolve, reject) {
        uye.updateUyeTelefon(data, function () {
            resolve(true);
        }, function () {
            resolve(false);
        });
    });
}

function updateUyeEpostaAsync(data) {
    data.tcId = data.tcid;
    return new Promise(function (resolve, reject) {
        uye.updateUyeEmail(data, function () {
            resolve(true);
        },function () {
            resolve(false);
        });
    });
}

function updateLastMoveAsync(uyeid){
    return new Promise(function (resolve, reject) {
        hareketManager.getLastHareketByUyeID(uyeid, function (lastRow) {
            if (lastRow.length!=0){
                lastRow = JSON.parse(lastRow);
                var lastHareketId = lastRow.hareketid;
                var lastcins = lastRow.cins;
                var q = 'update orgut.uye set sonhareket=' + lastHareketId + ', sonharekettipi=' + lastcins + ' where uyeid=' + uyeid;
                pg.query(q, function (result) {
                    resolve(result);
                });
            }else{
                resolve();
            }
        });
    });

}

function checkUyeIdsExistAsync(tcids){
    return new Promise(function (resolve, reject){
       uye.checkUyeExistByTcId(tcids, function (result){
            resolve(result);
       });
    });
}

function runQueryWithAsync(query){
    return new Promise(function (resolve, reject){
        uye.runQuery(query, function (){
            resolve();
        }, function (){
            reject();
        });
    })

}

