var pg = require('./../Work/Data/Postgre');
var sendika = {
    getAll:function (cb) {
        var q = 'select * from orgut.sendika where "tblStatus"=1';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    getone: function (sendikaid, cb) {
        var q = 'select * from orgut.sendika where sendikaid = '+sendikaid;
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    save: function (item, cb) {
        var q= 'INSERT INTO orgut.sendika(isim, dosyano, adres, telefonno)' +
            ' VALUES (\''+item.isim+'\', \''+item.dosyano+'\', \''+item.adres+'\', \''+item.telefonno+'\');';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    update: function (item, cb) {
        var q= 'UPDATE orgut.sendika ' +
            ' SET isim=\''+item.isim+'\',' +
            ' dosyano=\''+item.dosyano+'\',' +
            ' adres=\''+item.adres+'\',' +
            ' telefonno=\''+item.telefonno+'\'' +
            ' WHERE sendikaid='+item.sendikaid+';';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    delete: function(sendikaid, cb){
        var q= 'update orgut.sendika set "tblStatus"=0 WHERE sendikaid='+sendikaid+';';
        pg.query(q, function (data) {
            cb && cb(data);
        });
    },
    rapor:{
        tespit: function (item, cb) {
            /*var q = 'select s2.isim as sendikaisim ,i.isim as isyeriisim, tc.* from tespitcevap tc ' +
                ' inner join isyeri i on tc.isyeriid = i.isyeriid\n' +
                ' inner join sendika s2 on tc.sendikaid = s2.sendikaid\n' +
                ' where tc.sendikaid='+item.sendikaid+' and tc.tarih between \''+item.startdate+'\' and \''+item.enddate+'\'\n' +
                ' order by tc.tarih';
            */

            var q = 'select s2.isim as sendikaisim ,i.isim as isyeriisim, tc.* from orgut.tespitcevap tc \n' +
                'inner join orgut.isyeri i on tc.isyeriid = i.isyeriid \n' +
                'inner join orgut.sendika s2 on tc.sendikaid = s2.sendikaid where\n' +
                ' tc.tarih between \''+item.start+'\' and \''+item.end+'\'\n' ;
                if(item.sendikaid !='0') {
                    q += ' and tc.sendikaid=' + item.sendikaid.toString();
                }
                q += ' order by tc.tarih'
            pg.query(q, function (data) {
                cb && cb(data);
            });
        },
        isyeriSayisi: function (sendikaid, cb) {
            var q = 'select count(*) from orgut.isyeri where sendikaid = '+ sendikaid;
            pg.query(q, function (data) {
                cb && cb(data);
            });
        }
    }

};

module.exports= sendika;