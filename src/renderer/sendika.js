var tengine = require('./../middleware/templating/engi');
var userManager = require('./../DataAccess/user');
var makeDom = require('./../middleware/templating/MakeDom');

var sendikaRenderer = {
    init: {
        sendikalar: function (req, res, obj, cb) {
            var html = global.temps.sendikalar;
            var userData = obj.user;
            var sendikaList = obj.sendikalar;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(sendikaList, accesstype, html);
                }
            });
        },
        sendikadetay: function (req, res, obj, cb) {
            var html = global.temps.sendikadetay;
            var userData = obj.user;
            var sendikaItem = obj.sendika;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(sendikaItem, accesstype, html);
                }
            });
        },
        sendikatespit: function (req, res, obj, cb) {
            var html = global.temps.sendikatespit;
            var userData = obj.user;
            var sendikaList = obj.sendikaList;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(sendikaList, accesstype, html);
                }
            });
        }
    },
    sendikalar: function (req, res, obj) {
        this.init.sendikalar(req, res, obj, function (sendikaList, accesstype, html) {
            var htmlSendikaLar = '';

            for (var i = 0; i < sendikaList.length; i++) {
                var item = JSON.parse(sendikaList[i]);
                htmlSendikaLar+= '<tr>';
                htmlSendikaLar+='<td>'+item.isim+'</td>';
                htmlSendikaLar+='<td>'+item.dosyano+'</td>';
                htmlSendikaLar+='<td>'+item.adres+'</td>';
                htmlSendikaLar+='<td>'+item.telefonno+'</td>';
                htmlSendikaLar+='<td>' +
                        '<a href="/sendikadetay?sendikaid='+item.sendikaid+'" class="btn btn-success btn-mini">Düzenle</a>' +
                    '</td>';
                htmlSendikaLar+= '</tr>';
            }

            html = html.replace(new RegExp('{{obj.sendikaList}}', 'g'), htmlSendikaLar);
            html = html.replace(new RegExp('{{obj.sendika.count}}', 'g'), sendikaList.length);

            tengine.render(req, res, html);
        });
    },
    sendikadetay: function (req, res, obj) {
        this.init.sendikadetay(req, res, obj, function (sendikaItem, accesstype, html) {
            var item ={};
            if (sendikaItem.length == 0) {
                item.sendikaid = 0;
            }else {
                item = JSON.parse(sendikaItem);
            }
            if(item.sendikaid == 0){
                html = html.replace(new RegExp('{{obj.sendika.sendikaid}}', 'g'), '0');
                html = html.replace(new RegExp('{{obj.sendika.isim}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.sendika.dosyano}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.sendika.adres}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.sendika.telefonno}}', 'g'), '');
            }else{
                html = html.replace(new RegExp('{{obj.sendika.sendikaid}}', 'g'), item.sendikaid);
                html = html.replace(new RegExp('{{obj.sendika.isim}}', 'g'), item.isim);
                html = html.replace(new RegExp('{{obj.sendika.dosyano}}', 'g'), item.dosyano);
                html = html.replace(new RegExp('{{obj.sendika.adres}}', 'g'), item.adres);
                html = html.replace(new RegExp('{{obj.sendika.telefonno}}', 'g'), item.telefonno);
            }
            tengine.render(req, res, html);
        });
    },
    sendikatespit: function (req, res, obj) {
        this.init.sendikatespit(req, res, obj, function (sendikaList ,accesstype, html) {
            var sendikaSelect = htmlSendikaSelect = makeDom.generateSendikaSelectArrayDefaultValue(sendikaList, 'sendikaid');
            html = html.replace(new RegExp('{{obj.select.sendikaSelect}}', 'g'), sendikaSelect);
            html = html.replace(new RegExp('{{obj.data.sendika}}', 'g'), JSON.stringify(sendikaList));

            tengine.render(req, res, html);
        });
    }
};


module.exports = sendikaRenderer;