var tengine = require('./../middleware/templating/engi');
var domMaker = require('./../middleware/templating/MakeDom');
var userManager = require('./../DataAccess/user');

var renderer = {
    init: {
            subeyonetimedit: function (req, res, obj, cb) {
                var html = global.temps.subeyonetimedit;
                var userData = obj.user;
                var sube = JSON.parse(obj.sube[0]);
                var subeteskilat = obj.subeteskilat;
                var subeCalisanList = obj.subeCalisanList;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(subeCalisanList, sube, subeteskilat,  accesstype, html);
                    }
                });
            },
            subeler: function (req, res, obj, cb) {
                var html = global.temps.sube;
                var userData = obj.user;
                var subeList = obj.subelist;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(subeList, accesstype, html);
                    }
                });
            },
            detay: function (req, res, obj, cb) {
                var html = global.temps.subeDetay;
                var userData = obj.user;
                var subeItem = obj.sube;
                var yonetimArr = obj.yonetim;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(subeItem, yonetimArr, accesstype, html);
                    }
                });
            },
            edit: function (req, res, obj, cb) {
                var html = global.temps.subeEdit;
                var userData = obj.user;
                var subeItem = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(subeItem, accesstype, html);
                    }
                });
            },
            isyeri: function (req, res, obj, cb) {
                var html = global.temps.subeIsyeri;
                var userData = obj.user;
                var isyeriList = obj.isyeriList;
                var sube = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(isyeriList, sube, accesstype, html);
                    }
                });
            },
            istatistik: function (req, res, obj, cb) {
                var html = global.temps.subeIstatistik;
                var userData = obj.user;
                var isyeriIstatistik = obj.isyeriIstatistik;
                var sube = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(isyeriIstatistik, sube, accesstype, html);
                    }
                });

            },
            yonetimEx: function (req, res, obj, cb) {
                var html = global.temps.subeyonetim;
                var userData = obj.user;
                var sube = obj.sube;
                var yonetimList = obj.yonetimList;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(yonetimList, sube, accesstype, html);
                    }
                });

            },
            subegiriscikis: function (req, res, obj, cb) {
                var html = global.temps.subegiriscikis;
                var userData = obj.user;
                var sube = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(sube, accesstype, html);
                    }
                });
            },
            subegiriscikisisimli: function (req, res, obj, cb) {
                var html = global.temps.subegiriscikisisimli;
                var userData = obj.user;
                var sube = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(sube, accesstype, html);
                    }
                });
            },
            subetespit: function (req, res, obj, cb) {
                var html = global.temps.subetespit;
                var userData = obj.user;
                var sube = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(sube, accesstype, html);
                    }
                });

            },
            subesendika: function (req, res, obj, cb) {
                var html = global.temps.subesendika;
                var userData = obj.user;
                var sube = obj.sube;
                var items = obj.liste;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(items, sube, accesstype, html);
                    }
                });
            },
            temsilciler: function (req, res, obj, cb) {
                var html = global.temps.subetemsilciler;
                var userData = obj.user;
                var items = obj.liste;
                var sube = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(items, sube, accesstype, html);
                    }
                });
            },
            subeuyelistesi: function (req, res, obj, cb) {
                var html = global.temps.subeuyelistesi;
                var userData = obj.user;
                var sube = obj.sube;
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                userManager.getPermissions(userData.userid, function (perms) {
                    var subeMenuId = 5;
                    var htmlMenuItems = '';
                    for (var i = 0; i < perms.length; i++) {
                        var menuItem = JSON.parse(perms[i]);
                        if (subeMenuId === menuItem.id) {
                            accesstype = menuItem.permissiontype;
                        }
                        htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                    }
                    html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                    if (typeof cb !== "undefined") {
                        cb(sube, accesstype, html);
                    }
                });
            }
        },
    subeler: function (req, res, obj) {
        this.init.subeler(req, res, obj, function (subeList, accessType, html) {
            var htmlSubelist = '';
            for (var j = 0; j < subeList.length; j++) {
                var subeItem = JSON.parse(subeList[j]);
                htmlSubelist += '<tr class="gradeX"><td class="select-hover" style="width:100px; text-align: center;" >' + subeItem.isim + '</td>' +
                    '<td class="select-hover" style=" text-align: center;">' + subeItem.telefonno + '</td>' +
                    '<td class="select-hover">' + subeItem.adres + '</td>' +
                    '<td class="center">';
                if (accessType === 1) { // 1 for all access
                    htmlSubelist += '<a href="?editsube=' + subeItem.subeid + '" class="btn btn-primary btn-mini">Düzenle</a>';
                }
                htmlSubelist += ' <a href="?detaysube=' + subeItem.subeid + '" class="btn btn-success btn-mini">Detay</a>';
                htmlSubelist += ' <a href="sube?isyerleri=' + subeItem.subeid + '" class="btn btn-info btn-mini">İşyerleri</a>';

                htmlSubelist += '</td></tr>';
            }
            html = html.replace(new RegExp('{{obj.subelist}}', 'g'), htmlSubelist);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), subeItem.subeid);
            tengine.render(req, res, html);
        });
    },
    subeDetay: function (req, res, obj) {
        this.init.detay(req, res, obj, function (subeItem, yonetimArr, accessType, html) {
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), subeItem.subeid);
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), subeItem.isim);
            html = html.replace(new RegExp('{{obj.sube.telefonno}}', 'g'), subeItem.telefonno);
            html = html.replace(new RegExp('{{obj.sube.adres}}', 'g'), subeItem.adres);
            html = html.replace(new RegExp('{{obj.sube.aciklama}}', 'g'), (subeItem.detay === null) ? '' : subeItem.detay);

            var htmlYonetimList = '';
            for (var i = 0; i < yonetimArr.length; i++) {
                htmlYonetimList += '<tr class="gradeX">' +
                    '<td>' +
                    '<a href="/uyedetay?uyeid=' + yonetimArr[i].uyeid + '" >' + yonetimArr[i].isim + ' ' + yonetimArr[i].soyisim + '</a>' +
                    '</td>' +
                    '<td>' + global.dataenums.enums.vazife[yonetimArr[i].vazife] + '</td>' +
                    '<td>' +
                    '<a href="/isyeridetay?isyeriid=' + yonetimArr[i].isyeriid + '">' + yonetimArr[i].isyeriisim + '</a></td>' +
                    '<td>' + global.dataenums.enums.hareketCinsi[yonetimArr[i].cins] + '</td>' +
                    '<td>' + yonetimArr[i].sortorder + '</td>' +
                    '</tr>';
            }
            html = html.replace(new RegExp('{{obj.yonetimlist}}', 'g'), htmlYonetimList);
            tengine.render(req, res, html);
        });
    },
    subeEdit: function (req, res, obj) {
        this.init.edit(req, res, obj, function (subeItem, accesstype, html) {
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), subeItem.subeid);
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), subeItem.isim);
            html = html.replace(new RegExp('{{obj.sube.telefonno}}', 'g'), subeItem.telefonno);
            html = html.replace(new RegExp('{{obj.sube.adres}}', 'g'), subeItem.adres);
            html = html.replace(new RegExp('{{obj.sube.aciklama}}', 'g'), (subeItem.detay === null) ? '' : subeItem.detay);
            tengine.render(req, res, html);
        });
    },
    isyeri: function (req, res, obj) {
        this.init.isyeri(req, res, obj, function (isyerleri, sube, accesstype, html) {
            var htmlIsyeriList = '';
            var theSube = JSON.parse(sube);
            var taseronvarSelect = domMaker.generateSelect(global.dataenums.enums.taseron, 'taseronType');
            var orgutlenmeSelect = domMaker.generateSelect(global.dataenums.enums.orgutlenmeTipi, 'orgutlenmeType');
            for (var i = 0; i < isyerleri.length; i++) {
                var isyeriItem = JSON.parse(isyerleri[i]);
                var taseronVar = (typeof global.dataenums.enums.taseron[isyeriItem.tasaronvar] === "undefined") ? '' : global.dataenums.enums.taseron[isyeriItem.tasaronvar];
                htmlIsyeriList += '<tr class="gradeX"><td>' + isyeriItem.isim + '</td><td>' + isyeriItem.adres + '</td><td>'
                    + global.dataenums.enums.orgutlenmeTipi[isyeriItem.orgutlenmedurum] + '</td><td>' + taseronVar + '</td><td>' + ((isyeriItem.calisansayisi == null) ? '' : isyeriItem.calisansayisi) + '</td></tr>';
            }
            html = html.replace(new RegExp('{{obj.isyeri.count}}', 'g'), isyerleri.length);
            html = html.replace(new RegExp('{{obj.isyeriList}}', 'g'), htmlIsyeriList);
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), theSube.isim);
            html = html.replace(new RegExp('{{obj.select.orgutlenme}}', 'g'), orgutlenmeSelect);
            html = html.replace(new RegExp('{{obj.select.taseronvar}}', 'g'), taseronvarSelect);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), theSube.subeid);

            tengine.render(req, res, html);
        });
    },
    istatistik: function (req, res, obj) {
        this.init.istatistik(req, res, obj, function (isyeriIstatistik, sube, accesstype, html) {
            var htmlIst = '';
            var theSube = JSON.parse(sube);
            var orgutlenmeSelect = domMaker.generateSelect(global.dataenums.enums.orgutlenmeTipi, 'orgutlenmeType', "1");
            var total = 0;
            var totalMale = 0;
            var totalFemale = 0;

            var tmpIstatistiks =[];
            var isyeriIstatistikLen = isyeriIstatistik.length;
            for (var i =0; i< isyeriIstatistikLen; i++){
                tmpIstatistiks.push(JSON.parse(isyeriIstatistik[i]));
            }

            tmpIstatistiks.sort(global.core.genricSort('isyeriisim','sayi'));

            for (var i = 0; i < isyeriIstatistikLen; i++) {
                var isyeriStat = tmpIstatistiks[i];
                htmlIst += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + isyeriStat.isyeriisim + '</td>' +
                    '<td>' + isyeriStat.sayi + '</td>' +
                    '<td>' + isyeriStat.erkek + '</td>' +
                    '<td>' + isyeriStat.kadin + '</td>' +
                    '</tr>';
                total += parseInt(isyeriStat.sayi);
                totalMale += parseInt(isyeriStat.erkek);
                totalFemale += parseInt(isyeriStat.kadin);
            }
            htmlIst += '<tr><td></td><td></td><td>' + total + '</td><td>' + totalMale + '</td><td>' + totalFemale + '</td></tr>';

            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), theSube.isim);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), theSube.subeid);
            html = html.replace(new RegExp('{{obj.isyeriIstatistikList}}', 'g'), htmlIst);
            html = html.replace(new RegExp('{{obj.isyeri.count}}', 'g'), isyeriIstatistik.length);
            html = html.replace(new RegExp('{{obj.select.orgutlenme}}', 'g'), orgutlenmeSelect);

            tengine.render(req, res, html);
        });
    },
    yonetimEx: function (req, res, obj) {
        this.init.yonetimEx(req, res, obj, function (yonetimList, sube, accesstype, html) {
            var htmlYonetimlist = '';
            var subeItem = JSON.parse(sube);

            for (var i = 0; i < yonetimList.length; i++) {
                var item = JSON.parse(yonetimList[i]);
                htmlYonetimlist += '<tr ' + ((item.cins != 1) ? 'style="background-color: palevioletred;"' : '') + ' >' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + item.isim + '</td>' +
                    '<td>' + item.soyisim + '</td>' +
                    '<td>' + item.isyeriisim + '</td>' +
                    '<td>' + global.dataenums.enums.vazife[item.vazife] + '</td>' +
                    '<td>' + item.vatandaslik_no + '</td>' +
                    '<td>' + global.dataenums.enums.hareketCinsi[item.cins] + '</td>' +
                    '<td><a class="btn btn-primary" href="/subeyonetimedit?subeteskilatiid=' + item.subeteskilatiid +'&subeid='+subeItem.subeid+'">Güncelle</a>' +
                    '&nbsp;&nbsp; <button dataid="' + item.subeteskilatiid + '" class="btn btnDelete btn-primary">SİL</button> </td>';
                '</tr>';
            }
            html = html.replace(new RegExp('{{obj.yonetimList}}', 'g'), htmlYonetimlist);
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), subeItem.isim);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), subeItem.subeid);

            tengine.render(req, res, html);
        });
    },
    giriscikis: function (req, res, obj) {
        this.init.subegiriscikis(req, res, obj, function (sube, accesstype, html) {
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), sube.isim);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), sube.subeid);
            tengine.render(req, res, html);
        });
    },
    giriscikisisimli: function (req, res, obj) {
        this.init.subegiriscikisisimli(req, res, obj, function (sube, accesstype, html) {
            var hareketSelect = domMaker.generateSelect(global.dataenums.enums.hareketCinsi, 'hareketType');

            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), sube.isim);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), sube.subeid);
            html = html.replace(new RegExp('{{obj.select.hareketipi}}', 'g'), hareketSelect);

            tengine.render(req, res, html);
        });
    },
    subetespit: function (req, res, obj) {
        this.init.subetespit(req, res, obj, function (sube, accesstype, html) {
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), sube.isim);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), sube.subeid);
            tengine.render(req, res, html);
        });
    },
    subesendika: function (req, res, obj) {
        this.init.subesendika(req, res, obj, function (items, sube, accesstype, html) {
            var htmlIst = '';
            for (var i = 0; i < items.length; i++) {
                var item = JSON.parse(items[i]);
                var TarihiArr = item.tarih.split('T');
                var TarihiDateArr = TarihiArr[0].split('-');
                var Tarihi = TarihiDateArr[2] + '.' + TarihiDateArr[1] + '.' + TarihiDateArr[0];

                htmlIst += '<tr><td>' + (i + 1) + '</td><td>' + item.isyeriisim + '</td><td>' + item.sendikaisim +
                    '</td><td>' + Tarihi + '</td><td>' + item.uyesayisi + '</td><td>' + item.iscisayisi + '</td></tr>';
            }
            html = html.replace(new RegExp('{{obj.sendikalist}}', 'g'), htmlIst);
            html = html.replace(new RegExp('{{obj.count}}', 'g'), items.length);
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), sube.isim);
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), sube.subeid);

            tengine.render(req, res, html);
        });
    },
    temsilciler: function (req, res, obj) {
        this.init.temsilciler(req, res, obj, function (items, sube, accesstype, html) {

            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), sube.isim);

            var itemsHtml = '';
            for (var i = 0; i < items.length; i++) {
                var item = JSON.parse(items[i]);
                itemsHtml += '<tr>';
                itemsHtml += '<td>' + item.isyeriisim + '</td>';
                itemsHtml += '<td>' + ((typeof global.dataenums.enums.vazifeTemsilci[item.vazife] === "undefined") ? '' : global.dataenums.enums.vazifeTemsilci[item.vazife]) + '</td>';
                itemsHtml += '<td>' + item.isim + '</td>';
                itemsHtml += '<td>' + item.soyisim + '</td>';
                itemsHtml += '<td>' + item.vatandaslik_no + '</td>';
                itemsHtml += '<td>' + ((typeof global.dataenums.enums.hareketCinsi[item.cins] === "undefined") ? '' : global.dataenums.enums.hareketCinsi[item.cins]) + '</td>';
                itemsHtml += '</tr>';
            }
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), sube.subeid);
            html = html.replace(new RegExp('{{obj.subeTemsilciList}}', 'g'), itemsHtml);
            html = html.replace(new RegExp('{{obj.data}}', 'g'), JSON.stringify(items));

            tengine.render(req, res, html);
        });
    },
    subeuyelistesi: function (req, res, obj) {
        this.init.subeuyelistesi(req, res, obj, function (sube, accesstype, html) {
            html = html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), sube.subeid);
            html = html.replace(new RegExp('{{obj.sube.isim}}', 'g'), sube.isim);
            tengine.render(req, res, html);
        });
    },
    subeyonetimedit: function (req, res, obj) {
        this.init.subeyonetimedit(req, res, obj, function (subeCalisanList,  sube, subeteskilat, accesstype, html) {
            html =  html.replace(new RegExp('{{obj.sube.isim}}', 'g'), sube.isim);
            html =  html.replace(new RegExp('{{obj.sube.subeid}}', 'g'), sube.subeid);
            if(subeteskilat.length>0){
                subeteskilat = JSON.parse(subeteskilat[0]);
            }
            html =  html.replace(new RegExp('{{obj.yonetim.data}}', 'g'), JSON.stringify(subeteskilat));
            html =  html.replace(new RegExp('{{obj.uye.uyeid}}', 'g'), subeteskilat.subeteskilatiid==0 ? 0:subeteskilat.uyeid);
            html =  html.replace(new RegExp('{{obj.subeteskilati.subeteskilatiid}}', 'g'), (subeteskilat.subeteskilatiid == undefined)? 0 :subeteskilat.subeteskilatiid);

            var strVazifeSelect = domMaker.generateSelect(global.dataenums.enums.vazife, 'vazifeSelect',subeteskilat && subeteskilat.vazife);
            html =  html.replace(new RegExp('{{obj.select.vazifeler}}', 'g'), strVazifeSelect);
            html =  html.replace(new RegExp('{{obj.data}}', 'g'), JSON.stringify(subeCalisanList));


            tengine.render(req, res, html);
        });
    }
};


module.exports = renderer;