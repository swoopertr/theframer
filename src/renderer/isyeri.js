var tenginge = require('./../middleware/templating/engi');
var domMaker = require('./../middleware/templating/MakeDom');
var isyeriManager = require('./../DataAccess/isyeri');
var subeManager = require('./../DataAccess/sube');
var userManager = require('./../DataAccess/user');
var sendikaManager = require('./../DataAccess/sendika');
var setting = require('./../setting');

var renderer = {
    init: {
        isyerleri: function (req, res, obj, cb) {
            var html = global.temps.isyeri;
            var userData = obj.user;
            var isyeriList = obj.isyerleri;
            var subeList = obj.subeList;

            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                if (typeof cb !== "undefined") {
                    cb(isyeriList, subeList, 1, html);
                }
            });
        },
        isyeriDetay: function (req, res, obj, cb) {
            var html = global.temps.isyeriDetay;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;
            var istatistik = obj.istatistik;
            var tisData = obj.tisData;
            var uyeListesi = obj.uyeler;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(isyeriItem, istatistik, tisData, uyeListesi, accesstype, html);
                }
            });
        },
        isyeriEdit: function (req, res, obj, cb) {
            var html = global.temps.isyeriEdit;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;
            var tisItem = obj.tisData;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(isyeriItem, tisItem, accesstype, html);
                }
            });
        },
        uyelistesi: function (req, res, obj, cb) {
            var html = global.temps.isyeriUyeler;
            var userData = obj.user;
            var isyeri = obj.isyeri;
            var uyeList = obj.uyeList;

            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);


                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);


                if (typeof cb !== "undefined") {
                    cb(uyeList, isyeri, 1, html);
                }
            });
        },
        isyeriGirisCikis: function (req, res, obj, cb) {
            var html = global.temps.isyeriGirisCikis;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(isyeriItem, accesstype, html);
                }
            });
        },
        isyerigiriscikisisimli: function (req, res, obj, cb) {
            var html = global.temps.isyeriGirisCikisIsimli;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(isyeriItem, accesstype, html);
                }
            });
        },
        isyeriorgut: function (req, res, obj, cb) {
            var html = global.temps.isyeriOrgut;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;
            var orgutData = obj.orgutData;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(isyeriItem, orgutData, accesstype, html);
                }
            });
        },
        isyeritemsilci: function (req, res, obj, cb) {
            var html = global.temps.isyeriTemsilci;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;
            var orgutData = obj.orgutData;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(isyeriItem, orgutData, accesstype, html);
                }
            });
        },
        isyeriTespit: function (req, res, obj, cb) {
            var html = global.temps.isyeriTespit;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(isyeriItem, accesstype, html);
                }
            });
        },
        isyeritemsilciEdit: function (req, res, obj, cb) {
            var html = global.temps.isyeritemsilciedit;
            var userData = obj.user;
            var isyeriItem = obj.isyeri;
            var calisanlar = obj.calisanlar;
            var temsilci = obj.temsilci;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(temsilci, isyeriItem, calisanlar, accesstype, html);
                }
            });

        },
        isyeriisverentemsilcileri: function (req, res, obj, cb) {
            var html = global.temps.isyeriisverentemsilcileri;
            var userData = obj.user;
            var isyeri = obj.isyeri;
            var temsilciler = obj.temsilciler;

            userManager.getPermissions(userData.userid, function (perms) {
                var isyeriMenuId = 2;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (isyeriMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                if (typeof cb !== "undefined") {
                    cb(temsilciler, isyeri, accesstype, html);
                }
            });
        }

    },
    isyeriTespit: function (req, res, obj) {
        this.init.isyeriTespit(req, res, obj, function (isyeriItem, accesstype, html) {
            isyeriItem = JSON.parse(isyeriItem[0]);
            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);

            tenginge.render(req, res, html);

        });
    },
    isyerleri: function (req, res, obj) {
        this.init.isyerleri(req, res, obj, function (isyeriList, subeList, accesstype, html) {
            var tmpSubeObj = {};

            var htmlselectOrgutlenme = domMaker.generateSelect(global.dataenums.enums.orgutlenmeTipi, 'selectOrgutlenmeTipi');
            var htmlselectTaseron = domMaker.generateSelect(global.dataenums.enums.taseron, 'selectTaseron');
            var htmlselectSubeler = domMaker.generateSubeSelectArrayDefaultValue(subeList, 'selectSubeler');

            for (var j = 0; j < subeList.length; j++) {
                var tmpSubeItem = JSON.parse(subeList[j]);

                tmpSubeObj[tmpSubeItem.subeid] = tmpSubeItem.isim;
            }

            var htmlisyeriList = '';
            for (var i = 0; i < isyeriList.length; i++) {
                var isyeriItem = JSON.parse(isyeriList[i]);
                htmlisyeriList += '<tr class="gradeX dataitem" dataitem="' + isyeriItem.isyeriid + '">';
                htmlisyeriList += '<td class="select-hover">' + isyeriItem.isim + '</td>';
                htmlisyeriList += '<td class="select-hover">' + global.dataenums.enums.orgutlenmeTipi[isyeriItem.orgutlenmedurum] + '</td>';
                htmlisyeriList += '<td class="select-hover">' + ((isyeriItem.tasaronvar === null) ? ' ' : global.dataenums.enums.taseron[isyeriItem.tasaronvar]) + '</td>';
                htmlisyeriList += '<td class="select-hover">' + tmpSubeObj[isyeriItem.subeid] + '</td>';
                htmlisyeriList += '<td>';
                if (accesstype === global.dataenums.enums.ACCESSTYPE.ACCESS) {
                    htmlisyeriList += '<a href="/isyeriedit?isyeriid=' + isyeriItem.isyeriid + '" class="btn btn-primary btn-mini" style="margin-bottom: 5px;">Düzenle</a>';
                    htmlisyeriList += '&nbsp;<a href="" data="' + isyeriItem.isyeriid + '" class="btndeleteisyeri btn btn-danger btn-mini" style="margin-bottom: 5px;">Sil</a>';
                }
                htmlisyeriList += '&nbsp;<a href="/isyeridetay?isyeriid=' + isyeriItem.isyeriid + '" class="btn btn-info btn-mini" style="margin-bottom: 5px;">Detay</a></td>';
                htmlisyeriList += '</tr>';
            }
            html = html.replace(new RegExp('{{obj.isyeriList}}', 'g'), htmlisyeriList);
            html = html.replace(new RegExp('{{obj.select.OrgutDurum}}', 'g'), htmlselectOrgutlenme);
            html = html.replace(new RegExp('{{obj.select.Taseron}}', 'g'), htmlselectTaseron);
            html = html.replace(new RegExp('{{obj.select.Subeler}}', 'g'), htmlselectSubeler);
            html = html.replace(new RegExp('{{obj.allSubeData}}', 'g'), JSON.stringify(tmpSubeObj));
            html = html.replace(new RegExp('{{obj.pagingItemCount}}', 'g'), setting.pagingCount);

            if (req.isCache == true) {
                global.cache.set(req.url, html);
            }

            tenginge.render(req, res, html);

        });
    },
    isyeriDetay: function (req, res, obj) {
        this.init.isyeriDetay(req, res, obj, function (isyeriItem, istatistik, tisData, uyeListesi, accessType, html) {
            var subeList, subeObj = {};
            subeManager.getList(function (result) {
                for (var i = 0; i < result.length; i++) {
                    var subeItem = JSON.parse(result[i]);
                    subeObj[subeItem.subeid] = subeItem.isim;
                }

                if (tisData.length > 0) {
                    var tisItem = JSON.parse(tisData[0]);
                    var arrStartDate = tisItem.Startdate.split('T');
                    var arrDateStartDate = arrStartDate[0].split('-');
                    var strStartDate = arrDateStartDate[2] + '.' + arrDateStartDate[1] + '.' + arrDateStartDate[0];

                    var arrEndDate = tisItem.Enddate.split('T');
                    var arrDateEndDate = arrEndDate[0].split('-');
                    var strEndDate = arrDateEndDate[2] + '.' + arrDateEndDate[1] + '.' + arrDateEndDate[0];

                    var tisText = tisItem.Title + ' - (' + strStartDate + ' - ' + strEndDate + ')';
                    html = html.replace(new RegExp('{{obj.data.tisdata}}', 'g'), tisText);

                    var tisListHtml = '';
                    for (var i = 0; i < tisData.length; i++) {
                        var tisDataItem = JSON.parse(tisData[i]);
                        var _arrStartDate = tisDataItem.Startdate.split('T');
                        var _arrDateStartDate = _arrStartDate[0].split('-');
                        var _strStartDate = _arrDateStartDate[2] + '.' + _arrDateStartDate[1] + '.' + _arrDateStartDate[0];

                        var _arrEndDate = tisDataItem.Enddate.split('T');
                        var _arrDateEndDate = _arrEndDate[0].split('-');
                        var _strEndDate = _arrDateEndDate[2] + '.' + _arrDateEndDate[1] + '.' + _arrDateEndDate[0];

                        tisListHtml += '<tr>';
                        tisListHtml += '<td>' + tisDataItem.Title + '</td>';
                        tisListHtml += '<td>' + tisDataItem.SezonNumarasi + '</td>';
                        tisListHtml += '<td>' + _strStartDate + ' / ' + _strEndDate + '</td>';
                        tisListHtml += '</tr>';
                    }
                    html = html.replace(new RegExp('{{obj.data.tisDataList}}', 'g'), tisListHtml);
                } else {
                    html = html.replace(new RegExp('{{obj.data.tisDataList}}', 'g'), '');
                }

                for (var prop in isyeriItem) {
                    if (prop === "orgutlenmedurum") {
                        html = html.replace(new RegExp('{{obj.isyeri.' + prop + '}}', 'g'), (typeof isyeriItem[prop] === 'undefined' || isyeriItem[prop] == null) ? '' : global.dataenums.enums.orgutlenmeTipi[isyeriItem[prop]]);
                    } else if (prop === "tasaronvar") {
                        html = html.replace(new RegExp('{{obj.isyeri.' + prop + '}}', 'g'), (typeof isyeriItem[prop] === 'undefined' || isyeriItem[prop] == null) ? '' : global.dataenums.enums.taseron[isyeriItem[prop]]);
                    } else if (prop === "kamuozel") {
                        html = html.replace(new RegExp('{{obj.isyeri.' + prop + '}}', 'g'), (typeof isyeriItem[prop] === 'undefined' || isyeriItem[prop] == null) ? '' : global.dataenums.enums.kamuTipi[isyeriItem[prop]]);
                    } else if (prop === "isverensendikasi") {
                        html = html.replace(new RegExp('{{obj.isyeri.' + prop + '}}', 'g'), (typeof isyeriItem[prop] === 'undefined' || isyeriItem[prop] == null) ? '' : isyeriItem[prop]);
                    } else if (prop === "grpdahilmi") {
                        html = html.replace(new RegExp('{{obj.isyeri.' + prop + '}}', 'g'), (typeof isyeriItem[prop] === 'undefined' || isyeriItem[prop] == null) ? '' : global.dataenums.enums.gruplama[isyeriItem[prop]]);
                    } else if (prop === "subeid") {
                        html = html.replace(new RegExp('{{obj.isyeri.' + prop + '}}', 'g'), (typeof isyeriItem[prop] === 'undefined' || isyeriItem[prop] == null) ? '' : subeObj[isyeriItem[prop]]);
                    } else {
                        html = html.replace(new RegExp('{{obj.isyeri.' + prop + '}}', 'g'), (typeof isyeriItem[prop] === 'undefined' || isyeriItem[prop] == null) ? '' : isyeriItem[prop]);
                    }
                }
                if (istatistik.length != 0) {
                    var istatistikItem = JSON.parse(istatistik[0]);
                    html = html.replace(new RegExp('{{obj.istatistik.uyesayisi}}', 'g'), istatistikItem.calisan);
                    html = html.replace(new RegExp('{{obj.istatistik.erkek}}', 'g'), istatistikItem.erkek);
                    html = html.replace(new RegExp('{{obj.istatistik.kadin}}', 'g'), istatistikItem.kadin);
                } else {
                    html = html.replace(new RegExp('{{obj.istatistik.uyesayisi}}', 'g'), '~');
                    html = html.replace(new RegExp('{{obj.istatistik.erkek}}', 'g'), '~');
                    html = html.replace(new RegExp('{{obj.istatistik.kadin}}', 'g'), '~');
                }

                var uyeListesiLen = uyeListesi.length;
                var jsonUyeListesi = [];

                for (var i=0;i< uyeListesiLen ; i++){
                    jsonUyeListesi.push(JSON.parse(uyeListesi[i]));
                }
                jsonUyeListesi.sort(global.core.genricSort('isim', 'soyisim'));


                var uyeHtmlFull = '<tr>';
                for (var i = 0; i < uyeListesiLen; i++) {
                    var item = jsonUyeListesi[i];
                    var uyeHtml = '<tr>';
                    uyeHtml += '<td>'+(i+1)+'</td>';
                    uyeHtml += '<td><strong>' + item.isim + ' ' + item.soyisim + '</strong></td>';
                    uyeHtml += '<td>' + (item.vatandaslik_no == undefined ? '' : item.vatandaslik_no) + '</td>';
                    uyeHtml += '<td>' + ((item.kadinerkek == 1) ? 'Erkek' : 'Kadın') + '</td>';
                    uyeHtml += '<td>' + item.tarih.substring(0, item.tarih.indexOf('T')) + '</td>';
                    uyeHtml += '<td>' + item.kararno + '</td>';
                    uyeHtml += '<td>' + item.dogrulama_kodu + '</td>';
                    uyeHtml += '<td>' + ((item.telefon==null)?'~':item.telefon)+ '</td>';
                    uyeHtml += '</tr>';
                    uyeHtmlFull += uyeHtml;
                }
                html = html.replace(new RegExp('{{obj.uye.list}}', 'g'), uyeHtmlFull);


                tenginge.render(req, res, html);
            });
        });
    },
    isyeriEdit: function (req, res, obj) {
        this.init.isyeriEdit(req, res, obj, function (isyeriItem, tisData, accessType, html) {
            var subeList, sendikaList, selectSube, selectKamu, selectIsverenSendika, selectGrpDahilmi, orgutlenmedurum,
                taseron, sendikaSelect;

            subeManager.getList(function (subeArr) {
                subeList = subeArr;
                sendikaManager.getAll(function (sendikaArr) {
                    sendikaList = sendikaArr;

                    if (tisData.length > 0) {
                        var tisItem = JSON.parse(tisData[0]);
                        var arrStartDate = tisItem.Startdate.split('T');
                        var arrDateStartDate = arrStartDate[0].split('-');
                        var strStartDate = arrDateStartDate[2] + '.' + arrDateStartDate[1] + '.' + arrDateStartDate[0];

                        var arrEndDate = tisItem.Enddate.split('T');
                        var arrDateEndDate = arrEndDate[0].split('-');
                        var strEndDate = arrDateEndDate[2] + '.' + arrDateEndDate[1] + '.' + arrDateEndDate[0];

                        var tisText = tisItem.Title + ' - (' + strStartDate + ' - ' + strEndDate + ')';
                        html = html.replace(new RegExp('{{obj.data.tisdata}}', 'g'), tisText);

                        var tisListHtml = '';
                        for (var i = 0; i < tisData.length; i++) {
                            var tisDataItem = JSON.parse(tisData[i]);
                            var _arrStartDate = tisDataItem.Startdate.split('T');
                            var _arrDateStartDate = _arrStartDate[0].split('-');
                            var _strStartDate = _arrDateStartDate[2] + '.' + _arrDateStartDate[1] + '.' + _arrDateStartDate[0];

                            var _arrEndDate = tisDataItem.Enddate.split('T');
                            var _arrDateEndDate = _arrEndDate[0].split('-');
                            var _strEndDate = _arrDateEndDate[2] + '.' + _arrDateEndDate[1] + '.' + _arrDateEndDate[0];

                            tisListHtml += '<tr>';
                            tisListHtml += '<td>' + tisDataItem.Title + '</td>';
                            tisListHtml += '<td>' + tisDataItem.SezonNumarasi + '</td>';
                            tisListHtml += '<td>' + _strStartDate + ' / ' + _strEndDate + '</td>';
                            tisListHtml += '</tr>';
                        }
                        html = html.replace(new RegExp('{{obj.data.tisDataList}}', 'g'), tisListHtml);
                    } else {
                        html = html.replace(new RegExp('{{obj.data.tisDataList}}', 'g'), ' ');
                    }


                    if (isyeriItem.isyeriid == "0") {
                        sendikaSelect = domMaker.generateSendikaSelectArrayDefaultValue(sendikaList, 'sendikaSelect');
                        selectSube = domMaker.generateSubeSelectArrayDefaultValue(subeList, 'selectSube');
                        selectKamu = domMaker.generateSelect(dataenums.enums.kamuTipi, 'selectKamu');
                        selectIsverenSendika = domMaker.generateSelect(dataenums.enums.isverenSendikasi, 'selectIsverenSendika');
                        selectGrpDahilmi = domMaker.generateSelect(dataenums.enums.gruplama, 'selectGrpDahilmi');
                        orgutlenmedurum = domMaker.generateSelect(dataenums.enums.orgutlenmeTipi, 'selectOrgut');
                        taseron = domMaker.generateSelect(dataenums.enums.taseron, 'selectTaseron');


                        html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), '0');
                        html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.adres}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.telefonno}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.bcmdosyano}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.sskno}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.tisbaslangic}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.tisbitis}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.tisimza}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.ustkurulus}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.uretimkonusu}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.email}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.faxno}}', 'g'), '');
                        html = html.replace(new RegExp('{{obj.isyeri.calisansayisi}}', 'g'), '0');

                    } else {
                        sendikaSelect = domMaker.generateSendikaSelectArrayDefaultValue(sendikaList, 'sendikaSelect', isyeriItem.sendikaid);
                        selectSube = domMaker.generateSubeSelectArrayDefaultValue(subeList, 'selectSube', isyeriItem.subeid);
                        selectKamu = domMaker.generateSelect(dataenums.enums.kamuTipi, 'selectKamu', isyeriItem.kamuozel);
                        selectIsverenSendika = domMaker.generateSelect(dataenums.enums.isverenSendikasi, 'selectIsverenSendika', isyeriItem.isverensendikasi);
                        selectGrpDahilmi = domMaker.generateSelect(dataenums.enums.gruplama, 'selectGrpDahilmi', isyeriItem.grpdahilmi);
                        orgutlenmedurum = domMaker.generateSelect(dataenums.enums.orgutlenmeTipi, 'selectOrgut', isyeriItem.orgutlenmedurum);
                        taseron = domMaker.generateSelect(dataenums.enums.taseron, 'selectTaseron', isyeriItem.tasaronvar);

                        html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);
                        html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
                        html = html.replace(new RegExp('{{obj.isyeri.adres}}', 'g'), isyeriItem.adres);
                        html = html.replace(new RegExp('{{obj.isyeri.telefonno}}', 'g'), isyeriItem.telefonno);
                        html = html.replace(new RegExp('{{obj.isyeri.bcmdosyano}}', 'g'), isyeriItem.bcmdosyano);
                        html = html.replace(new RegExp('{{obj.isyeri.sskno}}', 'g'), isyeriItem.sskno);
                        var TisBaslangic = isyeriItem.tisbaslangic == null ? '' : isyeriItem.tisbaslangic.split('T')[0];
                        html = html.replace(new RegExp('{{obj.isyeri.tisbaslangic}}', 'g'), TisBaslangic);
                        var TisBitis = isyeriItem.tisbitis == null ? '' : isyeriItem.tisbitis.split('T')[0];
                        html = html.replace(new RegExp('{{obj.isyeri.tisbitis}}', 'g'), TisBitis);

                        var TisImza = isyeriItem.tisimza == null ? '' : isyeriItem.tisimza.split('T')[0];
                        html = html.replace(new RegExp('{{obj.isyeri.tisimza}}', 'g'), TisImza);

                        html = html.replace(new RegExp('{{obj.isyeri.ustkurulus}}', 'g'), isyeriItem.ustkurulus);
                        html = html.replace(new RegExp('{{obj.isyeri.uretimkonusu}}', 'g'), isyeriItem.uretimkonusu);
                        html = html.replace(new RegExp('{{obj.isyeri.email}}', 'g'), isyeriItem.email);
                        html = html.replace(new RegExp('{{obj.isyeri.faxno}}', 'g'), isyeriItem.faxno);
                        html = html.replace(new RegExp('{{obj.isyeri.calisansayisi}}', 'g'), isyeriItem.calisansayisi);
                    }
                    html = html.replace(new RegExp('{{obj.select.sendika}}', 'g'), sendikaSelect);
                    html = html.replace(new RegExp('{{obj.sube.selectSube}}', 'g'), selectSube);
                    html = html.replace(new RegExp('{{obj.select.selectKamu}}', 'g'), selectKamu);
                    html = html.replace(new RegExp('{{obj.select.isverensendikasi}}', 'g'), selectIsverenSendika);
                    html = html.replace(new RegExp('{{obj.select.selectGrpDahilmi}}', 'g'), selectGrpDahilmi);
                    html = html.replace(new RegExp('{{obj.select.orgutlenmedurum}}', 'g'), orgutlenmedurum);
                    html = html.replace(new RegExp('{{obj.select.taseron}}', 'g'), taseron);


                    tenginge.render(req, res, html);

                });
            });


        });
    },
    uyelistesi: function (req, res, obj) {
        this.init.uyelistesi(req, res, obj, function (uyeList, isyeri, accessType, html) {
            var isyeriItem = JSON.parse(isyeri[0]);
            var excelItem = [];
            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);

            var uyeListLen = uyeList.length;
            var jsonUyeList = [];
            for(var i=0; i<uyeListLen;i++){
                /*var item = JSON.parse(uyeList[i]);
                if(item.isim.startsWith('ALİ')){
                    jsonUyeList.push(JSON.parse(uyeList[i]));
                }*/
                jsonUyeList.push(JSON.parse(uyeList[i]));
            }
            jsonUyeList.sort(global.core.genricSort('isim', 'soyisim'));
            var thelen = jsonUyeList.length;

            var htmlUyeList = '';
            for (var i = 0; i < thelen; i++) {
                var uyeItem = jsonUyeList[i];

                htmlUyeList += '<tr class="gradeX dataitem" dataitem="' + uyeItem.isyeriid + '" uyeid="' + uyeItem.uyeid + '">';
                htmlUyeList += '<td class="contextmenu">' + (i + 1) + '</td>';
                htmlUyeList += '<td>' + uyeItem.isim + '</td>';
                htmlUyeList += '<td>' + uyeItem.soyisim + '</td>';
                htmlUyeList += '<td>' + uyeItem.vatandaslik_no + '</td>';
                htmlUyeList += '<td>' + global.dataenums.enums.kadinerkek[uyeItem.kadinerkek], +'</td>';
                var tarihArr = uyeItem.tarih.split('T')[0].split('-');
                var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];
                htmlUyeList += '<td>' + tarih + '</td>';
                htmlUyeList += '<td>' + uyeItem.kararno + '</td>';
                htmlUyeList += '<td>' + (uyeItem.dogrulama_kodu == null ? '' : uyeItem.dogrulama_kodu) + '</td>';
                htmlUyeList += '<td>' + uyeItem.telefon + '</td>';
                htmlUyeList += '</tr>';

                excelItem.push({
                    "Isim": uyeItem.isim,
                    "Soyisim": uyeItem.soyisim,
                    "TC_No": uyeItem.vatandaslik_no,
                    "Cinsiyet": global.dataenums.enums.kadinerkek[uyeItem.kadinerkek],
                    "Tarih": tarih,
                    "Karar": uyeItem.kararno,
                    "Onay": uyeItem.dogrulama_kodu,
                    "Telefon": uyeItem.telefon
                });
            }

            var htmlSelect =  domMaker.generateSelect(global.dataenums.enums.kadinerkek, 'selectGender', 0);

            html = html.replace(new RegExp('{{obj.uyelist}}', 'g'), htmlUyeList);
            html = html.replace(new RegExp('{{obj.data.full}}', 'g'), JSON.stringify(excelItem));
            html = html.replace(new RegExp('{{obj.select.gender}}', 'g'), htmlSelect);

            tenginge.render(req, res, html);
        });
    },
    isyeriGirisCikis: function (req, res, obj) {
        this.init.isyeriGirisCikis(req, res, obj, function (isyeriItem, accesstype, html) {
            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);
            tenginge.render(req, res, html);
        });
    },
    isyerigiriscikisisimli: function (req, res, obj) {
        this.init.isyerigiriscikisisimli(req, res, obj, function (isyeriItem, accesstype, html) {
            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);
            var hareketSelect = domMaker.generateSelect(global.dataenums.enums.hareketCinsi, 'hareketType');
            html = html.replace(new RegExp('{{obj.select.hareketipi}}', 'g'), hareketSelect);

            tenginge.render(req, res, html);
        });
    },
    isyeriorgut: function (req, res, obj) {
        this.init.isyeriorgut(req, res, obj, function (isyeriItem, orgutData, accessType, html) {
            isyeriItem = JSON.parse(isyeriItem[0]);
            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);

            html = html.replace(new RegExp('{{obj.data}}', 'g'), JSON.stringify(orgutData));

            tenginge.render(req, res, html);
        });
    },
    isyeritemsilci: function (req, res, obj) {
        this.init.isyeritemsilci(req, res, obj, function (isyeriItem, orgutData, accessType, html) {
            isyeriItem = JSON.parse(isyeriItem[0]);
            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);
            html = html.replace(new RegExp('{{obj.data}}', 'g'), JSON.stringify(orgutData));

            tenginge.render(req, res, html);
        });
    },
    isyeritemsilciEdit: function (req, res, obj) {
        this.init.isyeritemsilciEdit(req, res, obj, function (temsilci, isyeriItem, calisanlar, accesstype, html) {
            isyeriItem = JSON.parse(isyeriItem[0]);
            if (typeof temsilci == 'undefined') {
                temsilci = {temsilciid: 0, vazife: 0, sira: 0, uyeid: 0};
            } else {
                temsilci = JSON.parse(temsilci[0]);
            }
            //var strCalisanSelect = domMaker.generateCalisanArrayDefaultValue(calisanlar, 'calisanSelect', temsilci.uyeid);
            var strVazifeSelect = domMaker.generateSelectTemsilciVazife('vazifeSelect', temsilci.vazife);
            var strTemsilciStatusu=domMaker.generateSelectTemsilciStatusu('temsilciStatusu', temsilci.temsilciStatusu);
            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeriItem.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeriItem.isyeriid);
            html = html.replace(new RegExp('{{obj.temsilci.temsilciid}}', 'g'), temsilci.temsilciid);
            html = html.replace(new RegExp('{{obj.temsilci.sira}}', 'g'), temsilci.sira);
            html = html.replace(new RegExp('{{obj.isyeri.uyeid}}', 'g'), temsilci.uyeid);
            html = html.replace(new RegExp('{{obj.isyeri.uyeid}}', 'g'), temsilci.uyeid);
            html = html.replace(new RegExp('{{obj.isyeri.atamaTarihi}}', 'g'), temsilci.atamaTarihi);
            html = html.replace(new RegExp('{{obj.isyeri.secimTarihi}}', 'g'), temsilci.secimTarihi);


            //html = html.replace(new RegExp('{{obj.select.calisanlar}}', 'g'), strCalisanSelect);
            html = html.replace(new RegExp('{{obj.select.vazifeler}}', 'g'), strVazifeSelect);
            html = html.replace(new RegExp('{{obj.select.temsilciStatusu}}', 'g'), strTemsilciStatusu);

            if (temsilci.uyeid) {
                var changeValue=true;
                for (var i = 0; i < calisanlar.length; i++) {
                    var tmpItem = JSON.parse(calisanlar[i]);
                    if (temsilci.uyeid == tmpItem.uyeid) {
                        html = html.replace(new RegExp('{{obj.temsilci.data}}', 'g'), calisanlar[i]);
                        break;
                    }
                }
                if (changeValue){
                    html = html.replace(new RegExp('{{obj.temsilci.data}}', 'g'), '{}');
                }
            } else {
                html = html.replace(new RegExp('{{obj.temsilci.data}}', 'g'), '{}');
            }
            var calisanFullData = [];
            var htmlDatable = '';
            for (var i = 0; i < calisanlar.length; i++) {
                var calisan = JSON.parse(calisanlar[i]);
                calisanFullData.push(calisan);
                htmlDatable += '<tr class="gradeX selectRow" data-id="' + calisan.uyeid + '">' +
                    '<td>' + calisan.soyisim + '</td>' +
                    '<td>' + calisan.isim + '</td>' +
                    '</tr>';
            }


            html = html.replace(new RegExp('{{obj.data.isyericalisanList}}', 'g'), htmlDatable);
            html = html.replace(new RegExp('{{obj.calisanlar.data}}', 'g'), JSON.stringify(calisanFullData));
            tenginge.render(req, res, html);
        });
    },
    isyeriisverentemsilcileri: function (req, res, obj) {
        this.init.isyeriisverentemsilcileri(req, res, obj, function (temsilciler, isyeri, accesstype, html) {

            html = html.replace(new RegExp('{{obj.isyeri.isim}}', 'g'), isyeri.isim);
            html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), isyeri.isyeriid || '');
            html = html.replace(new RegExp('{{obj.isyeri.dkisverentems1}}', 'g'), temsilciler.dkisverentems1 || '');
            html = html.replace(new RegExp('{{obj.isyeri.dkisverentems2}}', 'g'), temsilciler.dkisverentems2 || '');
            html = html.replace(new RegExp('{{obj.isyeri.dkisverentemsydk1}}', 'g'), temsilciler.dkisverentemsydk1 || '');
            html = html.replace(new RegExp('{{obj.isyeri.dkisverentemsydk2}}', 'g'), temsilciler.dkisverentemsydk2 || '');

            tenginge.render(req, res, html);
        });
    }

};
module.exports = renderer;