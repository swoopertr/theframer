var tenginge = require('./../middleware/templating/engi');
var domMaker = require('./../middleware/templating/MakeDom');
var userManager = require('./../DataAccess/user');

var tespit = {
    init: {
        tespit: function (req, res, obj, cb) {
            var html = global.temps.tespitler;
            var userData = obj.user;
            var tespitler = obj.tespitler;
            var sendikaList = obj.sendikaList;
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);

                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
                html = html.replace(new RegExp('{{obj.tespit.count}}', 'g'), tespitler.length);

                cb && cb(tespitler, sendikaList, 1, html);

            });
        },
        tespitdetay: function (req, res, obj, cb) {
            var html = global.temps.tespitdetay;
            var userData = obj.user;
            var tespitData = obj.tespit;
            var isyeriList = obj.isyeriList;
            var sendikaList = obj.sendikaList;
            var subeList = obj.subeList;
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);

                var tmpList = {};
                for (var i = 0 ; i< subeList.length; i++){
                    var item = JSON.parse(subeList[i]);
                    tmpList[item.subeid]= item;
                }


                cb && cb(tespitData, isyeriList, sendikaList, tmpList, 1, html);
            });
        }
    },
    tespitler: function (req, res, obj) {
        this.init.tespit(req, res, obj, function (tespitItems, sendikaList, accesstype, html) {

            var htmlselectSendikaler = domMaker.generateSendikaSelectArrayDefaultValue(sendikaList, 'selectSubeler');

            html = html.replace(new RegExp('{{obj.select.Subeler}}', 'g'), htmlselectSendikaler);
            html = html.replace(new RegExp('{{obj.allTespitData}}', 'g'), JSON.stringify(tespitItems));
            html = html.replace(new RegExp('{{obj.allSendikaData}}', 'g'), JSON.stringify(sendikaList));

            var sendikaObj = {};
            for (var i = 0; i < sendikaList.length; i++) {
                var item = JSON.parse(sendikaList[i]);
                sendikaObj[item.sendikaid] = item.isim;
            }

            var htmlTespit = '';
            for (var i = 0; i < tespitItems.length; i++) {
                var item = JSON.parse(tespitItems[i]);
                var bTarihArr = item.basvurutarihi.split('T')[0].split('-');
                var bTarih = bTarihArr[2] + '.' + bTarihArr[1] + '.' + bTarihArr[0];
                var tarihArr = item.tarih.split('T')[0].split('-');
                var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];

                htmlTespit += '<tr class="gradeX bgcolored dataitem" dataitem="' + item.tespitcevapid + '">';
                htmlTespit += '<td class="clicker select-hover">' + sendikaObj[item.sendikaid] + '</td>';
                htmlTespit += '<td class="clicker select-hover">' + item.isim + '</td>';
                htmlTespit += '<td class="clicker select-hover">' + bTarih + '</td>';
                htmlTespit += '<td class="clicker select-hover">' + item.iscisayisi + '</td>';
                htmlTespit += '<td class="clicker select-hover">' + item.uyesayisi + '</td>';
                htmlTespit += '<td class="clicker select-hover">' + tarih + '</td>';
                htmlTespit += '<td class="clicker select-hover">' + item.evraksayi + '</td>';
                htmlTespit += '<tr>';
            }
            html = html.replace(new RegExp('{{obj.tblTespitler}}', 'g'), htmlTespit);
            tenginge.render(req, res, html);
        });
    },
    tespitdetay: function (req, res, obj) {
        this.init.tespitdetay(req, res, obj, function (tespitData, isyeriList, sendikaList, subeList, accesstype, html) {
            if (tespitData.length == 0) {
                tespitData.tespitcevapid = 0;
            } else {
                tespitData = JSON.parse(tespitData);
            }

            if (tespitData.tespitcevapid == 0) {
                var sendinkaSelect = domMaker.generateSendikaSelectArrayDefaultValue(sendikaList, 'sendikaselect');
                html = html.replace(new RegExp('{{obj.select.SendikaSelect}}', 'g'), sendinkaSelect);
                var htmlDatable = '';
                for (var i = 0; i < isyeriList.length; i++) {
                    var isyeri = JSON.parse(isyeriList[i]);
                    htmlDatable += '<tr class="gradeX selectRow" data-id="' + isyeri.isyeriid + '">' +
                        '<td>' + isyeri.isim + '</td>' +
                        '<td>' + global.dataenums.enums.orgutlenmeTipi[isyeri.orgutlenmedurum] + '</td>' +
                        '<td>' + subeList[''+isyeri.subeid].isim + '</td>' +
                        '<td>' + isyeri.calisansayisi + '</td>' +
                        '</tr>';
                }
                html = html.replace(new RegExp('{{obj.data.isyeriList}}', 'g'), htmlDatable);
                html = html.replace(new RegExp('{{obj.tespit.isyeriisim}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.tespit.calisansayisi}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.tespit.uyesayisi}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.tespit.evraksayisi}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.tespit.tespitid}}', 'g'), '0');

            } else {

                var sendinkaSelect = domMaker.generateSendikaSelectArrayDefaultValue(sendikaList, 'sendikaselect', tespitData.sendikaid);
                html = html.replace(new RegExp('{{obj.select.SendikaSelect}}', 'g'), sendinkaSelect);
                var htmlDatable = '';
                var selectedIsyeri = {};
                for (var i = 0; i < isyeriList.length; i++) {
                    var isyeri = JSON.parse(isyeriList[i]);
                    if (isyeri.isyeriid == tespitData.isyeriid) {
                        selectedIsyeri = isyeri;
                    }
                    htmlDatable += '<tr class="gradeX selectRow" data-id="' + isyeri.isyeriid + '">' +
                        '<td>' + isyeri.isim + '</td>' +
                        '<td>' + global.dataenums.enums.orgutlenmeTipi[isyeri.orgutlenmedurum] + '</td>' +
                        '</tr>';
                }

                var evrakTarihi = tespitData.tarih.split('T')[0];
                var basvuruTarihi = tespitData.basvurutarihi.split('T')[0];

                html = html.replace(new RegExp('{{obj.data.isyeriList}}', 'g'), htmlDatable);
                html = html.replace(new RegExp('{{obj.isyeri.isyeriid}}', 'g'), tespitData.isyeriid);
                html = html.replace(new RegExp('{{obj.tespit.isyeriisim}}', 'g'), selectedIsyeri.isim);
                html = html.replace(new RegExp('{{obj.tespit.calisansayisi}}', 'g'), tespitData.iscisayisi);
                html = html.replace(new RegExp('{{obj.tespit.uyesayisi}}', 'g'), tespitData.uyesayisi);
                html = html.replace(new RegExp('{{obj.tespit.evraksayisi}}', 'g'), tespitData.evraksayi);
                html = html.replace(new RegExp('{{obj.tespit.basvuruTarihi}}', 'g'), basvuruTarihi);
                html = html.replace(new RegExp('{{obj.tespit.evraktarihi}}', 'g'), evrakTarihi);
                html = html.replace(new RegExp('{{obj.tespit.tespitid}}', 'g'), tespitData.tespitcevapid);
            }

            tenginge.render(req, res, html);
        });
    }
};


module.exports = tespit;