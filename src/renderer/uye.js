var tengine = require('./../middleware/templating/engi');
var domMaker = require('./../middleware/templating/MakeDom');
var userManager = require('./../DataAccess/user');
var isyeriManager = require('./../DataAccess/isyeri');
var subeManager = require('./../DataAccess/sube');
var setting = require('./../setting');
var uyeRenderer = {
    init: {
        uyeler: function (req, res, obj, cb) {
            var html = global.temps.uyeler;
            var userData = obj.user;
            var uyeList = obj.uyelist;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(uyeList, accesstype, html);
                }
            });
        },
        uyeDetay:function (req, res, obj, cb) {
            var html = global.temps.uyedetay;
            var userData = obj.user;
            var uyeItem = obj.uye;
            var harekets= obj.harekets;
            var isyeriList = obj.isyeriList;
            var subeList = obj.subeList;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(uyeItem, harekets, isyeriList, subeList, accesstype, html);
                }
            });
        },
        yeniuye:function (req, res,obj, cb) {
            var html = global.temps.yeniuye;
            var userData = obj.user;
            var isyeriList = obj.isyeriList;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html, isyeriList);
                }
            });
        }
    },
    uyeler: function (req, res, obj) {
        this.init.uyeler(req, res, obj, function (uyeList, accessType, html) {
            var hareketcinsiSelect = domMaker.generateSelect(global.dataenums.enums.hareketCinsi,'hareketcinsiType');

            var htmlUyelist = '';
            for (var j = 0; j < uyeList.length; j++) {
                var uyeItem = JSON.parse(uyeList[j]);
                var hareketTarihiArr = uyeItem.sonharekettarihi.split('T');
                var hareketTarihiDateArr = hareketTarihiArr[0].split('-');
                var hareketTarihi = hareketTarihiDateArr[2]+'.'+hareketTarihiDateArr[1]+'.'+hareketTarihiDateArr[0];
                htmlUyelist += '<tr class="gradeX dataitem" dataitem ="'+uyeItem.uyeid+'" >';
                htmlUyelist += '<td class="clicker">' + uyeItem.soyisim + '</td>';
                htmlUyelist += '<td class="clicker">' + uyeItem.isim + '</td>';
                htmlUyelist += '<td class="clicker">' + uyeItem.babaismi + '</td>';
                htmlUyelist += '<td class="clicker">' + ((uyeItem.vatandaslik_no === null) ? '' : uyeItem.vatandaslik_no )+ '</td>';
                htmlUyelist += '<td class="clicker">' + uyeItem.isyeriisim + '</td>';
                htmlUyelist += '<td class="clicker">' + hareketTarihi + '</td>';
                htmlUyelist += '<td class="clicker">' + ((uyeItem.sonharekettipi === null) ? '' : global.dataenums.enums.hareketCinsi[uyeItem.sonharekettipi]) + '</td>';
                htmlUyelist += '</tr>';
            }
            html = html.replace(new RegExp('{{obj.uyelist}}', 'g'), htmlUyelist);
            html = html.replace(new RegExp('{{obj.select.harekettipi}}', 'g'), hareketcinsiSelect);
            html = html.replace(new RegExp('{{obj.pagingItemCount}}', 'g'), setting.pagingCount);

            tengine.render(req, res, html);
        });
    },
    uyeDetay:function (req, res, obj) {
      this.init.uyeDetay(req, res, obj, function (uyeItem, harekets, isyeriList, subeList, accessType, html) {

        var sexSelect = domMaker.generateSelect(global.dataenums.enums.kadinerkek, 'sexSelect', uyeItem.kadinerkek);
        var eduSelect = domMaker.generateSelect(global.dataenums.enums.egitim, 'eduSelect', uyeItem.egitim);
        var bloodSelect = domMaker.generateSelect(global.dataenums.enums.kangrubu, 'bloodSelect', uyeItem.kangrubu);
        var isyeriSelect = domMaker.generateIsyeriSelectJsonArrayDefaultValue(isyeriList,'isyeriSelect');

        html = html.replace(new RegExp('{{obj.uye.uyeid}}', 'g'), uyeItem.uyeid);
        html = html.replace(new RegExp('{{obj.uye.vatandaslik_no}}', 'g'), uyeItem.vatandaslik_no);
        html = html.replace(new RegExp('{{obj.uye.isim}}', 'g'), uyeItem.isim);
        html = html.replace(new RegExp('{{obj.uye.soyisim}}', 'g'), uyeItem.soyisim);
        html = html.replace(new RegExp('{{obj.uye.email}}', 'g'), uyeItem.email);
        html = html.replace(new RegExp('{{obj.uye.telefon}}', 'g'), uyeItem.telefon);
        html = html.replace(new RegExp('{{obj.uye.babaismi}}', 'g'), uyeItem.babaismi);
        html = html.replace(new RegExp('{{obj.uye.anaismi}}', 'g'), uyeItem.anaismi);
        html = html.replace(new RegExp('{{obj.uye.dogumyeri}}', 'g'), uyeItem.dogumyeri);
        var dogumyili = '';
        if (uyeItem.tf_dtar != undefined) {
            console.log(uyeItem.tf_dtar);
            var thebirthDate = global.core.addHour(uyeItem.tf_dtar,4);

            dogumyili= thebirthDate.toJSON().split("T")[0];
            console.log(dogumyili.toString());
        }
        html = html.replace(new RegExp('{{obj.uye.dogumyili}}', 'g'), dogumyili);
        html = html.replace(new RegExp('{{obj.select.sex}}', 'g'), sexSelect);
        html = html.replace(new RegExp('{{obj.select.egitim}}', 'g'), eduSelect);
        html = html.replace(new RegExp('{{obj.select.kangrubu}}', 'g'), bloodSelect);
        html = html.replace(new RegExp('{{obj.uye.sskno}}', 'g'), uyeItem.sskno);
        html = html.replace(new RegExp('{{obj.uye.nkil}}', 'g'), uyeItem.nkil);
        html = html.replace(new RegExp('{{obj.uye.nkilce}}', 'g'), uyeItem.nkilce);
        html = html.replace(new RegExp('{{obj.uye.nkkoy}}', 'g'), uyeItem.nkkoy);
        html = html.replace(new RegExp('{{obj.uye.nkmahalle}}', 'g'), uyeItem.nkmahalle);
        html = html.replace(new RegExp('{{obj.uye.nkcilt}}', 'g'), uyeItem.nkcilt);
        html = html.replace(new RegExp('{{obj.uye.nkhane}}', 'g'), uyeItem.nkhane);
        html = html.replace(new RegExp('{{obj.uye.nksayfa}}', 'g'), uyeItem.nksayfa);
        html = html.replace(new RegExp('{{obj.uye.aciklama}}', 'g'), uyeItem.aciklama);
        html = html.replace(new RegExp('{{obj.uye.email}}', 'g'), uyeItem.email);
        html = html.replace(new RegExp('{{obj.data}}', 'g'), JSON.stringify(harekets));
        html = html.replace(new RegExp('{{obj.isyeriList}}', 'g'), JSON.stringify(isyeriList));
        html = html.replace(new RegExp('{{obj.data.isyerilist}}', 'g'), isyeriSelect);
          html = html.replace(new RegExp('{{obj.subeList}}', 'g'), JSON.stringify(subeList));


          var htmlDatable = '';
          for (var i = 0; i < isyeriList.length;i++){
              htmlDatable +='<tr class="gradeX selectRow" data-id="'+isyeriList[i].isyeriid+'">'+
                  '<td>'+isyeriList[i].isyeriisim+'</td>'+
                  '<td>'+global.dataenums.enums.orgutlenmeTipi[isyeriList[i].orgutlenmedurum]+'</td>'+
                  '<td>'+isyeriList[i].subeisim+'</td>'+
                  '</tr>';
          }
          html = html.replace(new RegExp('{{obj.data.isyeriList}}', 'g'), htmlDatable);


        tengine.render(req, res, html);
      });
    },
    yeniuye:function (req, res, obj) {
        this.init.yeniuye(req, res, obj, function (accesstype, html, isyeriList) {

            var sexSelect = domMaker.generateSelect(global.dataenums.enums.kadinerkek, 'sexSelect',1);
            var eduSelect = domMaker.generateSelect(global.dataenums.enums.egitim, 'eduSelect');
            var bloodSelect = domMaker.generateSelect(global.dataenums.enums.kangrubu, 'bloodSelect');
            //var isyeriSelect = domMaker.generateIsyeriSelectJsonArrayDefaultValue(isyeriList,'isyeriSelect');

            var htmlDatable = '';
            for (var i = 0; i < isyeriList.length;i++){
                htmlDatable +='<tr class="gradeX selectRow" data-id="'+isyeriList[i].isyeriid+'">'+
                        '<td>'+isyeriList[i].isyeriisim+'</td>'+
                        '<td>'+global.dataenums.enums.orgutlenmeTipi[isyeriList[i].orgutlenmedurum]+'</td>'+
                        '<td>'+isyeriList[i].subeisim+'</td>'+
                    '</tr>';
            }
            html = html.replace(new RegExp('{{obj.data.isyeriList}}', 'g'), htmlDatable);
            html = html.replace(new RegExp('{{obj.select.sex}}', 'g'), sexSelect);
            html = html.replace(new RegExp('{{obj.select.egitim}}', 'g'), eduSelect);
            html = html.replace(new RegExp('{{obj.select.kangrubu}}', 'g'), bloodSelect);
            //html = html.replace(new RegExp('{{obj.data.isyerilist}}', 'g'), isyeriSelect);
            html = html.replace(new RegExp('{{obj.isyeriList}}', 'g'), JSON.stringify(isyeriList));

            tengine.render(req, res, html);
        });
    }
};

module.exports = uyeRenderer;