var tengine = require('./../middleware/templating/engi');
var domMaker = require('./../middleware/templating/MakeDom');
var userManager = require('./../DataAccess/user');

var hareketRenderer = {
    init: {
        detay: function (req, res, obj, cb) {
            var html = global.temps.hareketdetay;
            var userData = obj.user;
            var uyeItem = obj.uye;
            var hareket = obj.hareket;
            var isyeriItem = obj.isyeri;
            var isyeriList = obj.isyeriList;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(uyeItem, hareket, isyeriItem, isyeriList, accesstype, html);
                }
            }); 
        }
    },
    detay: function (req ,res, obj) {
        this.init.detay(req, res, obj,function (uyeItem, hareketItem, isyeriItem, isyeriList, accesstype, html) {
            var hareket;
            if (hareketItem == null){
             hareketItem = {
                 hareketid :0
             };
                hareket = hareketItem;
            }else{
                hareket = JSON.parse(hareketItem[0]);
            }

            var hareketSelect = domMaker.generateSelect(global.dataenums.enums.hareketCinsi, 'hareketSelect', hareket.cins);
            var uye = JSON.parse(uyeItem[0]);
            var isyeri = (isyeriItem==null)? null : JSON.parse(isyeriItem[0]);

            html = html.replace(new RegExp('{{obj.uye.isim}}', 'g'), uye.isim);
            html = html.replace(new RegExp('{{obj.uye.soyisim}}', 'g'), uye.soyisim);
            html = html.replace(new RegExp('{{obj.hareket.uyeid}}', 'g'), uye.uyeid);
            html = html.replace(new RegExp('{{obj.select.hareket}}', 'g'), hareketSelect);

            var isyeriSelect ='';
            if(hareket.hareketid === 0){
                isyeriSelect = domMaker.generateIsyeriWithSubeDataSelectArrayDefaultValue(isyeriList,"isyeriDom",(isyeri==null)? undefined: isyeri.isyeriid);//todo: take care with new item insert.
                html = html.replace(new RegExp('{{obj.hareket.tarih}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.kararno}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.isyeriisim}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.noter}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.notertarih}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.noteryevmiye}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.kayitsirano}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.dogrulama_kodu}}', 'g'), "");
                html = html.replace(new RegExp('{{obj.hareket.hareketid}}', 'g'), "0");
            }else {
                isyeriSelect = domMaker.generateIsyeriSelectArrayDefaultValue(isyeriList,"isyeriDom",(isyeri==null)? undefined: isyeri.isyeriid);//todo: take care with new item insert.
                html = html.replace(new RegExp('{{obj.hareket.tarih}}', 'g'), hareket.tarih);
                html = html.replace(new RegExp('{{obj.hareket.kararno}}', 'g'), hareket.kararno);
                if (isyeri == null){
                    isyeri={isim:''};
                }
                html = html.replace(new RegExp('{{obj.hareket.isyeriisim}}', 'g'), isyeri.isim);
                html = html.replace(new RegExp('{{obj.hareket.noter}}', 'g'), hareket.noter);
                html = html.replace(new RegExp('{{obj.hareket.notertarih}}', 'g'), hareket.notertarih);
                html = html.replace(new RegExp('{{obj.hareket.noteryevmiye}}', 'g'), hareket.noteryevmiye);
                html = html.replace(new RegExp('{{obj.hareket.kayitsirano}}', 'g'), hareket.kayitsirano);
                html = html.replace(new RegExp('{{obj.hareket.dogrulama_kodu}}', 'g'), hareket.dogrulama_kodu);
                html = html.replace(new RegExp('{{obj.hareket.hareketid}}', 'g'), hareket.hareketid);
            }
            html = html.replace(new RegExp('{{obj.select.isyerleri}}', 'g'), isyeriSelect);

            tengine.render(req, res, html);

        });
    }
};


module.exports = hareketRenderer;