var tengine = require('./../middleware/templating/engi');
var userManager = require('./../DataAccess/user');
var domMaker = require('./../middleware/templating/MakeDom');

var genelraporRenderer = {
    init: {
        subeler: function (req, res, obj, cb) {
            var html = global.temps.genelraporlarsube;
            var userData = obj.user;
            var subeList = obj.subeList;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(subeList, accesstype, html);
                }
            });
        },
        istatistik: function (req, res, obj, cb) {
            var html = global.temps.genelraporlaristatistik;
            var userData = obj.user;
            var data = obj.data;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(data, accesstype, html);
                }
            });
        },
        genelraporlarisimligiriscikis: function (req, res, obj, cb) {
            var html = global.temps.genelraporlarisimligiriscikis;
            var userData = obj.user;
            var data = obj.data;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html);
                }
            });
        },
        genelraporlargiriscikis: function (req, res, obj, cb) {
            var html = global.temps.genelraporlargiriscikis;
            var userData = obj.user;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html);
                }
            });
        },
        genelraporlarkayitlistesi:function (req, res, obj, cb) {
            var html = global.temps.genelraporlarkayitlistesi;
            var userData = obj.user;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html);
                }
            });
        },
        genelraporlarkayitdefteri: function (req, res, obj, cb) {
            var html = global.temps.genelraporlarkayitdefteri;
            var userData = obj.user;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html);
                }
            });
        },
        genelraporlarorgbaslangic:function (req, res, obj, cb) {
            var html = global.temps.genelraporlarorgbaslangic;
            var userData = obj.user;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html);
                }
            });
        },
        genelraporlaruyeliklistesi:function (req, res, obj, cb) {
            var html = global.temps.genelraporlaruyeliklistesi;
            var userData = obj.user;

            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html);
                }
            });
        },
        genelraporlarsonhareketler : function (req, res, obj, cb) {
            var html = global.temps.genelraporlarsonhareketler;
            var userData = obj.user;
            var hareketList= obj.hareketList;
            var gun = obj.gun;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var subeMenuId = 5;
                var htmlMenuItems = '';
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (subeMenuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(hareketList, gun, accesstype, html);
                }
            });
        }
    },
    subeler: function (req, res, obj) {
        this.init.subeler(req, res, obj, function (subelist, accesstype, html) {
            var htmlTbl = '';
            for (var i = 0; i < subelist.length; i++) {
                var item = JSON.parse(subelist[i]);
                var tr = '<tr class="gradeX dataitem" >';
                tr += '<td >' + item.isim + '</td>';
                tr += '<td >' + item.telefonno + '</td>';
                tr += '<td >' + item.adres + '</td>';
                tr += '</tr>';
                htmlTbl += tr;
            }
            html = html.replace(new RegExp('{{obj.subelist}}', 'g'), htmlTbl);

            tengine.render(req, res, html);
        });
    },
    istatistik: function (req, res, obj) {
        this.init.istatistik(req, res, obj, function (result, accesstype, html) {
            var orgutlenmeSelect = domMaker.generateSelect(global.dataenums.enums.orgutlenmeTipi, 'orgutlenmeType', '1');

            var totalCount = 0;
            var tblHtml = '';
            var jsonResult = [];
            var resultLen = result.length;
            for (var i = 0 ; i< resultLen; i++){
                jsonResult.push(JSON.parse(result[i]));
            }
            jsonResult.sort(global.core.turkishSort);

            for (var i = 0; i < jsonResult.length; i++) {
                var item = jsonResult[i];
                tblHtml += '<tr class="gradeX dataitem">';
                tblHtml += '<td>' + item.isim + '</td>';
                tblHtml += '<td>' + item.calisan + '</td>';
                tblHtml += '<td>' + item.erkek + '</td>';
                tblHtml += '<td>' + item.kadin + '</td>';
                tblHtml += '</tr>';
                totalCount += parseInt(item.calisan);
            }


            html = html.replace(new RegExp('{{obj.calisan.count}}', 'g'), totalCount);
            html = html.replace(new RegExp('{{obj.isyeriIstatistikList}}', 'g'), tblHtml);
            html = html.replace(new RegExp('{{obj.select.orgutlenme}}', 'g'), orgutlenmeSelect);
            tengine.render(req, res, html);

        });
    },
    genelraporlarisimligiriscikis: function (req, res, obj) {
        this.init.genelraporlarisimligiriscikis(req, res, obj, function (accesstype, html) {
            var hareketSelect = domMaker.generateSelect(global.dataenums.enums.hareketCinsi, 'hareketType');
            html = html.replace(new RegExp('{{obj.select.hareketipi}}', 'g'), hareketSelect);
            tengine.render(req, res, html);
        });
    },
    genelraporlargiriscikis: function (req, res, obj) {
        this.init.genelraporlargiriscikis(req, res, obj, function (accesstype, html) {
            tengine.render(req, res, html);
        });
    },
    genelraporlarkayitlistesi:function (req, res, obj) {
        this.init.genelraporlarkayitlistesi(req, res, obj, function (accesstype, html) {
            tengine.render(req, res, html);
        });
    },
    genelraporlarkayitdefteri:function (req, res, obj) {
        this.init.genelraporlarkayitdefteri(req, res, obj, function (accesstype, html) {
           tengine.render(req, res, html);
        });
    },
    genelraporlarorgbaslangic:function (req, res, obj) {
        this.init.genelraporlarorgbaslangic(req,res, obj, function (accesstype, html) {
            tengine.render(req, res, html);
        });
    },
    genelraporlaruyeliklistesi:function (req, res, obj) {
        this.init.genelraporlaruyeliklistesi(req, res, obj, function(accesstype, html){

            var orgutlenmeTypeSelect = domMaker.generateSelect(global.dataenums.enums.orgutlenmeTipi, 'orgutlenmeType');
            html = html.replace(new RegExp('{{obj.select.orgutlenmetype}}', 'g'), orgutlenmeTypeSelect);

            tengine.render(req, res, html);
        });
    },
    genelraporlarsonhareketler: function (req, res, obj) {
        this.init.genelraporlarsonhareketler(req, res, obj, function (result,gun, accesstype, html) {
            var htmlList = '';
            var obj =[];
            for (var i = 0 ; i< result.length; i++){
                var theItem = JSON.parse(result[i]);
                htmlList +='<tr>';
                var arrTarih = theItem.tarih.split('T')[0].split('-');
                var strTarih = arrTarih[2]+'.'+arrTarih[1]+'.'+arrTarih[0];
                htmlList += '<td>' + (i+1) + '</td>';
                htmlList += '<td>' + strTarih + '</td>';
                htmlList += '<td>' + theItem.isim + '</td>';
                htmlList += '<td>' + theItem.soyisim + '</td>';
                htmlList += '<td>' + theItem.vatandaslik_no + '</td>';
                htmlList += '<td>' + theItem.isyeriisim + '</td>';
                htmlList += '<td>' + theItem.bcmdosyano + '</td>';
                htmlList += '<td>' + theItem.subeisim + '</td>';
                htmlList += '<td>' + global.dataenums.enums.hareketCinsi[theItem.cins] + '</td>';
                htmlList +='</tr>';
                obj.push({
                    'No':(i+1),
                    'Tarih':strTarih,
                    'Üye': theItem.isim +' '+ theItem.soyisim,
                    'İşyeri': theItem.isyeriisim,
                    'BCM_NO': theItem.bcmdosyano,
                    'Şube': theItem.subeisim,
                });
            }

            html = html.replace(new RegExp('{{obj.prm.gun}}', 'g'), gun);
            html = html.replace(new RegExp('{{obj.data.list}}', 'g'), htmlList);
            html = html.replace(new RegExp('{{obj.data.pure}}', 'g'), JSON.stringify(obj));
            tengine.render(req, res, html);
        });
    }

};
module.exports = genelraporRenderer;