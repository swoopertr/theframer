var tengine = require('./../middleware/templating/engi');
var userManager = require('./../DataAccess/user');
var domMaker = require('./../middleware/templating/MakeDom');
const zlib = require("zlib");

var educationRenderer = {
    init: {
        main: function (req, res, obj, cb) {
            var html = global.temps.homepageEducation;
            var userData = obj.user;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html);
                }
            });
        },
        listeducationTypes: function (req, res, obj, cb) {
            var html = global.temps.educationCategoryList;
            var userData = obj.user;
            var list = obj.list;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html, list);
                }
            });
        },
        editeducationCategory: function (req, res, obj, cb) {
            var html = global.temps.educationCategoryAddEdit;
            var userData = obj.user;
            var data = obj.data;
            var upperList = obj.upperList;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html, data, upperList);
                }
            });
        },
        egitimTipiDetay: function (req, res, obj, cb) {
            var html = global.temps.educationCategoryDetail;
            var userData = obj.user;
            var data = obj.data;
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                if (typeof cb !== "undefined") {
                    cb(accesstype, html, data);
                }
            });
        },
        teachers: function (req, res, obj, cb) {
            var html = global.temps.educationTeachers;
            var userData = obj.user;
            var teachers = obj.teachers
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                cb && cb(accesstype, html, teachers);
            });
        },
        teacher: function (req, res, obj, cb){
            var html = global.temps.educationTeacherDetail;
            var userData = obj.user;
            var teacher = obj.teacher
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                cb && cb(accesstype, html, teacher);
            });
        },
        educationList: function(req, res, obj, cb){
            var html = global.temps.educationItemList;
            var userData = obj.user;
            var educationList = obj.list
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                cb && cb(accesstype, html, educationList);
            });
        },
        educationCreate : function(req, res, obj, cb){
            var html = global.temps.educationCreate;
            var userData = obj.user;
            //var educationList = obj.list
            html = html.replace(new RegExp('{{obj.user.name}}', 'g'), userData.firstname + ' ' + userData.lastname);
            userManager.getPermissions(userData.userid, function (perms) {
                var menuId = 3;
                var htmlMenuItems = '';
                var accesstype = 0;
                for (var i = 0; i < perms.length; i++) {
                    var menuItem = JSON.parse(perms[i]);
                    if (menuId === menuItem.id) {
                        accesstype = menuItem.permissiontype;
                    }
                    htmlMenuItems += "<li><a href='" + menuItem.menulink + "'><i class='" + menuItem.menuclass + "'></i> <span>" + menuItem.menuname + "</span></a></li>";
                }
                if (accesstype !== 1) {
                    global.core.redirect(res, "/izinyok");
                    return;
                }
                html = html.replace(new RegExp('{{obj.menuItems}}', 'g'), htmlMenuItems);
                cb && cb(accesstype, html);
            });
        }
    },
    main: function (req, res, obj) {
        this.init.main(req, res, obj, function (accesstype, html) {
            tengine.render(req, res, html);
        });
    },
    listeducationTypes: function (req, res, obj) {
        this.init.listeducationTypes(req, res, obj, function (accesstype, html, list) {
            var htmlTbl = '';
            var len = list.length;
            for (var i = 0; i < len; i++) {
                var item = JSON.parse(list[i]);
                var tr = '<tr>';
                tr += '<td class="gradeX">' + item.Id + '</td>';
                tr += '<td class="gradeX">' + item.educationType + '</td>';
                tr += '<td class="gradeX" data-parent-id>' + (item.parentname == null ? '-' : item.parentname) + '</td>';
                tr += '<td class="gradeX">' + (item.status == 0 ? 'Pasif' : 'Aktif') + '</td>';
                //tr += '<td class="gradeX">' + (item.ex_props == null ? '' : 'bas bişiler') + '</td>';
                tr += '<td>'
                if (accesstype === 1) { // 1 for all access
                    tr += '<a href="/egitimTipiEdit?Id=' + item.Id + '" class="btn btn-primary btn-mini">Düzenle</a>';
                }
                //tr += ' <a href="/egitimTipiDetay?Id=' + item.Id + '" class="btn btn-success btn-mini">Detay</a>';
                tr += '</td>'
                tr += '</tr>';
                htmlTbl += tr;
            }
            html = html.replace(new RegExp('{{obj.educationCategoryList}}', 'g'), htmlTbl);
            tengine.render(req, res, html);
        });
    },
    editeducationCategory: function (req, res, obj) {
        this.init.editeducationCategory(req, res, obj, function (accesstype, html, edu, list) {
            let item = edu;
            let strHtml = '';
            let eduSystem = {};
            const listLen = list.length;
            for (let i = 0; i< listLen; i++){
                let tmp_item = list[i];
                if(item.Id != 0){
                    if (item.Id == tmp_item.Id){
                        continue;
                    }
                }
                eduSystem[tmp_item.Id] = tmp_item.educationType;
            }
            if(item.Id==0){
                html = html.replace(new RegExp('{{obj.id}}', 'g'),0);
                html = html.replace(new RegExp('{{obj.edu.typeName}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.aciklama}}', 'g'), '');
                var htmlSelect = domMaker.generateSelect(global.dataenums.enums.status, 'statusSelect',1);
                html = html.replace(new RegExp('{{obj.select.status}}', 'g'), htmlSelect);
                let htmlRoofSelect = domMaker.generateSelect(eduSystem, 'selectRoof');
                html = html.replace(new RegExp('{{obj.select.parent}}', 'g'), htmlRoofSelect);
            }else{
                html = html.replace(new RegExp('{{obj.id}}', 'g'),item.Id);
                html = html.replace(new RegExp('{{obj.edu.typeName}}', 'g'), item.educationType);
                html = html.replace(new RegExp('{{obj.aciklama}}', 'g'), item.description);
                var htmlSelect = domMaker.generateSelect(global.dataenums.enums.status, 'statusSelect',item.status);
                html = html.replace(new RegExp('{{obj.select.status}}', 'g'), htmlSelect);
                let htmlRoofSelect = domMaker.generateSelect(eduSystem, 'selectRoof', item.parentId);
                html = html.replace(new RegExp('{{obj.select.parent}}', 'g'), htmlRoofSelect);
            }

            html = html.replace(new RegExp('{{data.this}}', 'g'), JSON.stringify(edu));
            html = html.replace(new RegExp('{{data.list}}', 'g'), JSON.stringify(list));
            tengine.render(req, res, html);
        });
    },
    egitimTipiDetay: function (req, res, obj) {
        this.init.egitimTipiDetay(req, res, obj, function (accesstype, html) {
            tengine.render(req, res, html);
        });
    },
    teachers: function (req, res, obj) {
        this.init.teachers(req, res, obj, function (accesstype, html, teachers) {
            var teacherLen = teachers.length;
            var teacherHtml = '';
            for (var i = 0; i < teacherLen; i++) {
                var item = JSON.parse(teachers[i]);
                var teacherRow = '<tr>';
                teacherRow += '<th style="color: #000000">' + (i + 1) + '</th>';
                teacherRow += '<th style="color: #000000">' + item.Name +' '+item.Surname+ '</th>';
                teacherRow += '<th style="color: #000000">' + global.dataenums.enums.ogretmenTipi[item.EducatorType] + '</th>';
                teacherRow += '<th style="color: #000000">' + item.Phone + '</th>';
                teacherRow += '<th style="color: #000000">' + item.TcNo + '</th>';
                teacherRow += '<th style="color: #000000">' + (item.Status == "1" ? "Aktif" : "Pasif") + '</th>';
                if (accesstype === 1) {
                    teacherRow += '<th style="color: #000000"><a href="/egitimcidetay?id='+item.Id+'">Düzenle</a></th>';
                }
                teacherRow += '</tr>';

                teacherHtml += teacherRow;
            }
            html = html.replace(new RegExp('{{obj.teachers}}', 'g'), teacherHtml);
            tengine.render(req, res, html);
        });
    },
    teacher: function (req, res, obj){
        this.init.teacher(req,res,obj, function (accesstype, html, teacher) {
            var htmlSelect ='';
            var htmlEducatorType = '';
            if (teacher.Id === 0){
                html = html.replace(new RegExp('{{obj.id}}', 'g'), 0);
                html = html.replace(new RegExp('{{obj.isim}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.soyad}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.telefon}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.tcId}}', 'g'), '');
                html = html.replace(new RegExp('{{obj.aciklama}}', 'g'), '');
                htmlSelect = domMaker.generateSelect(global.dataenums.enums.status, 'statusSelect',1);
                htmlEducatorType = domMaker.generateSelect(global.dataenums.enums.ogretmenTipi, 'statusEducatorType',1);
            }else {
                html = html.replace(new RegExp('{{obj.id}}', 'g'), teacher.Id);
                html = html.replace(new RegExp('{{obj.isim}}', 'g'), teacher.Name);
                html = html.replace(new RegExp('{{obj.soyad}}', 'g'), teacher.Surname);
                html = html.replace(new RegExp('{{obj.telefon}}', 'g'), teacher.Phone);
                html = html.replace(new RegExp('{{obj.tcId}}', 'g'), teacher.TcNo);
                html = html.replace(new RegExp('{{obj.aciklama}}', 'g'), teacher.Description);
                htmlSelect = domMaker.generateSelect(global.dataenums.enums.status, 'statusSelect',teacher.Status);
                htmlEducatorType = domMaker.generateSelect(global.dataenums.enums.ogretmenTipi, 'statusEducatorType',teacher.EducatorType);
            }
            html = html.replace(new RegExp('{{obj.Select.Status}}', 'g'), htmlSelect);
            html = html.replace(new RegExp('{{obj.Select.EducatorType}}', 'g'), htmlEducatorType);
            tengine.render(req, res, html);
        });
    },
    educationList: function(req, res, obj){
        this.init.educationList(req,res,obj, function (accesstype, html, list) {
            var htmlTbl = '';
            var len = list.length;
            for (var i = 0; i < len; i++) {
                var item = JSON.parse(list[i]);
                var tr = '<tr>';
                tr += '<td class="gradeX">' + item.Id + '</td>';
                tr += '<td class="gradeX">' + item.Name + '</td>';
                tr += '<td class="gradeX" data-parent-id>' + (item.educationType == null ? '-' : item.educationType) + '</td>';
                tr += '<td class="gradeX">' + (item.status == 0 ? 'Pasif' : 'Aktif') + '</td>';
                //tr += '<td class="gradeX">' + (item.ex_props == null ? '' : 'bas bişiler') + '</td>';
                tr += '<td>'
                if (accesstype === 1) { // 1 for all access
                    tr += '<a href="/egitimdetay?Id=' + item.Id + '" class="btn btn-primary btn-mini">Düzenle</a>';
                }
                tr += '</td>'
                tr += '</tr>';
                htmlTbl += tr;
            }
            html = html.replace(new RegExp('{{obj.educationList}}', 'g'), htmlTbl);
            tengine.render(req, res, html);
        });
    },
    educationCreate: function (req, res, obj) {
        this.init.educationCreate(req, res, obj, function (accesstype, html){
            tengine.render(req, res, html);
        });
    }
};

module.exports = educationRenderer;