
var router = require('./../route');

var servMe = function (req, res, callback){

    if(typeof (callback) != 'undefined'){
        router.callRoute(req, res, callback);
    }
};

module.exports.servMe = servMe;