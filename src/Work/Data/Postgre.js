var setting = require('./../../setting');
var pg = require('pg');
pg.defaults.ssl = false;
pg.types.setTypeParser(1114, function (stringValue) {
    var temp = new Date(stringValue);
    var result = new Date(Date.UTC(temp.getFullYear(), temp.getMonth(), temp.getDate(), temp.getHours(), temp.getMinutes(), temp.getSeconds(), temp.getMilliseconds()));
    return result;
});
pg.types.setTypeParser(1082, function (stringValue) {
    var temp = new Date(stringValue);
    var result = new Date(Date.UTC(temp.getFullYear(), temp.getMonth(), temp.getDate(), temp.getHours(), temp.getMinutes(), temp.getSeconds(), temp.getMilliseconds()));
    return result;
});
pg.types.setTypeParser(1184, function (stringValue) {
    var temp = new Date(stringValue);
    var result = new Date(Date.UTC(temp.getFullYear(), temp.getMonth(), temp.getDate(), temp.getHours(), temp.getMinutes(), temp.getSeconds(), temp.getMilliseconds()));
    return result;
});

const Pool = require('pg').Pool;
const pool = new Pool({
    user: setting.postGreDb.user,
    host: setting.postGreDb.ip,
    database: setting.postGreDb.dbname,
    password: setting.postGreDb.pass,
    port: setting.postGreDb.port,
});

var dataAccess = {
    query: function (query, cb, errcb) {
        console.log("query runs :" + query);
        pool.query(query, (err, results) => {
            if (err) {
                console.log('veritabanı bağlantı hatası : ' + err);
                errcb && errcb();
            }
            var data = [];


            if(results.hasOwnProperty('rows')){
                var resultLen = results.rows.length;
                for (var i = 0; i< resultLen ; i++){
                    data.push(JSON.stringify(results.rows[i]));
                }
            }else{
                var resultLen = results.length;
                for (var i = 0; i< resultLen ; i++){
                    data.push(JSON.stringify(results[i]));
                }
            }
            cb && cb(data);
        })

       /* pg.connect(setting.PostGreConnection, function (err, client) {
            if (err) {
                console.log('veritabanı bağlantı hatası : ' + err);
                errcb && errcb();
            }
            var data = [];
            client.query(query)
                .on('row', function (row) {
                    data.push(JSON.stringify(row));
                })
                .on('end', function () {
                    client.end();
                    cb(data);
                    pg.end();
                })
                .on('error', function (err) {
                    client.end();
                    cb({msg: 'error', err: err});
                    console.log("err :" + err);

                    pg.end();
                });
        });*/
    },
    getTables: function (cb) {
        pg.connect(setting.PostGreConnection, function (err, client) {
            if (err) throw err;
            var data = [];
            client.query(setting.PostGre.Query.GetAllTables)
                .on('row', function (row) {
                    data.push(JSON.stringify(row));
                })
                .on('end', function () {
                    client.end();
                    cb(data);
                });
        });
    },
    backupDatabase: function (cb) {
        pg.connect(setting.PostGreConnection, function (err, client) {
            if (err) throw err;
            var q = ''; //todo backup whole db query
            client.query(q)
                .on('row', function (row) {
                    data.push(JSON.stringify(row));
                })
                .on('end', function () {
                    client.end();
                    cb();
                });
        });
    }
};
module.exports = dataAccess;