var xlsx = require('xlsx');

var excelReader = {
    readExcel : function (file, cb) {
        var result = {};
        var workbook = xlsx.readFile(file);
        var sheet_name_list = workbook.SheetNames;
        for (var i = 0 ; i< sheet_name_list.length ; i++){
            result[sheet_name_list[i]] = xlsx.utils.sheet_to_json(workbook.Sheets[sheet_name_list[i]], defVal = "");
        }
        cb && cb(result);
    }
};
module.exports = excelReader;