var setting = require('./../setting');
var Sessions = require("./../middleware/Session/sessions");
var thework = require('./Basics');
var fs = require('fs');
var dir = setting.proj.Path;
var sessionHandler = new Sessions(null, {expires: setting.sessionExpireTime});//10 mins.

var getfileContentImg = function (res, filePath) {
    global.cache.get(filePath, function (fileConent) {
        if (fileConent !== undefined) {
            var binary = JSON.parse(fileConent);
            var img;
            try {
                img = Buffer.from(binary.data);

            } catch (err) {
                console.log(err);
                res.write('file doesnt exists');
                return;
            }
            res.end(img);
        } else {
            fs.readFile(dir + filePath, function (err, data) {
                if (err) {
                    console.log(err);
                    res.end('file doesnt exists');
                    return;
                }
                res.end(data);
                var strData = JSON.stringify(data);
                global.cache.set(filePath, strData);
            });

        }
    });
};

var getfileContent = function (res, filePath) {
    global.cache.get(filePath, function (fileConent) {
        if (fileConent !== undefined) {
            res.write(fileConent);
            res.end();
        } else {
            fs.readFile(dir + filePath, function (err, content) {
                if (err) {
                    console.log(err);
                    res.end('file doesnt exists');
                    return;
                }
                res.write(content);
                res.end();
                global.cache.set(filePath, content);
            });
        }
    });
};

var handler = function (req, res) {

    global.core.postHandler(req, res);
    if (req.url === '/favicon.ico') {                       // just blocks favico requests
        res.writeHead(200, {'Content-Type': 'image/x-icon'});
        res.end();
    } else if (req.url.indexOf('.css') !== -1) {    // serving css files
        res.writeHead(200, setting.TheHeaderCss);
        getfileContent(res, req.url);
    } else if (req.url.indexOf('.js') !== -1) {     // serving js files
        res.writeHead(200, setting.TheHeaderJavascript);
        getfileContent(res, req.url);
    } else if (req.url.indexOf('.png') !== -1) {    // serving png files
        res.writeHead(200, setting.TheHeaderPNG);
        getfileContentImg(res, req.url);
    } else if (req.url.indexOf('.jpeg') !== -1) {   // serving jpeg files
        res.writeHead(200, setting.TheHeaderJPEG);
        getfileContentImg(res, req.url);
    } else if (req.url.indexOf('.jpg') !== -1) {    // serving jpg files
        res.writeHead(200, setting.TheHeaderJPG);
        getfileContentImg(res, req.url);
    } else if (req.url.indexOf('.xlsx') !== -1) {   // serving xlsx files
        res.writeHead(200, setting.TheHeaderXLSX);
        getfileContentImg(res, req.url);
    } else {
        console.info("url : " + req.url);
        sessionHandler.httpRequest(req, res, function (err, sess) {
            req.sess = sess;
            thework.servMe(req, res, function (data) {
            });
        });
    }

};
module.exports.handler = handler;