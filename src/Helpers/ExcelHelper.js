var exporter = require('./../Work/Data/excelExporter');
var reader = require('./../Work/Data/excelReader');

var theHelper = {
    export: exporter,
    reader: reader
};

module.exports = theHelper;