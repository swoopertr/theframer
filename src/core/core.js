var setting = require('./../setting');
var excel = require('./../Work/Data/excelExporter');
var fs = require('fs');
var routes = require('./../route');

var alfabe = "AaBbCcÇçDdEeFfGgĞğHhIıİiJjKkLlMmNnOoÖöPpQqRrSsŞşTtUuÜüVvWwXxYyZz0123456789 ";

var getDictionaryFormData = function (formData, fields, cb) {
    var result = {};
    var hashes = decodeURIComponent(formData.toString());
    var datas = hashes.split('&');
    if(fields == 'all'){
        for (var i = 0; i < datas.length; i++) {
            var itemObj = datas[i].split('=');
            result[itemObj[0]] = itemObj[1];
        }
    }else{
        for (var i = 0; i < datas.length; i++) {
            var itemObj = datas[i].split('=');
            for (var j = 0; j < fields.length; j++) {
                if (fields[j] === itemObj[0]) {
                    result[fields[j]] = itemObj[1];
                }
            }
        }
    }

    
    cb && cb(result);
};

var redirect = function (res, path) {
    res.writeHead(302, {Location: path});
    res.end();
}

var sendMailer = function (to, subject, content, cb) {
    var nodemailer = require('nodemailer');
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        /*host: setting.email.host,
        port : setting.email.port,
        secure :setting.email.secure,*/
        auth: {
            user: setting.email.auth.user,
            pass: setting.email.auth.pass
        }
    });

    var opts = {
        from: setting.email.from,
        to: to,
        subject: subject,
        text: content
    };

    transporter.sendMail(opts, function (err, info) {
        if (err) {
            var usr = {
                Id: 0
            };
            //global.writeErr('mail err : ' + err, usr);
            console.log('mail err : ' + err, usr);
            cb && cb();
        } else {
            cb && cb();
            //global.writeLog(info);
        }
    });
};

var parseReq = function (req, cb) {
    var result = {};
    if (req.url.split('?').length === 1) {
        if (typeof cb === "function") {
            cb(result);
        }
    } else {
        var prmList = req.url.split('?')[1].split('&');
        for (var i = 0; i < prmList.length; i++) {
            var item = prmList[i].split('=');
            result[item[0]] = decodeURI(item[1]);
        }
        cb && cb(result);
    }
};

var callMethods = function (methArr, i, cb) {
    if (i === methArr.length - 1) {
        methArr[i](function () {
            cb && cb();
        });
    } else {
        methArr[i](function () {
            callMethods(methArr, i + 1, cb);
        });
    }
};

var checkLogin = function (req, res, cb) {
    var userData = req.sess.get('user');
    var isLogin = req.sess.get('isLogin');
    if (typeof userData === "undefined") {
        redirect(res, '/login');
    } else {
        if (isLogin === true) {
            cb && cb(userData, isLogin);
        } else {
            redirect(res, '/login');
        }
    }
};

var excelFileCreate = function (filename, sheetName, data, cb) {
    excel.GenerateExcelFile(filename, sheetName, data, cb);
};

var returnFile = function (fileName, res) {
    fs.exists(fileName, function (exists) {
        var sendFileName = fileName.substring(fileName.lastIndexOf('/') + 1);
        if (exists) {
            res.writeHead(200, {
                "Content-Type": "application/octet-stream",
                "Content-Disposition": "attachment; filename=" + sendFileName
            });
            fs.createReadStream(fileName).pipe(res);
        } else {
            res.writeHead(400, {"Content-Type": "text/plain"});
            res.end("ERROR File does not exist");
        }
    });
};

var returnJson = function (res, result) {
    res.writeHead(200, setting.TheHeaderJson);
    res.end(JSON.stringify(result));
};

var formatDatePostgre = function (date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month <= 9) ? ('0' + month) : month;
    var day = date.getDate();
    day = (day <= 9) ? '0' + day : day;
    var result = year + '-' + month + '-' + day;
    return result;
};

var formatDateWithTimePostgre = function (date) {
    var year = date.getFullYear();
    var month = date.getMonth() + 1;
    month = (month <= 9) ? ('0' + month) : month;
    var day = date.getDate();
    day = (day <= 9) ? '0' + day : day;
    var hour = date.getHours();
    hour = hour <= 9 ? '0' + hour : hour;
    var min = date.getMinutes();
    min = min < 9 ? '0' + min : min;
    var result = year + '-' + month + '-' + day + ' ' + hour + ':' + min + ':00';
    return result;
};

var addHour = function (dateString, hour) {
    var thedate = new Date(dateString);
    thedate.setTime(thedate.getTime() + (hour * 60 * 60 * 1000));
    return thedate;
};

var randomPls = function (len) {
    var chars = '';
    while (chars.length < len) {
        chars += Math.random().toString(36).substring(2);
    }
    return chars.substring(0, len);
};

var isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

var getDaysInMonth = function (year, month) {
    return [31, (isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};

var addMonths = function (theDate, value) {
    var n = theDate.getDate();
    theDate.setDate(1);
    theDate.setMonth(theDate.getMonth() + value);
    theDate.setDate(Math.min(n, getDaysInMonth(theDate.getFullYear(), theDate.getMonth())));
    return theDate;
};

var turkcesiralama = function (a, b) {
    var atitle = a.isim,
        btitle = b.isim,
        alen = atitle.length,
        blen = btitle.length;

    if (alen === 0 || blen === 0) {
        return 0;
    }
    for (var i = 0; i < alen && i < blen; i++) {
        var ai = alfabe.indexOf(atitle[i]);
        var bi = alfabe.indexOf(btitle[i]);
        if (ai !== bi) {
            return ai - bi;
        }
    }
};

var genericTurkcesiralama = function (a, b, field) {
    var atitle = a[field].replace(/[ ]/g, ""),
        btitle = b[field].replace(/[ ]/g, ""),
        alen = atitle.length,
        blen = btitle.length;

    if (alen === 0 || blen === 0) {
        return 0;
    }
    for (var i = 0; i < alen && i < blen; i++) {
        var ai = alfabe.indexOf(atitle[i]);
        var bi = alfabe.indexOf(btitle[i]);
        if (ai !== bi) {
            return ai - bi;
        }
    }
    if (alen > 0 && blen > 0) {
        if (alen > blen) {
            return 1;
        } else if (alen < blen) {
            return -1;
        }
    }
};

var genricSort = function (firstKey, secondKey) {
    return function (a, b) {
        var firstKeySortResult = genericTurkcesiralama(a, b, firstKey);
        if (firstKeySortResult > 0) {
            return 1;
        } else if (firstKeySortResult < 0) {
            return -1;
        } else {
            var secondKeySortResult = genericTurkcesiralama(a, b, secondKey);
            if (secondKeySortResult > 0) {
                return 1;
            } else if (secondKeySortResult < 0) {
                return -1;
            } else {
                return 0;
            }
        }
    }
};

var postHandler = function (req, res) {
    if (req.method !== "POST") {
        return;
    }

    var routeName = req.url.split('/');
    var routePath = routeName[1].split('?')[0];
    if (routes.routes.hasOwnProperty(routePath)) {
        var item = routes.routes[routePath];
        if (item.hasOwnProperty('file')) {
            return;
        }
    }

    req.on('data', function (data) {
        if (req.url)
            if (!req.formData) {
                req.formData = Buffer.from(data); //new Buffer(data, 'utf-8');
            }
    });
    req.on('end', function () {
        var body = req.formData.toString('utf-8');
        body = body.replace(/\+/g, ' ');
        req.formData = body;
        console.log(req.formData);
    });


};

var core = {
    init: function (cb) {
        global.core = {};
        this.defineUtil();
        if (typeof cb === "function") {
            cb();
        }
    },
    getFormData: getDictionaryFormData,
    redirect: redirect,
    defineUtil: function () {
        global.core.redirect = redirect;
        global.core.getFormData = getDictionaryFormData;
        global.core.sendMailer = sendMailer;
        global.core.parseReq = parseReq;
        global.core.callMethods = callMethods;
        global.core.checkLogin = checkLogin;
        global.core.excelCreate = excelFileCreate;
        global.core.returnJson = returnJson;
        global.core.returnFile = returnFile;
        global.core.formatDateTime = formatDateWithTimePostgre;
        global.core.formatDate = formatDatePostgre;
        global.core.daysInMonth = getDaysInMonth;
        global.core.addHour = addHour;
        global.core.addMonth = addMonths;
        global.core.random = randomPls;
        global.core.turkishSort = turkcesiralama;
        global.core.genricSort = genricSort;
        global.core.postHandler = postHandler;
    }
};
module.exports = core;