var redis = require("redis");
var setting = require("./../setting");
var client = redis.createClient(setting.Redis.Port, setting.Redis.IP);
client.on('error',function (err) {
  console.log('redis error ->' + err);
});

var redisCache= {
    get: function (key, callback) {
        client.get(key, function (err, reply) {
            if (err) {
                console.log('redis-error : ' + err);
            }
            callback(reply);
        });
    },
    setex: function (key, value) {
        client.set(key, value, 'EX', 300);//cache expire after 5 mins.
    },
    set: function (key, value) {
        client.set(key, value);//cache never expire.
    },
    send: function (command, cb) {
        client.send_command(command, null, function () {
          if (typeof cb !== "undefined") {
            cb();
          }
        });
    }
};
module.exports = redisCache;

