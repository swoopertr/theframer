var constants = {
    htmlStatusCodes:{
        text:{'Content-Type': 'text/plain'},
        json:{'Content-Type': 'application/json'},
        html:{'Content-Type': 'text/html'}
    }
};
module.exports = constants;