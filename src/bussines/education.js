var renderer = require('./../renderer/education');
var educationManager = require('./../DataAccess/education');

var educationBussines = {
    main: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var theObj = {
                user: userData
            };
            renderer.main(req, res, theObj);
        });
    },
    listeducationTypes: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            educationManager.types.getAll(function (result) {
                var theObj = {
                    user: userData,
                    list: result
                };
                renderer.listeducationTypes(req, res, theObj);
            });
        });
    },
    editeducationCategory: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                educationManager.types.getOne(qs.Id, function (result) {
                    educationManager.types.getAll( function (upperList) {
                        if (result.length == 0) {
                            result = {Id: 0};
                        } else {
                            result = JSON.parse(result[0]);
                        }

                        var len = upperList.length;
                        for (var i = 0; i < len; i++) {
                            var item = JSON.parse(upperList[i]) ;
                            if (item.Id == result.Id){
                                upperList.splice(i,1);
                                break;
                            }
                        }

                        len = upperList.length;
                        for (var i = 0; i < len; i++) {
                            upperList [i]= JSON.parse(upperList[i]) ;
                        }

                        var theObj = {
                            user: userData,
                            data: result,
                            upperList: upperList
                        };
                        renderer.editeducationCategory(req, res, theObj);
                    });
                });
            });
        });
    },
    egitimTipiDetay: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                educationManager.types.getOne(qs.Id, function (result) {
                    var theObj = {
                        user: userData,
                        data: result
                    };
                    renderer.egitimTipiDetay(req, res, theObj);
                });
            });
        });
    },
    teachers: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            educationManager.teacher.getTeachers(function (teachers) {
                var theObj = {
                    user: userData,
                    teachers: teachers
                };
                renderer.teachers(req, res, theObj);
            });
        });
    },
    educationTeacherDetail: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                educationManager.teacher.getTeacher(qs.id, function (teacher) {
                    if (teacher.length === 0) {
                        teacher = {Id: 0};
                    } else {
                        teacher = JSON.parse(teacher[0]);
                    }
                    var theObj = {
                        user: userData,
                        teacher: teacher
                    };
                    renderer.teacher(req, res, theObj);
                });
            });
        });
    },
    educationList: function (req ,res){
        global.core.checkLogin(req, res, function (userData, isLogin) {
            
            educationManager.education.getAllForPosing(function (result) {
                var theObj = {
                    user: userData,
                    list: result
                };
                renderer.educationList(req, res, theObj);
            });
            
        });
    },
    educationItemDetail: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                educationManager.education.getOne(qs.Id, function (result) {
                    var theObj = {
                        user: userData,
                        data: result
                    };
                    renderer.educationItemDetail(req, res, theObj);
                });
            });
        });
    },
    educationCreate: function (req, res){
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                renderer.educationCreate(req, res, {
                    user: userData
                });
            });
        });

    }
};


module.exports = educationBussines;