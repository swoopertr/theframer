var fs = require('fs');
var setting = require('./../setting');
var uyeManager = require('./../DataAccess/uye');
var uyeRenderer = require('./../renderer/uye');
var isyeriManager = require('./../DataAccess/isyeri');
var subeManager = require('./../DataAccess/sube');
var formidable = require('formidable');
var excelHelper = require('./../Helpers/ExcelHelper');

var uyeWork = {
	uyeListesi: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				var pageIndex = (qs.page === undefined) ? 1 : qs.page;
				var itemCount = setting.pagingCount;
				uyeManager.getAllForScreen(pageIndex, itemCount, function (result) {
					var dataObj = {
						uyelist: result,
						user: userData
					};
					uyeRenderer.uyeler(req, res, dataObj);
				});

			});
		});
	},
	getTotalCount: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			uyeManager.getTotalCount(function (result) {
				res.writeHead(200, setting.TheHeaderJson);
				res.end(JSON.stringify(result));
			});
		});
	},
	getTotalUyePagingCount: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			uyeManager.getTotalCount(function (totalItemCount) {
				res.writeHead(200, setting.TheHeaderJson);
				var pagingItemCount = setting.pagingCount;
				totalItemCount = JSON.parse(totalItemCount[0]).count;
				var pageCount = Math.floor(totalItemCount / pagingItemCount);
				var result = {result: pageCount};
				res.end(JSON.stringify(result));
			});
		});
	},
	getUyePagingData: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.getAllForScreen(qs.page, setting.pagingCount, function (result) {
					global.core.returnJson(res, result);
				});
			});

		})
	},
	uyeAjaxSearch: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.searchAllForScreen(qs.isim, qs.soyisim, qs.babaisim, qs.vatNo, qs.isyeriisim, qs.harekettipNo, qs.page, setting.pagingCount, function (result) {
					res.writeHead(200, setting.TheHeaderJson);
					res.end(JSON.stringify(result));
				});
			});
		});
	},
	uyeAjaxSearchCount: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.searchAllForScreenCount(qs.isim, qs.soyisim, qs.babaisim, qs.vatNo, qs.isyeriisim, qs.harekettipNo, function (result) {
					res.writeHead(200, setting.TheHeaderJson);
					res.end(JSON.stringify(result));
				});
			});
		});
	},
	searchinuye: function(req, res){
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.searchinuye(qs.name, qs.surname, qs.isyeriisim, function (result) {
					res.writeHead(200, setting.TheHeaderJson);
					res.end(JSON.stringify(result));
				});
			});
		});
	},
	uyedetay: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.getById(parseInt(qs.uyeid), function (uyeItem) {
					isyeriManager.getAllListWithorgulenmeDurumAndsubeInfo(function (isyeriListItems) {
						if (uyeItem.length == 0) {
							global.core.redirect(res, '/uye');
						} else {
							uyeItem = JSON.parse(uyeItem[0]);
							uyeManager.getUyeHareketbyId(uyeItem.uyeid, function (hareketItems) {
								subeManager.getList(function (subelist) {
								var hs = [];
								var isList = [];
								for (var i = 0; i < hareketItems.length; i++) {
									hs.push(JSON.parse(hareketItems[i]));
								}
								for (var i = 0; i < isyeriListItems.length; i++) {
									isList.push(JSON.parse(isyeriListItems[i]));
								}
								var obj = {
									user: userData,
									uye: uyeItem,
									harekets: hs,
									isyeriList: isList,
									subeList:subelist
								};
								uyeRenderer.uyeDetay(req, res, obj);
                                });
							});
						}

					});
				});
			});
		});
	},
	saveUyeDetay: function (req, res) {
		req.on('end', function () {
			var fields = ['uyeid', 'vatNo', 'isim', 'soyisim', 'babaisim', 'anaisimi',
				'dogumyeri', 'dogumyili', 'sskno', 'nkil', 'nkilce', 'nkkoy', 'telefon',
				'nkmahalle', 'nkcilt', 'nkhane', 'nksayfa', 'description', 'sexSelect',
				'eduSelect', 'bloodSelect', 'sonhareketipi', 'sonhareket','aciklama','email'];
			global.core.getFormData(req.formData, fields, function (data) {
				if (data.uyeid !== "0") {
					uyeManager.update(data, function (result) {
						var redirectUrl = '/uye?uyeid=' + data.uyeid;
						global.core.redirect(res, redirectUrl);
					});
				} else {
					uyeManager.insert(data, function (result) {
						var redirectUrl = '/uye?uyeid=' + result;
						global.core.redirect(res, redirectUrl);
					});
				}

			});
		});
	},
	yeniuyepostajax: function (req, res) {
		req.on('end', function () {
			var fields = ['uyeid', 'vatNo', 'isim', 'soyisim', 'babaisim', 'anaisimi',
				'dogumyeri', 'dogumyili', 'sskno', 'nkil', 'nkilce', 'nkkoy', 'telefon',
				'nkmahalle', 'nkcilt', 'nkhane', 'nksayfa', 'description', 'sexSelect',
				'eduSelect', 'bloodSelect', 'sonhareketipi', 'sonhareket', 'aciklama', 'email'];
			global.core.getFormData(req.formData, fields, function (data) {
				if (data.uyeid !== "0") {
					uyeManager.update(data, function (result) {
						res.end(JSON.stringify(data.uyeid));
					});
				} else {
					uyeManager.insert(data, function (result) {
                        res.end(''+result);
					});
				}
			});
		});
	},
	getexcelFile: function(req, res){
		var formidable_opts = {
			uploadDir : setting.excelFolder
		};
		var form = new formidable.IncomingForm(formidable_opts);
		form.parse(req, function (err, fields, files) {
			if (err) {throw err;}
			var theFile = files[Object.keys(files)[0]];
			var RandomFileName = global.core.random(6);
			var newPath = setting.excelFolder + '/' + RandomFileName+'.xlsx';
			fs.rename(theFile.path, newPath, function (errIn,a) {
				if (errIn) {throw errIn}
				excelHelper.reader.readExcel(newPath, async function(result) {
					var uyeler = result["uye"],
						uyelerLen = uyeler.length;
					var tcids =[];
					for (var i = 0 ; i< uyelerLen;i++){
						tcids.push(uyeler[i].tcid);
					}

					var getlisted = await uyeManager.checkUyeIdsExistAsync(tcids);
					var getlistedLen = getlisted.length;
					tcids =[];
					for (var i =0; i< getlistedLen; i++){
						tcids.push(parseInt(JSON.parse(getlisted[i]).vatandaslik_no));
					}
					var update_eposta = [];
					var update_tel = [];
					for (var i = 0 ; i <uyelerLen; i++){
						var item = uyeler[i];
						item.kayitlimi = tcids.indexOf(item.tcid) !== -1;
						if(item.hasOwnProperty('eposta')){
							update_eposta.push(item);
						}
						if(item.hasOwnProperty('tel')){
							update_tel.push(item);
						}
						uyeler[i] = item;
					}
					var query_email = prepareQueryForEmail(update_eposta);

					var query_tel = prepareQueryForTel(update_tel);

					await RunQueryAsync(query_email);

					await RunQueryAsync(query_tel);

					global.core.returnJson(res,uyeler);
				});
			});
		});

	},
	deleteUye: function (req, res) {
		req.on('end', function () {
			var fields = ['uyeid'];
			global.core.getFormData(req.formData, fields, function (data) {
				uyeManager.delete(data.uyeid, function (result) {    //todo: üye silinemiyor problem var. araştır.
					global.core.redirect(res, '/uye');
				});
			});
		});
	},
	/*CheckSskNo: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.checkSskNo(qs.uyeid, qs.sskno, function (result) {
					res.writeHead(200, setting.TheHeaderJson);
					res.end(JSON.stringify(result));
				});
			});
		});
	},*/
	CheckVatNo: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.checkVatNo(qs.uyeid, qs.vatno, function (result) {
					res.writeHead(200, setting.TheHeaderJson);
					res.end(JSON.stringify(result));
				});
			});
		});
	},
	yeniuye: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			isyeriManager.getAllListWithorgulenmeDurumAndsubeInfo(function (isyeriListItems) {
				var isList = [];
				for (var i = 0; i < isyeriListItems.length; i++) {
					isList.push(JSON.parse(isyeriListItems[i]));
				}
				var obj = {
					user: userData,
					isyeriList: isList
				};
				uyeRenderer.yeniuye(req, res, obj);
			});

		});
	},
    ajaxUyeById:function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
				uyeManager.getById(qs.uyeid, function (result) {
					global.core.returnJson(res, result);
                });
            });
		});
    },
	updateLastMove: function (req, res) {
		global.core.checkLogin(req, res, function (userData, isLogin) {
			global.core.parseReq(req, function (qs) {
				uyeManager.updateLastMove(qs.uyeid, function (result) {
					global.core.returnJson(res, result);
				});
			});
		});
	},
	makeCorrectRecords: function (req, res){
		global.core.checkLogin(req, res, function (userData, isLogin) {
			uyeManager.makeCorrectRecords(function (){
				global.core.returnJson(res, {status:"ok"});
			});
		});
	}

};


function prepareQueryForEmail(items){
	var q = 'BEGIN;\n\r';
	var itemLen = items.length;
	for (var i = 0 ;i< itemLen; i++){
		var item = items[i];
		q +='UPDATE orgut.uye SET email=\''+item.eposta+'\' where vatandaslik_no=\''+item.tcid+'\';\n\r';
	}
	q +='COMMIT;';
	return q;
}

function prepareQueryForTel(items){
	var q = 'BEGIN;\n\r';
	var itemLen = items.length;
	for (var i = 0; i< itemLen;i++){
		var item = items[i];
		q += 'UPDATE orgut.uye SET telefon=\''+item.tel+'\' where vatandaslik_no=\''+item.tcid+'\';\n\r';
	}
	q +='COMMIT;';
	return q;
}

function RunQueryAsync(query){
	return new Promise(function (resolve, reject){
		uyeManager.runQuery(query, function (){
			resolve();
		}, function () {
			reject();
		});
	});
}


module.exports = uyeWork;