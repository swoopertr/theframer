var setting = require('./../setting');
var genelRaporManager = require('./../DataAccess/genelraporlar');
var isyeriManager = require('./../DataAccess/isyeri');
var renderer = require('./../renderer/genelraporlar');


var genelrapor = {
    subeler: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            genelRaporManager.subeler(function (result) {
                theObj = {
                    user: userData,
                    subeList: result
                };
                renderer.subeler(req, res, theObj);
            });
        });
    },
    istatistik: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            genelRaporManager.istatistik(1, function (result) {
                theObj = {
                    user: userData,
                    data: result
                };
                renderer.istatistik(req, res, theObj);
            });
        });
    },
    istatistikAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.istatistik(qs.orgutlenmedurumu, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    genelraporlarisimligiriscikis: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var theObj = {
                user: userData
            };
            renderer.genelraporlarisimligiriscikis(req, res, theObj);
        });
    },
    genelraporlarisimligiriscikisAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarisimligiriscikis(qs.startDate, qs.endDate, qs.harekettipi, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    genelraporlargiriscikis: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var theObj = {
                user: userData
            };
            renderer.genelraporlargiriscikis(req, res, theObj);
        });
    },
    genelraporlargiriscikisAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlargiriscikisObj.genelraporlargiriscikis(qs.startDate, qs.endDate, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    genelraporlarkayitlistesi: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var theObj = {
                user: userData
            };
            renderer.genelraporlarkayitlistesi(req, res, theObj);
        });
    },
    genelraporlarkayitlistesiAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarkayitlistesi(qs.startDate, qs.endDate, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    genelraporlarkayitdefteri: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var theObj = {
                user: userData
            };
            renderer.genelraporlarkayitdefteri(req, res, theObj);
        });
    },
    genelraporlarkayitdefteriAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarkayitdefteri(qs.startDate, qs.endDate, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    genelraporlarorgbaslangic: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var theObj = {
                user: userData
            };
            renderer.genelraporlarorgbaslangic(req, res, theObj);
        });
    },
    genelraporlarorgbaslangicAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var result = {};
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarorgbaslangicListBefores(qs.startDate, function (resultBefore) {
                    genelRaporManager.genelraporlarorgbaslangicListInterval(qs.startDate, qs.endDate, function (resultInterval) {
                        result.interval = resultInterval;
                        result.listBefore = resultBefore;
                        result.listBeforeInt =[];
                        result.finalresultids = [];

                        for (var i  = 0 ; i< result.listBefore.length ; i++){
                            var item = JSON.parse(result.listBefore[i]);
                            result.listBeforeInt.push(item.isyeriid);
                        }


                        for (var j = 0; j < result.interval.length; j++) {
                            var tmpItem = JSON.parse(result.interval[j]);
                            if (result.listBeforeInt.indexOf(tmpItem.isyeriid) == -1){
                                result.finalresultids.push(tmpItem.isyeriid);
                            }
                        }

                        var ids = result.finalresultids.join(',');
                        isyeriManager.getListByIds(ids, qs.startDate, qs.endDate, function (result) {
                            global.core.returnJson(res, result);
                        });

                    });
                });
            });
        });
    },
    genelraporlaruyeliklistesi:function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            var theObj = {
                user:userData
            };
            renderer.genelraporlaruyeliklistesi(req, res, theObj);
        });
    },
    genelraporlaruyeliklistesiAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function(qs){
                genelRaporManager.genelraporlaruyeliklistesi(qs.endDate, qs.orguttype, function(result){
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    genelraporlarsonhareketler: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarsonhareketler(qs.gun, function (result) {
                    var theObj = {
                        user: userData,
                        hareketList: result,
                        gun: qs.gun
                    };
                    renderer.genelraporlarsonhareketler(req, res, theObj);
                });
            });
        });
    },
    genelraporlartilldayCountAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlargiriscikisObj.untillDate(qs.startdate, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    }
};




module.exports = genelrapor;