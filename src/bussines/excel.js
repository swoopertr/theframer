var excelHelper = require('./../Helpers/ExcelHelper');
var setting = require('./../setting');
var isyeriManager = require('./../DataAccess/isyeri');
var subeManager = require('./../DataAccess/sube');
var tespitManager = require('./../DataAccess/tespit');
var sendikaManager = require('./../DataAccess/sendika');
var genelRaporManager = require('./../DataAccess/genelraporlar');

var excel = {
    isyeriuyelistesiexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {

                isyeriManager.getUyebyisyerid(qs.isyeriid, function (excelData) {

                    var excelList = [];
                    for (var i = 0; i < excelData.length; i++) {
                        excelData[i] = JSON.parse(excelData[i]);
                        excelData[i].sonharekettipi = global.dataenums.enums.hareketCinsi[excelData[i].sonharekettipi];
                        excelList.push({
                            isim: excelData[i].isim,
                            soyisim: excelData[i].soyisim,
                            cinsiyet: global.dataenums.enums.kadinerkek[excelData[i].kadinerkek],
                            TC_no: excelData[i].vatandaslik_no,
                            tarih: excelData[i].tarih,
                            kararNo: excelData[i].kararno,
                            dogrulama_kodu: excelData[i].dogrulama_kodu,
                            Telefon: excelData[i].telefon
                        });
                    }
                    var excelFileName = 'isyeriuyelistesi.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', excelList, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });

            });

        });
    },
    isyerigiriscikisexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = qs.isyeriid;
                var startDate = qs.startDate;
                var endDate = qs.endDate;
                isyeriManager.rapor.isyerigiriscikis(startDate, endDate, isyeriid, function (result) {
                    isyeriManager.getone(isyeriid, function (isyeriItem) {
                        var total_kayit = 0;
                        var total_istifa = 0;
                        var total_madde17 = 0;
                        var total_madde25 = 0;
                        var total_emekli = 0;

                        var excelList = [];
                        var firstItem = {
                            'Yıl': startDate,
                            'Ay': '',
                            'İşyeri': '',
                            'Kayıt': '',
                            'İstifa': '',
                            'Madde17': '',
                            'Madde25': '',
                            'Emekli': '',
                            'Kalan': JSON.parse(result.tillStart[0]).count
                        };
                        excelList.push(firstItem);

                        for (var i = 0; i < result.interval.length; i++) {
                            var tmp = JSON.parse(result.interval[i]);
                            var item = {
                                'Yıl': tmp.year,
                                'Ay': tmp.month,
                                'İşyeri': JSON.parse(isyeriItem[0]).isim,
                                'Kayıt': tmp.kayit,
                                'İstifa': tmp.istifa,
                                'Madde17': tmp.madde17,
                                'Madde25': tmp.madde25,
                                'Emekli': tmp.emekli,
                                'Kalan': tmp.kalan
                            };
                            excelList.push(item);
                            total_kayit += parseInt(tmp.kayit);
                            total_istifa += parseInt(tmp.istifa);
                            total_madde17 += parseInt(tmp.madde17);
                            total_madde25 += parseInt(tmp.madde25);
                            total_emekli += parseInt(tmp.emekli);
                        }

                        var lastItem = {
                            'Yıl': endDate,
                            'Ay': '',
                            'İşyeri': '---- Genel Toplam ----',
                            'Kayıt': total_kayit,
                            'İstifa': total_istifa,
                            'Madde17': total_madde17,
                            'Madde25': total_madde25,
                            'Emekli': total_emekli,
                            'Kalan': JSON.parse(result.tillEnd[0]).count
                        };
                        excelList.push(lastItem);
                        var excelFileName = 'isyerigiriscikis.xlsx';
                        excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', excelList, function (err, fileName) {
                            if (err) {
                                global.core.returnJson(res, {
                                    status: "error",
                                    message: err
                                });
                            } else {
                                global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                            }
                        });
                    });
                });
            });
        });
    },
    isyerigiriscikisisimliexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = qs.isyeriid;
                var harekettipi = qs.harekettipi;
                var startDate = qs.startDate;
                var endDate = qs.endDate;
                isyeriManager.rapor.isyeriGirisCikisIsimli(isyeriid, startDate, endDate, harekettipi, function (data) {

                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var tarihArr = item.tarih.split('T')[0].split('-');
                        var tarihFormatText = tarihArr[2] + '/' + tarihArr[1] + '/' + tarihArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Tarih': tarihFormatText,
                            'Ad': item.uyeisim,
                            'Soyad': item.soyisim,
                            'Vat No': item.vatandaslik_no,
                            'Durum': global.dataenums.enums.hareketCinsi[item.hareketcinsi]
                        };
                        result.push(itemTmp);
                    }
                    var excelFileName = 'isyeriisimligiriscikis.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });

                });
            });
        });
    },
    isyeriorgutexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = qs.isyeriid;
                isyeriManager.rapor.isyeriorgut(isyeriid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var itemTmp = {
                            'No': (i + 1),
                            'Vazife': global.dataenums.enums.vazife[item.vazife],
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Durum': global.dataenums.enums.hareketCinsi[item.cins]
                        }
                        result.push(itemTmp);
                    }
                    var excelFileName = 'isyeriorgut.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });

            });
        });

    },
    isyeriuyeexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getUyebyisyerid(qs.isyeriid, function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var tarihArr = item.tarih.split('T')[0].split('-');
                        var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Vatandaslik No': item.vatandaslik_no,
                            'Cinsiyet': global.dataenums.enums.kadinerkek[item.kadinerkek],
                            'Tarih': tarih,
                            'Karar': item.kararno,
                            'Onay No': (item.dogrulama_kodu == null ? '' : item.dogrulama_kodu),
                            'Telefon': item.telefon
                        }
                        result.push(itemTmp);
                    }
                    var excelFileName = 'isyeriuye.xlsx';
                    result.sort(global.core.genricSort('Ad', 'Soyad'));
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    isyeritespitexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = qs.isyeriid;
                var startDate = qs.startDate;
                var endDate = qs.endDate;
                isyeriManager.getone(isyeriid, function (isyeriData) {
                    isyeriManager.rapor.tespit(isyeriid, startDate, endDate, function (data) {
                        var result = [];

                        for (var i = 0; i < data.length; i++) {
                            var item = JSON.parse(data[i]);
                            var tarihArr = item.tarih.split('T')[0].split('-');
                            var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];
                            var basvuruTarihiArr = item.basvurutarihi.split('T')[0].split('-');
                            var basvuruTarihi = basvuruTarihiArr[2] + '.' + basvuruTarihiArr[1] + '.' + basvuruTarihiArr[0];
                            var itemTmp = {
                                'No': (i + 1),
                                'Isyeri': JSON.parse(isyeriData).isim,
                                'Sendika': item.isim,
                                'Tespit Tarihi': tarih,
                                'Basvuru Tarihi': basvuruTarihi,
                                'Calisan Sayisi': item.iscisayisi,
                                'Uye Sayisi': item.uyesayisi,
                                'Evrak No': ((typeof item.evraksayi === 'undefined') ? '' : item.evraksayi)
                            };
                            result.push(itemTmp);
                        }
                        var excelFileName = 'isyeritespit.xlsx';
                        excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                            if (err) {
                                global.core.returnJson(res, {
                                    status: "error",
                                    message: err
                                });
                            } else {
                                global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                            }
                        });
                    });
                });
            });
        });
    },
    isyeritemsilciexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = qs.isyeriid;
                isyeriManager.rapor.isyeriorgut(qs.isyeriid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);

                        var itemTmp = {
                            'No': (i + 1),
                            'Isyeri': item.isyeriisim,
                            'Vazife': ((typeof global.dataenums.enums.vazifeTemsilci[item.vazife] === "undefined") ? '' : global.dataenums.enums.vazifeTemsilci[item.vazife]),
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Vat No': item.vatandaslik_no,
                            'Durum': ((typeof global.dataenums.enums.hareketCinsi[item.cins] === "undefined") ? '' : global.dataenums.enums.hareketCinsi[item.cins])
                        };
                        result.push(itemTmp);
                    }
                    var excelFileName = 'isyeritemsilci.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    isyeriisverentemsilcileriexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = qs.isyeriid;
                isyeriManager.isyeriTemsilcileri(qs.isyeriid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);

                        var itemTmp1 = {
                            'Tanim': 'Temsilci 1',
                            'Aciklama': item.dkisverentems1,
                        };
                        result.push(itemTmp1);

                        var itemTmp2 = {
                            'Tanim': 'Temsilci 2',
                            'Aciklama': item.dkisverentems2,
                        };
                        result.push(itemTmp2);

                        var itemTmp3 = {
                            'Tanim': 'Yedek 1',
                            'Aciklama': item.dkisverentemsydk1,
                        };
                        result.push(itemTmp3);

                        var itemTmp4 = {
                            'Tanim': 'Yedek 2',
                            'Aciklama': item.dkisverentemsydk2,
                        };
                        result.push(itemTmp4);

                    }
                    var excelFileName = 'isyeriisverentemsilci.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subeexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.getList(function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var itemTmp = {
                            'No': (i + 1),
                            'Sube Isım': item.isim,
                            'Telefon': item.telefonno,
                            'Adres': item.adres
                        }
                        result.push(itemTmp);
                    }
                    var excelFileName = 'subeler.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subeisyerleriexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.Isyerleri(qs.subeid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var itemTmp = {
                            'No': (i + 1),
                            'Isyeri': item.isim,
                            'Adres': item.adres,
                            'Durum': global.dataenums.enums.orgutlenmeTipi[item.orgutlenmedurum],
                            'Taseron': (typeof global.dataenums.enums.taseron[item.tasaronvar] === "undefined") ? '' : global.dataenums.enums.taseron[item.tasaronvar],
                            'Calisan Sayisi': ((item.calisansayisi == null) ? '' : item.calisansayisi)
                        }
                        result.push(itemTmp);
                    }
                    var excelFileName = 'subeisyerleri.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subeistatistikexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.Istatistik(qs.subeid, qs.orgutlenmedurumu, function (data) {
                    var result = [];
                    var kadinSayi = 0;
                    var erkekSayi = 0;
                    var toplamSayi = 0;
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var itemTmp = {
                            'No': (i + 1),
                            'Isyeri': item.isyeriisim,
                            'Uye': item.sayi,
                            'Erkek': item.erkek,
                            'Kadin': item.kadin
                        }
                        kadinSayi += parseInt(item.kadin);
                        erkekSayi += parseInt(item.erkek);
                        toplamSayi += parseInt(item.sayi);
                        result.push(itemTmp);
                    }
                    var totalItem = {
                        'No': '',
                        'Isyeri': '',
                        'Uye': toplamSayi,
                        'Erkek': erkekSayi,
                        'Kadin': kadinSayi
                    };
                    result.push(totalItem);

                    var excelFileName = 'subeistatistik.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subeyonetimexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.yonetimEx(qs.subeid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var itemTmp = {
                            'No': (i + 1),
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Isyeri': item.isyeriisim,
                            'Vazife': global.dataenums.enums.vazife[item.vazife],
                            'Vat No': item.vatandaslik_no,
                            'Durum': global.dataenums.enums.hareketCinsi[item.cins]
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'subeyonetim.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subegiriscikisexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.giriscikis(qs.startDate, qs.endDate, qs.subeid, function (data) {
                    var result = [];
                    if (data.tillStart.length > 0) {
                        var tempFirst = {
                            'Yil': qs.startDate,
                            'Ay': '',
                            'Isyeri': '',
                            'Kayit': '',
                            'Istifa': '',
                            'Madde 17': '',
                            'Madde 25': '',
                            'Emekli': '',
                            'Kalan': JSON.parse(data.tillStart[0]).sayi
                        };
                        result.push(tempFirst);
                    }

                    var currentMonth = (data.interval.length > 0) ? JSON.parse(data.interval[0]).month : 1;
                    var totalMonth_kayit = 0;
                    var totalMonth_istifa = 0;
                    var totalMonth_madde17 = 0;
                    var totalMonth_madde25 = 0;
                    var totalMonth_emekli = 0;

                    for (var i = 0; i < data.interval.length; i++) {
                        var item = JSON.parse(data.interval[i]);
                        var itemTmp = {
                            'Yil': item.year,
                            'Ay': item.month,
                            'Isyeri': item.isyeri,
                            'Kayit': item.kayit,
                            'Istifa': item.istifa,
                            'Madde 17': item.madde17,
                            'Madde 25': item.madde25,
                            'Emekli': item.emekli,
                            'Kalan': ''
                        };
                        result.push(itemTmp);
                        if (currentMonth == item.month) {
                            totalMonth_kayit += parseInt(item.kayit);
                            totalMonth_istifa = parseInt(item.istifa);
                            totalMonth_madde17 = parseInt(item.madde17);
                            totalMonth_madde25 = parseInt(item.madde25);
                            totalMonth_emekli = parseInt(item.emekli);
                        } else {
                            var itemTmp = {
                                'Yil': item.year,
                                'Ay': item.month,
                                'Isyeri': '--------- Aylık Toplam -----------',
                                'Kayit': totalMonth_kayit,
                                'Istifa': totalMonth_istifa,
                                'Madde 17': totalMonth_madde17,
                                'Madde 25': totalMonth_madde25,
                                'Emekli': totalMonth_emekli,
                                'Kalan': item.kalan
                            };
                            result.push(itemTmp);
                            currentMonth = item.month;
                            totalMonth_kayit = 0;
                            totalMonth_istifa = 0;
                            totalMonth_madde17 = 0;
                            totalMonth_madde25 = 0;
                            totalMonth_emekli = 0;

                        }
                    }

                    var itemLast = {
                        'Yil': qs.endDate,
                        'Ay': '',
                        'Isyeri': '---- Genel Toplam ----',
                        'Kayit': '',
                        'Istifa': '',
                        'Madde 17': '',
                        'Madde 25': '',
                        'Emekli': '',
                        'Kalan': JSON.parse(data.tillEnd[0]).sayi
                    };
                    result.push(itemLast);

                    var excelFileName = 'subegiriscikis.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subegiriscikisisimliexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.giriscikisisimli(qs.startDate, qs.endDate, qs.subeid, qs.hareketcinsi, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var arrTarih = item.tarih.split('T');
                        var arrDate = arrTarih[0].split('-');
                        var theDate = arrDate[2] + '.' + arrDate[1] + '.' + arrDate[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Tarih': theDate,
                            'Ad': item.uyeisim,
                            'Soyad': item.soyisim,
                            'Vat No': item.vatandaslik_no,
                            'Isyeri': item.isim
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'subegiriscikisisimli.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subetespitexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.tespit(qs.startDate, qs.endDate, qs.subeid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var TarihiArr = item.tarih.split('T');
                        var TarihiDateArr = TarihiArr[0].split('-');
                        var Tarihi = TarihiDateArr[2] + '.' + TarihiDateArr[1] + '.' + TarihiDateArr[0];

                        var BasvuruTarihiArr = item.basvurutarihi.split('T');
                        var BasvuruTarihiDateArr = BasvuruTarihiArr[0].split('-');
                        var BasTarihi = BasvuruTarihiDateArr[2] + '.' + BasvuruTarihiDateArr[1] + '.' + BasvuruTarihiDateArr[0];

                        var itemTmp = {
                            'No': (i + 1),
                            'Isyeri': item.isyeriisim,
                            'Sendika': item.sendikaisim,
                            'Tespit Tarih': Tarihi,
                            'Basvuru Tarih': BasTarihi,
                            'Çalışan Sayısı': item.iscisayisi,
                            'Üye Sayısı': item.uyesayisi,
                            'Evrak No': item.evraksayi
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'subetespit.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subesendikaexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.sendika(qs.subeid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var TarihiArr = item.tarih.split('T');
                        var TarihiDateArr = TarihiArr[0].split('-');
                        var Tarihi = TarihiDateArr[2] + '.' + TarihiDateArr[1] + '.' + TarihiDateArr[0];

                        var itemTmp = {
                            'No': (i + 1),
                            'Isyeri': item.isyeriisim,
                            'Sendika': item.sendikaisim,
                            'Tespit Tarih': Tarihi,
                            'Üye ': item.uyesayisi,
                            'Çalışan Sayısı': item.iscisayisi,
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'subesendika.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subetemsilcilerexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.temsilciler(qs.subeid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var itemTmp = {
                            'Isyeri': item.isyeriisim,
                            'Vazife': ((typeof global.dataenums.enums.vazifeTemsilci[item.vazife] === "undefined") ? '' : global.dataenums.enums.vazifeTemsilci[item.vazife]),
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Vat No': item.vatandaslik_no,
                            'Durum': ((typeof global.dataenums.enums.hareketCinsi[item.cins] === "undefined") ? '' : global.dataenums.enums.hareketCinsi[item.cins]),
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'subetemsilciler.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    subeuyeexcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {

                subeManager.rapor.subeuyelistesi(qs.subeid, function (data) {
                    var result = [];

                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var TarihiArr = item.tarih.split('T');
                        var TarihiDateArr = TarihiArr[0].split('-');
                        var Tarihi = TarihiDateArr[2] + '.' + TarihiDateArr[1] + '.' + TarihiDateArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Vat No': item.vatandaslik_no,
                            'Cinsiyet': (item.kadinerkek == 1) ? 'Erkek' : 'Kadın',
                            'Tarih': Tarihi,
                            'Karar': item.kararno,
                            'Onay No': item.dogrulama_kodu,
                            'Isyeri': item.isyeriisim,
                            'Orgutdurum': global.dataenums.enums.orgutlenmeTipi[item.orgutlenmedurum],
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'subeuyeler.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    tespitExel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                tespitManager.getAll(function (tespitItems) {
                    sendikaManager.getAll(function (sendikaList) {
                        var result = [];
                        var sendikaObj = {};
                        for (var i = 0; i < sendikaList.length; i++) {
                            var item = JSON.parse(sendikaList[i]);
                            sendikaObj[item.sendikaid] = item.isim;
                        }

                        for (var i = 0; i < tespitItems.length; i++) {
                            var item = JSON.parse(tespitItems[i]);
                            var bTarihArr = item.basvurutarihi.split('T')[0].split('-');
                            var bTarih = bTarihArr[2] + '.' + bTarihArr[1] + '.' + bTarihArr[0];
                            var tarihArr = item.tarih.split('T')[0].split('-');
                            var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];

                            var itemTmp = {
                                'No': (i + 1),
                                'Sendika Isim': sendikaObj[item.sendikaid],
                                'Isyeri Isim': item.isim,
                                'Basv. Tarihi': bTarih,
                                'Isci Sayisi': item.iscisayisi,
                                'Uye Sayisi': item.uyesayisi,
                                'Tarih': tarih,
                                'Evrak': item.evraksayi
                            };

                            if (qs.sendikaid == '0' && qs.isyeritxt == '') {
                                result.push(itemTmp);
                            } else if (qs.sendikaid == '0' && qs.isyeritxt != '') {
                                if (item.isim.startsWith(qs.isyeritxt)) {
                                    result.push(itemTmp);
                                }
                            } else if (qs.sendikaid != '0' && qs.isyeritxt == '') {
                                if (item.sendikaid == qs.sendikaid) {
                                    result.push(itemTmp);
                                }
                            } else if (qs.sendikaid != '0' && qs.isyeritxt != '') {
                                if (item.sendikaid == qs.sendikaid && item.isim.startsWith(qs.isyeritxt)) {
                                    result.push(itemTmp);
                                }
                            }

                        }

                        var excelFileName = 'tespitler.xlsx';
                        excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                            if (err) {
                                global.core.returnJson(res, {
                                    status: "error",
                                    message: err
                                });
                            } else {
                                global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                            }
                        });

                    });
                });

            });
        });
    },
    genelraporlarsubeExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.subeler(function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);

                        var itemTmp = {
                            'No': (i + 1),
                            'Sube Isim': item.isim,
                            'Telefon': item.telefonno,
                            'Adres': item.adres
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'genelraporlarsube.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                            return;
                        }
                    });
                });
            });
        });
    },
    genelraporlaristatistikExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.istatistik(qs.durum, function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);

                        var itemTmp = {
                            'No': (i + 1),
                            'Sube Isim': item.isim,
                            'Uye': item.calisan,
                            'Erkek': item.erkek,
                            'Kadin': item.kadin
                        };
                        result.push(itemTmp);
                    }

                    var excelFileName = 'genelraporlaristatistik.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    genelraporlarisimligiriscikisExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarisimligiriscikis(qs.startDate, qs.endDate, qs.harekettipi, function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var tarihArr = item.tarih.split('T')[0].split('-');
                        var tarihFormatText = tarihArr[2] + '/' + tarihArr[1] + '/' + tarihArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Tarih': tarihFormatText,
                            'Ad': item.uyeisim,
                            'Soyad': item.soyisim,
                            'Vat No': item.vatandaslik_no,
                            'Isyeri': item.isyeriisim,
                            'Sube': item.subeisim
                        };
                        result.push(itemTmp);
                    }
                    var excelFileName = 'genelraporlarisimligiriscikis.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    genelraporlargiriscikisExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlargiriscikisObj.genelraporlargiriscikis(qs.startDate, qs.endDate, async function (data) {
                    let result = [];
                    let monthYear = "";
                    if (data.interval.length > 0) {
                        let item = JSON.parse(data.interval[0]);
                        monthYear = item.year + '_' + item.month;
                    }

                    let itemTmp = {
                        'Yil': qs.startDate,
                        'Ay': '',
                        'Sube': '',
                        'Kayit': '',
                        'Istifa': '',
                        'Madde 17': '',
                        'Madde 25': '',
                        'Emekli': '',
                        'Kalan': JSON.parse(data.tillStart[0]).count
                    };
                    result.push(itemTmp);

                    let month = 0;
                    let year = 0;
                    let total_kayit = 0;
                    let total_istifa = 0;
                    let total_madde17 = 0;
                    let total_madde25 = 0;
                    let total_emekli = 0;
                    let monthly_kayit = 0;
                    let monthly_istifa = 0;
                    let monthly_madde17 = 0;
                    let monthly_madde25 = 0;
                    let monthly_emekli = 0;

                    for (let i = 0; i < data.interval.length; i++) {
                        let item = JSON.parse(data.interval[i]);
                        month = item.month;
                        year = item.year;
                        
                        if(`${item.year}_${item.month}`!= monthYear ){
                            monthYear = item.year + '_' + item.month;
                            var lastDayDateOfMonth = global.core.daysInMonth(item.year, item.month - 1);
                            var strMonth = item.month < 10 ? "0"+item.month:item.month;
        
                            var strLastDayDate =  item.year+"-"+strMonth+"-"+ lastDayDateOfMonth;
                            var returnVal = await getValueFromDb(strLastDayDate);
                            let itemTmp = {
                                'Yil': item.year,
                                'Ay': item.month,
                                'Sube': '---------Aylık Toplam-----------',
                                'Kayit': monthly_kayit,
                                'Istifa': monthly_istifa,
                                'Madde 17': monthly_madde17,
                                'Madde 25': monthly_madde25,
                                'Emekli': monthly_emekli,
                                'Kalan': returnVal
                            };
                            result.push(itemTmp);
                            monthly_kayit = 0;
                            monthly_istifa = 0;
                            monthly_madde17 = 0;
                            monthly_madde25 = 0;
                            monthly_emekli = 0;
                        }

                        let itemTmp = {
                            'Yil': item.year,
                            'Ay': item.month,
                            'Sube': item.isim,
                            'Kayit': item.kayit,
                            'Istifa': item.istifa,
                            'Madde 17': item.madde17,
                            'Madde 25': item.madde25,
                            'Emekli': item.emekli,
                            'Kalan': null
                        };
                        result.push(itemTmp);

                        total_kayit += parseInt(item.kayit);
                        total_istifa += parseInt(item.istifa);
                        total_madde17 += parseInt(item.madde17);
                        total_madde25 += parseInt(item.madde25);
                        total_emekli += parseInt(item.emekli);
                        monthly_kayit += parseInt(item.kayit);
                        monthly_istifa += parseInt(item.istifa);
                        monthly_madde17 += parseInt(item.madde17);
                        monthly_madde25 += parseInt(item.madde25);
                        monthly_emekli += parseInt(item.emekli);

                       
                    

                    }

/*
                    for (var i = 0; i < data.interval.length; i++) {
                        var item = JSON.parse(data.interval[i]);
                        month = item.month;
                        year = item.year;
                        var itemTmp = {
                            'Yil': item.year,
                            'Ay': item.month,
                            'Sube': item.isim,
                            'Kayit': item.kayit,
                            'Istifa': item.istifa,
                            'Madde 17': item.madde17,
                            'Madde 25': item.madde25,
                            'Emekli': item.emekli,
                            'Kalan': ''
                        };
                        result.push(itemTmp);

                        if (item.year + '_' + item.month == monthYear) {
                            total_kayit += parseInt(item.kayit);
                            total_istifa += parseInt(item.istifa);
                            total_madde17 += parseInt(item.madde17);
                            total_madde25 += parseInt(item.madde25);
                            total_emekli += parseInt(item.emekli);
                            monthly_kayit += parseInt(item.kayit);
                            monthly_istifa += parseInt(item.istifa);
                            monthly_madde17 += parseInt(item.madde17);
                            monthly_madde25 += parseInt(item.madde25);
                            monthly_emekli += parseInt(item.emekli);
                        } else {
                            var prevItem = JSON.parse(data.interval[i - 1]);
                            monthYear = item.year + '_' + item.month;
                            var lastDayDateOfMonth = global.core.daysInMonth(prevItem.year, prevItem.month - 1);
                            var strLastDayDate = lastDayDateOfMonth + '.' + prevItem.month.padStart(2,'0') + '.' + prevItem.year;

                            var returnVal = await getValueFromDb(strLastDayDate);
                            var itemTmp = {
                                'Yil': item.year,
                                'Ay': item.month,
                                'Sube': '---------Aylık Toplam-----------',
                                'Kayit': monthly_kayit,
                                'Istifa': monthly_istifa,
                                'Madde 17': monthly_madde17,
                                'Madde 25': monthly_madde25,
                                'Emekli': monthly_emekli,
                                'Kalan': returnVal
                            };
                            result.push(itemTmp);
                            monthly_kayit = 0;
                            monthly_istifa = 0;
                            monthly_madde17 = 0;
                            monthly_madde25 = 0;
                            monthly_emekli = 0;

                        }

                        if (i === data.interval.length - 1) {
                            monthYear = item.year + '_' + item.month;
                            var lastDayDateOfMonth = global.core.daysInMonth(item.year, item.month - 1);
                            var strLastDayDate = lastDayDateOfMonth + '.' + item.month + '.' + item.year;

                            var returnVal = await getValueFromDb(strLastDayDate);

                            var itemTmp = {
                                'Yil': item.year,
                                'Ay': item.month,
                                'Sube': '---------Aylık Toplam-----------',
                                'Kayit': monthly_kayit,
                                'Istifa': monthly_istifa,
                                'Madde 17': monthly_madde17,
                                'Madde 25': monthly_madde25,
                                'Emekli': monthly_emekli,
                                'Kalan': returnVal
                            };
                            result.push(itemTmp);
                            monthly_kayit = 0;
                            monthly_istifa = 0;
                            monthly_madde17 = 0;
                            monthly_madde25 = 0;
                            monthly_emekli = 0;
                        }
                    }
*/
                    itemTmp = {
                        'Yil': qs.startDate,
                        'Ay': '',
                        'Sube': '---- Genel Toplam ----',
                        'Kayit': total_kayit,
                        'Istifa': total_istifa,
                        'Madde 17': total_madde17,
                        'Madde 25': total_madde25,
                        'Emekli': total_emekli,
                        'Kalan': JSON.parse(data.tillEnd[0]).count
                    };
                    result.push(itemTmp);

                    var excelFileName = 'genelraporlargiriscikis.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    genekraporlarkayitlistesiExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarkayitlistesi(qs.startDate, qs.endDate, function (data) {

                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var tarihArr = item.tarih.split('T')[0].split('-');
                        var tarihFormatText = tarihArr[2] + '/' + tarihArr[1] + '/' + tarihArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Tc No': item.vatandaslik_no,
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Cinsiyet': ((item.kadinerkek == 1) ? 'Erkek' : 'Kadın'),
                            'Telefon': item.telefon,
                            'Karar_No': item.kararno,
                            'Tarih': tarihFormatText,
                            'Isyeri': item.isyeriisim,
                            'BCM Dosya No': item.bcmdosyano,
                            'Sube': item.subeisim
                        };
                        result.push(itemTmp);
                    }
                    var excelFileName = 'genelraporlarkayitlistesi.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    genekraporlarorgbaslangicExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {

                genelRaporManager.genelraporlarorgbaslangicListBefores(qs.startDate, function (resultBefore) {
                    genelRaporManager.genelraporlarorgbaslangicListInterval(qs.startDate, qs.endDate, function (resultInterval) {
                        var result = {};
                        result.interval = resultInterval;
                        result.listBefore = resultBefore;
                        result.listBeforeInt = [];
                        result.finalresultids = [];

                        for (var i = 0; i < result.listBefore.length; i++) {
                            var item = JSON.parse(result.listBefore[i]);
                            result.listBeforeInt.push(item.isyeriid);
                        }


                        for (var j = 0; j < result.interval.length; j++) {
                            var tmpItem = JSON.parse(result.interval[j]);
                            if (result.listBeforeInt.indexOf(tmpItem.isyeriid) == -1) {
                                result.finalresultids.push(tmpItem.isyeriid);
                            }
                        }

                        var ids = result.finalresultids.join(',');
                        isyeriManager.getListByIds(ids, qs.startDate, qs.endDate, function (data) {
                            var Lresult = [];
                            for (var i = 0; i < data.length; i++) {
                                var item = JSON.parse(data[i]);
                                var tarihArr = item.tarih.split('T')[0].split('-');
                                var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];
                                var itemTmp = {
                                    'No': (i + 1),
                                    'Tarih': tarih,
                                    'Isyeri': item.isim,
                                    'Sube': item.subeisim
                                };
                                Lresult.push(itemTmp);
                            }
                            var excelFileName = 'genelraporlarorgbaslangic.xlsx';
                            excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                                if (err) {
                                    global.core.returnJson(res, {
                                        status: "error",
                                        message: err
                                    });
                                } else {
                                    global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                                }
                            });
                        });

                    });
                });


            });
        });
    },
    genekraporlaruyelistesiExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlaruyeliklistesi(qs.endDate, qs.orgutlenmedurum, function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var tarihArr = item.tarih.split('T')[0].split('-');
                        var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Ad': item.calisanisim,
                            'Soyad': item.soyisim,
                            'Telefon': item.telefon,
                            'Vat No': item.vatandaslik_no,
                            'Onay Kodu': ((item.dogrulama_kodu == null) ? '' : item.dogrulama_kodu),
                            'Tarih': tarih,
                            'Isyeri': item.isyeriisim,
                            'Sube': item.subeisim
                        };
                        result.push(itemTmp);
                    }
                    var excelFileName = 'genelraporlaruyelistesi.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    genekraporlarsonhareketlerExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                genelRaporManager.genelraporlarsonhareketler(qs.gun, function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var tarihArr = item.tarih.split('T')[0].split('-');
                        var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Tarih': tarih,
                            'Ad': item.isim,
                            'Soyad': item.soyisim,
                            'Vatandaslik No': item.vatandaslik_no,
                            'Isyeri': item.isyeriisim,
                            'BCM No': item.bcmdosyano,
                            'Sube': item.subeisim,
                            'Hareket Tipi': global.dataenums.enums.hareketCinsi[item.cins]
                        };
                        result.push(itemTmp);
                    }
                    var excelFileName = 'genelraporlarsonhareketlistesi.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });
            });
        });
    },
    sendikatespitExcel: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var startDateArr = qs.startDate.split('.');
                var start = startDateArr[1] + '.' + startDateArr[0] + '.' + startDateArr[2];

                var endDateArr = qs.endDate.split('.');
                var end = endDateArr[1] + '.' + endDateArr[0] + '.' + endDateArr[2];

                var itemObj = {
                    sendikaid: qs.sendikaid,
                    start: start,
                    end: end
                };

                sendikaManager.rapor.tespit(itemObj, function (data) {
                    var result = [];
                    for (var i = 0; i < data.length; i++) {
                        var item = JSON.parse(data[i]);
                        var bTarihArr = item.basvurutarihi.split('T')[0].split('-');
                        var bTarih = bTarihArr[2] + '.' + bTarihArr[1] + '.' + bTarihArr[0];
                        var tarihArr = item.tarih.split('T')[0].split('-');
                        var tarih = tarihArr[2] + '.' + tarihArr[1] + '.' + tarihArr[0];
                        var itemTmp = {
                            'No': (i + 1),
                            'Sendika': item.sendikaisim,
                            'Isyeri': item.isyeriisim,
                            'Basvuru Tarihi': bTarih,
                            'Calisan': item.iscisayisi,
                            'Uye': item.uyesayisi,
                            'Tespit Tarihi': tarih,
                            'Evrak': item.evraksayi
                        };
                        result.push(itemTmp);
                    }
                    var excelFileName = 'sendikatespit.xlsx';
                    excelHelper.export.GenerateExcelFile(excelFileName, 'Sheet1', result, function (err, fileName) {
                        if (err) {
                            global.core.returnJson(res, {
                                status: "error",
                                message: err
                            });
                        } else {
                            global.core.returnFile(setting.virtualExcelFolder + excelFileName, res);
                        }
                    });
                });

            });
        });
    }
};


function getValueFromDb(strLastDayDate) {
    return new Promise(function (resolve, reject) {
        genelRaporManager.genelraporlargiriscikisObj.untillDate(strLastDayDate, function (lastdateResult) {
            var result = JSON.parse(lastdateResult[0]).count;
            resolve(result);
        });
    });
}

module.exports = excel;