var sendikaManager = require('./../DataAccess/sendika');
var renderer = require('./../renderer/sendika');

var sendika = {
    sendikalar: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            sendikaManager.getAll(function (result) {
                theObj = {
                    user: userData,
                    sendikalar: result
                };
                renderer.sendikalar(req, res, theObj);
            });
        });
    },
    sendikadetay: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                sendikaManager.getone(qs.sendikaid, function (result) {

                    var theobj = {
                        user: userData,
                        sendika: result
                    };
                    renderer.sendikadetay(req, res, theobj);

                });


            });
        });
    },
    postsendikadetay: function (req, res) {
        req.on('end', function () {
            var fields = ['sendikaid', 'isim', 'adres', 'dosyano', 'telefonno'];
            global.core.getFormData(req.formData, fields, function (data) {
                if (data.sendikaid == "0") {
                    sendikaManager.save(data, function (result) {
                        global.console.log('sendika inserted redirect...');
                        global.core.redirect(res, 'sendika');
                    });
                } else {
                    sendikaManager.update(data, function (result) {
                        global.console.log('sendika updated redirect...');
                        global.core.redirect(res, 'sendika');
                    });
                }

            });
        });
    },
    sendikatespit: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            sendikaManager.getAll(function (result) {
                var theobj = {
                    user: userData,
                    sendikaList: result
                };
                renderer.sendikatespit(req, res, theobj);
            });
        });
    },
    sendikatespitajax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var startDateArr = qs.startdate.split('.');
                var start = startDateArr[1]+'.'+startDateArr[0]+'.'+startDateArr[2];

                var endDateArr =qs.enddate.split('.');
                var end = endDateArr[1]+'.'+endDateArr[0]+'.'+endDateArr[2];

                var item = {
                    sendikaid: qs.sendikaid,
                    start: start,
                    end: end
                };
                sendikaManager.rapor.tespit(item, function (list) {
                    global.core.returnJson(res, JSON.stringify(list));
                });
            });
        });
    },
    sendikaisyeriCountAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                sendikaManager.rapor.isyeriSayisi(qs.sendikaid, function (result) {
                   global.core.returnJson(res, result);
                });
            });
        });
    },
    sendikaDeleteAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                sendikaManager.delete(qs.sendikaid, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    }
};

module.exports = sendika;