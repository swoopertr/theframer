var setting = require('./../setting');
var subeManager = require('./../DataAccess/sube');
var renderer = require('./../renderer/sube');

var workobj = {
    check: function (req, res, callback) {
        var userData = req.sess.get('user');
        var isLogin = req.sess.get('isLogin');
        if (typeof userData === "undefined") {
            global.core.redirect(res, '/login');
        } else {
            if (isLogin === true) {
                if (typeof callback !== "undefined") {
                    callback(userData, isLogin);
                }
            } else {
                global.core.redirect(res, '/login');
            }
        }
    },
    subeListesi: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            subeManager.getList(function (result) {
                var dataObj = {
                    subelist: result,
                    user: userData
                };
                renderer.subeler(req, res, dataObj);
            });
        });
    },
    subeDetay: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.getOne(qs.detaysube, function (result) {
                    var subeItem = JSON.parse(result[0]);
                    subeManager.rapor.yonetim(subeItem.subeid, function (resultYonetim) {
                        var yonetimArr = [];
                        for (var i = 0; i < resultYonetim.length; i++) {
                            yonetimArr.push(JSON.parse(resultYonetim[i]));
                        }
                        var theObj = {user: userData, sube: subeItem, yonetim: yonetimArr};
                        renderer.subeDetay(req, res, theObj);
                    });

                });
            });
        });
    },
    subeEdit: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.getOne(qs.editsube, function (result) {
                    var subeItem = {};
                    if (result.length == 0) {
                        subeItem.subeid = 0;
                        subeItem.isim = '';
                        subeItem.adres = '';
                        subeItem.telefonno = '';
                        subeItem.detay = '';
                    } else {
                        subeItem = JSON.parse(result[0]);
                    }

                    var theObj = {user: userData, sube: subeItem};
                    renderer.subeEdit(req, res, theObj);
                })
            });
        });
    },
    isyeri: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.isyerleri, function (subeResult) {
                    subeManager.rapor.Isyerleri(params.isyerleri, function (isyeriResult) {//params.isyerleri acually means subeid
                        var dataObj = {
                            isyeriList: isyeriResult,
                            user: userData,
                            sube: subeResult
                        };
                        renderer.isyeri(req, res, dataObj);
                    });
                });
            });
        });
    },
    istatistik: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {
                    subeManager.rapor.Istatistik(params.subeid, 1, function (isyeriIstatistik) {
                        var dataObj = {
                            isyeriIstatistik: isyeriIstatistik,
                            user: userData,
                            sube: subeResult
                        };
                        renderer.istatistik(req, res, dataObj);
                    });
                });
            });
        });
    },
    requestAjax: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.Istatistik(qs.subeid, qs.orgutlenmedurumu, function (result) {
                    var resultItem = [];
                    for (var i = 0; i < result.length; i++) {
                        resultItem.push(JSON.parse(result[i]));
                    }
                    res.writeHead(200, setting.TheHeaderJson);
                    res.end(JSON.stringify(resultItem));
                });
            });
        });
    },
    subeyonetim: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {
                    subeManager.rapor.yonetimEx(params.subeid, function (yonetimList) {
                        var dataObj = {
                            yonetimList: yonetimList,
                            user: userData,
                            sube: subeResult
                        };
                        renderer.yonetimEx(req, res, dataObj);
                    });
                });
            });
        });
    },
    subeyonetimedit: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.rapor.subeuyelistesi(params.subeid, function (subeUyeleri) {
                    subeManager.getTeskilatById(params.subeteskilatiid, function (teskilat) {
                        subeManager.getOne(params.subeid, function (subeData) {
                            var dataObj = {
                                subeteskilat: teskilat,
                                subeCalisanList: subeUyeleri,
                                sube: subeData,
                                user: userData,

                            };
                            renderer.subeyonetimedit(req, res, dataObj);
                        });
                    });

                });
            });

        });
    },
    subegiriscikis: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {

                    var dataObj = {
                        user: userData,
                        sube: JSON.parse(subeResult)
                    };
                    renderer.giriscikis(req, res, dataObj);
                });
            });
        });
    },
    subeAjaxSubegiriscikis: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.giriscikis(qs.startDate, qs.endDate, qs.subeid, function (result) {
                    res.writeHead(200, setting.TheHeaderJson);
                    res.end(JSON.stringify(result));
                });
            });
        });
    },
    giriscikisisimli: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {
                    var dataObj = {
                        user: userData,
                        sube: JSON.parse(subeResult)
                    };
                    renderer.giriscikisisimli(req, res, dataObj);
                });
            });
        });
    },
    subeAjaxSubegiriscikisisimli: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.giriscikisisimli(qs.startDate, qs.endDate, qs.subeid, qs.hareketcinsi, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    subetespit: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {
                    var dataObj = {
                        user: userData,
                        sube: JSON.parse(subeResult)
                    };
                    renderer.subetespit(req, res, dataObj);
                });
            });
        });
    },
    subeAjaxtespit: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                subeManager.rapor.tespit(qs.startDate, qs.endDate, qs.subeid, function (result) {
                    res.writeHead(200, setting.TheHeaderJson);
                    res.end(JSON.stringify(result));
                });
            });
        });
    },
    subesendika: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {
                    subeManager.rapor.sendika(JSON.parse(subeResult[0]).subeid, function (result) {
                        var dataObj = {
                            liste: result,
                            user: userData,
                            sube: JSON.parse(subeResult)
                        };
                        renderer.subesendika(req, res, dataObj);
                    });
                });
            });
        });
    },
    subetemsilciler: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {
                    subeManager.rapor.temsilciler(JSON.parse(subeResult[0]).subeid, function (result) {
                        var dataObj = {
                            liste: result,
                            user: userData,
                            sube: JSON.parse(subeResult)
                        };
                        renderer.temsilciler(req, res, dataObj);
                    });
                });
            });
        });
    },
    subeuyelistesi: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.getOne(params.subeid, function (subeResult) {
                    var dataObj = {
                        user: userData,
                        sube: JSON.parse(subeResult)
                    };
                    renderer.subeuyelistesi(req, res, dataObj);
                });
            });
        });
    },
    ajaxsubeuyelistesi: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.rapor.subeuyelistesi(params.subeid, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    ajaxdeletesube: function (req, res) {
        this.check(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (params) {
                subeManager.deletebyid(params.subeid, function (result) {
                    global.core.returnJson(res, !result.hasOwnProperty('err'));
                });
            });
        })
    }

};
module.exports = workobj;