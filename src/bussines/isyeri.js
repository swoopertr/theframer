var setting = require('./../setting');
var isyeriManager = require('./../DataAccess/isyeri');
var subeManager = require('./../DataAccess/sube');
var temsilciManager = require('./../DataAccess/temsilci');
var tisManager = require('./../DataAccess/topluissozlesmesi');
var uyeManager = require('./../DataAccess/uye');
var renderer = require('./../renderer/isyeri');

var isyeri = {
    isyeriAllList: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var pageIndex = (qs.page === undefined) ? 1 : qs.page;
                var isim = (qs.isim === undefined) ? '' : qs.isim;
                var orgutDurum = (qs.orgutdurum === undefined) ? '' : qs.orgutdurum;
                var tasaron = (qs.tasaron === undefined) ? '' : qs.tasaron;
                var subeid = (qs.subeid === undefined) ? '' : qs.subeid;
                var itemCount = setting.pagingCount;
                isyeriManager.getListForScreen(isim, orgutDurum, tasaron, subeid, pageIndex, itemCount, function (result) {
                    subeManager.getList(function (subeler) {
                        var theobj = {
                            user: userData,
                            isyerleri: result,
                            subeList: subeler
                        };
                        renderer.isyerleri(req, res, theobj);
                    });
                });
            });
        });
    },
    isyeriAjaxList: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var pageIndex = (qs.page === undefined) ? 1 : qs.page;
                var isim = (qs.isim === undefined) ? '' : qs.isim;
                var orgutDurum = (qs.orgut === "0") ? '' : qs.orgut;
                var tasaron = (qs.tasaron === "0") ? '' : qs.tasaron;
                var subeid = (qs.subeid === "0") ? '' : qs.subeid;
                var itemCount = setting.pagingCount;
                isyeriManager.getListForScreen(isim, orgutDurum, tasaron, subeid, pageIndex, itemCount, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    isyeriAjaxListCount: function(req, res){
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var pageIndex = (qs.page === undefined) ? 1 : qs.page;
                var isim = (qs.isim === undefined) ? '' : qs.isim;
                var orgutDurum = (qs.orgut === "0") ? '' : qs.orgut;
                var tasaron = (qs.tasaron === "0") ? '' : qs.tasaron;
                var subeid = (qs.subeid === "0") ? '' : qs.subeid;

                isyeriManager.getListCountForScreen(isim, orgutDurum, tasaron, subeid, pageIndex, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    isyeriAjaxTotalCount: function (req, res) {
        //global.core.checkLogin(req, res, function (userData, isLogin) {
        isyeriManager.getTotalCount(function (result) {
            if (req.isCached == true) {
                global.cache.set(req.url, result);
            }
            global.core.returnJson(res, result);
        });
        //});
    },
    isyeriAjaxTotalPagingCount: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            isyeriManager.getTotalCount(function (result) {
                var pagingItemCount = setting.pagingCount;
                var totalCount = JSON.parse(result).count;
                var pageCount = Math.floor(totalCount / pagingItemCount);
                var resultReturn = {result: pageCount};
                res.end(JSON.stringify(resultReturn));
            });
        })
    },
    isyeriAjaxPagingData: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getAllPagingData(qs.page, setting.pagingCount, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    isyeriEdit: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (result) {
                    tisManager.getListByIsyeriId(qs.isyeriid, function (tisData) {
                        if (typeof result[0] != 'undefined') {
                            var isyeriItem = JSON.parse(result[0]);
                            var theObj = {
                                user: userData,
                                isyeri: isyeriItem,
                                tisData: tisData
                            };
                        } else {
                            var isyeriItem = {isyeriid: "0"};
                            var theObj = {
                                user: userData,
                                isyeri: isyeriItem,
                                tisData: []
                            };
                        }
                        renderer.isyeriEdit(req, res, theObj);
                    });
                });
            });
        });
    },
    isyeriDetay: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (result) {
                    isyeriManager.isyeriistatistikByisyeriid(qs.isyeriid, function (istatistik) {
                        tisManager.getListByIsyeriId(qs.isyeriid, function (tisData) {
                            uyeManager.getUyelerByIsyeriid(qs.isyeriid, function (uyeler) {
                                var isyeriItem = JSON.parse(result[0]);
                                var theObj = {
                                    user: userData,
                                    isyeri: isyeriItem,
                                    uyeler: uyeler,
                                    istatistik: istatistik,
                                    tisData: tisData
                                };
                                renderer.isyeriDetay(req, res, theObj);
                            });
                        });
                    });
                });
            });
        });
    },
    isyeriByIdAjax: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (result) {
                    global.core.returnJson(res, result[0]);
                });
            });
        });

    },
    istatistik: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            //todo:istatistik
        });
    },
    uyelistesi: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = parseInt(qs.isyeriid);
                isyeriManager.getUyebyisyerid(isyeriid, function (result) {
                    isyeriManager.getone(isyeriid, function (isyeri) {
                        var theObj = {
                            user: userData,
                            isyeri: isyeri,
                            uyeList: result
                        };
                        renderer.uyelistesi(req, res, theObj);
                    });
                });
            });
        });
    },
    isyeritespit: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (isyeri) {
                    var theObj = {
                        user: userData,
                        isyeri: isyeri
                    };
                    renderer.isyeriTespit(req, res, theObj);
                });
            });
        });
    },
    isyeriAjaxtespit: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.rapor.tespit(qs.isyeriid, qs.startDate, qs.endDate, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    isyeritemsilciEdit: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (isyeri) {
                    isyeriManager.getUyebyisyerid(qs.isyeriid, function (result) {
                        var theObj = {
                            calisanlar: result,
                            isyeri: isyeri,
                            user: userData
                        };
                        if (qs.hasOwnProperty('temsilciId')) {
                            temsilciManager.getOne(qs.temsilciId, function (result) {
                                theObj.temsilci = result;
                                renderer.isyeritemsilciEdit(req, res, theObj);

                            });
                        } else {
                            renderer.isyeritemsilciEdit(req, res, theObj);

                        }
                    });
                });

            });
        });
    },
    isyeriorgut: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (isyeri) {
                    isyeriManager.rapor.isyeriorgut(qs.isyeriid, function (result) {
                        var theObj = {
                            user: userData,
                            isyeri: isyeri,
                            orgutData: result
                        };
                        renderer.isyeriorgut(req, res, theObj);
                    });
                });

            });

        });
    },
    isyeritemsilci: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (isyeri) {
                    isyeriManager.rapor.isyeriorgut(qs.isyeriid, function (result) {
                        var theObj = {
                            user: userData,
                            isyeri: isyeri,
                            orgutData: result
                        };
                        renderer.isyeritemsilci(req, res, theObj);
                    });

                });
            });
        });
    },
    isyerigiriscikis: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (result) {
                    var isyeriItem = JSON.parse(result[0]);
                    var theObj = {
                        user: userData,
                        isyeri: isyeriItem
                    };
                    renderer.isyeriGirisCikis(req, res, theObj);
                });
            });
        });
    },
    isyeriAjaxisyerigiriscikis: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.rapor.isyerigiriscikis(qs.startDate, qs.endDate, qs.isyeriid, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    isyerigiriscikisisimli: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.getone(qs.isyeriid, function (result) {
                    var isyeriItem = JSON.parse(result[0]);
                    var theObj = {
                        user: userData,
                        isyeri: isyeriItem
                    };
                    renderer.isyerigiriscikisisimli(req, res, theObj);
                });
            });
        });
    },
    isyeriAjaxgirisCikisIsimli: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.rapor.isyeriGirisCikisIsimli(qs.isyeriid, qs.startDate, qs.endDate, qs.harekettipi, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    },
    isyeriisverentemsilcileri: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                isyeriManager.isyeriTemsilcileri(qs.isyeriid, function (temsilciler) {
                    isyeriManager.getone(qs.isyeriid, function (isyeriItem) {
                        var theObj = {
                            user: userData,
                            isyeri: JSON.parse(isyeriItem[0]),
                            temsilciler: JSON.parse(temsilciler[0])
                        };
                        renderer.isyeriisverentemsilcileri(req, res, theObj);
                    });
                });
            });
        });
    },
    isyeriAjaxDelete: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var isyeriid = qs.isyeriid;
                isyeriManager.tespitCevapNullerbyIsyeriid(isyeriid, function (nuller) {
                    isyeriManager.deleteByid(isyeriid, function (result) {
                        global.core.returnJson(res, !result.hasOwnProperty('err'));
                    });
                });
            });
        });
    },
    checkBcmDosyanoForIsyeri: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                console.log(qs);
                isyeriManager.checkBcmDosyanoForIsyeri(qs.bcmdosyano, function (result) {
                    global.core.returnJson(res, result);
                });
            });
        });
    }
};

module.exports = isyeri;