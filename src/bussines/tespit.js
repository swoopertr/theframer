var tespitManager = require('./../DataAccess/tespit');
var sendikaManager = require('./../DataAccess/sendika')
var isyeriManager = require('./../DataAccess/isyeri');
var subeManager = require('./../DataAccess/sube');
var renderer = require('./../renderer/tespit');


var tespit = {
    tespitler: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            tespitManager.getAll(function (result) {
                sendikaManager.getAll(function (sendikaList) {
                    var theobj = {
                        user: userData,
                        tespitler: result,
                        sendikaList: sendikaList
                    };
                    renderer.tespitler(req, res, theobj);
                });
            });
        });

    },
    tespitdetay: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                tespitManager.getone(qs.tespitid, function (tespitResult) {
                    sendikaManager.getAll(function (sendikaList) {
                        isyeriManager.getlist(function (isyeriList) {
                            subeManager.getList(function (subeList) {
                                if (tespitResult.length == 0) {
                                    tespitResult.tespitcevapid = 0;
                                }
                                var theobj = {
                                    user: userData,
                                    tespit: tespitResult,
                                    sendikaList: sendikaList,
                                    isyeriList: isyeriList,
                                    subeList:subeList
                                };
                                renderer.tespitdetay(req, res, theobj);
                            });
                        });
                    });
                });
            });
        });
    },
    tespitpost: function (req, res) {
        req.on('end', function () {
            var fields = ['isyeriid', 'sendikaselect', 'basvuruTarihi', 'calisansayisi',
                'uyesayisi', 'evraktarihi', 'evraksayisi', 'tespitid'];

            global.core.getFormData(req.formData, fields, function (data) {
                var nowDate = new Date();
                var year = nowDate.getFullYear();
                var month = ((nowDate.getMonth() + 1) < 9) ? '0' + (nowDate.getMonth() + 1) : (nowDate.getMonth() + 1);
                var day = (nowDate.getDate() < 9) ? '0' + nowDate.getDate() : nowDate.getDate();
                data.tarih = year + '-' + month + '-' + day;
                if (data.tespitid !== "0") {
                    tespitManager.update(data, function (result) {
                        isyeriManager.updateCalisanSayisiByIsyeriId(data, function () {
                            global.core.redirect(res, '/tespit');
                        });

                    });
                } else {
                    tespitManager.insert(data, function (result) {
                        isyeriManager.updateCalisanSayisiByIsyeriId(data, function () {
                            global.core.redirect(res, '/tespit');
                        });
                    });
                }
            });

        });
    },
    deleteptespit: function (req, res) {
        req.on('end', function () {
            var fields = ['tespitid'];

            global.core.getFormData(req.formData, fields, function (data) {
                tespitManager.delete(data.tespitid, function () {
                    global.core.returnJson(res, {status:'ok'});
                });
            });


        });
    }
};

module.exports = tespit;