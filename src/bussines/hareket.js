var setting = require('./../setting');
var hareketManager = require('./../DataAccess/hareket');
var uyeManager = require('./../DataAccess/uye');
var hareketRenderer = require('./../renderer/harket');
var isyeriManager = require('./../DataAccess/isyeri');
var temsilciManager = require('./../DataAccess/temsilci');
var subeTemsilciManager = require('./../DataAccess/subeTemsilci');


var hareketWork = {
    getHareket: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin){
            global.core.parseReq(req, function (qs) {
                hareketManager.getById(qs.hareketid, function (hareketItem) {
                    uyeManager.getById(qs.uyeid, function (uyeItem) {
                        if (hareketItem.length ==0){
                            isyeriManager.getAllListWithSubeInfo(function (isyeriList) {
                                var obj={
                                    user: userData,
                                    uye : uyeItem,
                                    hareket : null,
                                    isyeri: null,
                                    isyeriList: isyeriList
                                };
                                hareketRenderer.detay(req, res, obj);
                                return;
                            });
                        }else{
                            isyeriManager.getone(JSON.parse(hareketItem[0]).isyeriid, function (isyeriItem) {
                                isyeriManager.getAllListWithSubeInfo(function (isyeriList) {
                                    var obj = {
                                        user: userData,
                                        uye : uyeItem,
                                        hareket : hareketItem,
                                        isyeri: isyeriItem,
                                        isyeriList: isyeriList
                                    };
                                    hareketRenderer.detay(req, res, obj);
                                });
                            });
                        }

                    });
                });
            });
        });
    },
    checkforclevel:function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin){
            global.core.parseReq(req, function (qs) {
                temsilciManager.getOneByUyeid(qs.uyeid, function (result) {
                   res.end(''+JSON.stringify(result));
                });
            });
        });
    },
    checkforclevelsube: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin){
            global.core.parseReq(req, function (qs) {
                subeTemsilciManager.getOneByUyeid(qs.uyeid, function (result) {
                    res.end(''+JSON.stringify(result));
                });
            });
        });
    },
    deletemovementhareketid: function (req, res) {
        global.core.checkLogin(req, res, function (userData, isLogin) {
            global.core.parseReq(req, function (qs) {
                var hareketid = qs.hareketid;
                hareketManager.updateNullSonHareket(hareketid, function (nuller) {
                    if (nuller ===true){
                        hareketManager.deleteOneById(hareketid, function (result) {
                            res.end('' + result);
                        });
                    }else{
                        res.end(''+nuller);
                    }

                });

            });
        });
    }

};

module.exports = hareketWork;