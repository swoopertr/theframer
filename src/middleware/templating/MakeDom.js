var domGenerator = {
    generateSelect: function (enums, domid, defaultValue) {
        var domHtml = '<select id="' + domid + '" name="' + domid + '" style="width: auto;" class="span5">';
        domHtml += '<option value="0">Seçiniz</option>';
        for (var key in enums) {
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == key ? 'selected' : '' : '';
            domHtml += '<option value="' + key + '" ' + selected + '>' + enums[key] + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    },
    generateSelectTemsilciVazife: function (domid, defaultValue) {
        var arr = global.dataenums.enums.vazifeTemsilciArr;
        const arrLen = arr.length;
        var domHtml = '<select id="' + domid + '" name="' + domid + '" style="width: auto;" class="span5">';
        domHtml += '<option value="0">Seçiniz</option>';
        for (var i = 0 ;i<arrLen; i++){
            var item = arr[i];
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == item.key ? 'selected' : '' : '';
            domHtml += '<option value="' + item.key + '" ' + selected + '>' + item.val + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    },
    generateSelectTemsilciStatusu: function (domid, defaultValue){
        var arr='';//todo.. generate select html output.

    },
    generateIsyeriSelectArrayDefaultValue: function (arr, domid, defaultValue) {
        var domHtml = '<select id="' + domid + '" name="' + domid + '" style="width: auto;">';
        domHtml += '<option value="0">Seçiniz</option>';
        const len = arr.length;
        for (var i = 0; i < len; i++) {
            var item = JSON.parse(arr[i]);
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == item.isyeriid ? 'selected' : '' : '';
            domHtml += '<option value="' + item.isyeriid + '" ' + selected + ' >' + item.isim + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    },
    generateIsyeriSelectJsonArrayDefaultValue: function (arr, domid, defaultValue) {
        var domHtml = '<select id="' + domid + '" name="' + domid + '" style="width: auto;">';
        domHtml += '<option value="0">Seçiniz</option>';
        const len = arr.length;
        for (var i = 0; i < len; i++) {
            var item = arr[i];
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == item.isyeriid ? 'selected' : '' : '';
            domHtml += '<option value="' + item.isyeriid + '" ' + selected + ' >' + item.isyeriisim + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    },
    generateIsyeriWithSubeDataSelectArrayDefaultValue: function (arr, domid, defaultValue) {
        var domHtml = '<select id="' + domid + '" name="' + domid + '" style="width: auto;">';
        domHtml += '<option value="0">Seçiniz</option>';
        const len = arr.length;
        for (var i = 0; i < len; i++) {
            var item = JSON.parse(arr[i]);
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == item.isyeriid ? 'selected' : '' : '';
            domHtml += '<option value="' + item.isyeriid + '" ' + selected + ' >' + item.isyeriisim +' ('+ item.subeisim+')' + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    },
    generateSubeSelectArrayDefaultValue: function (arr, domid, defaultValue) {
        var domHtml = '<select id ="' + domid + '" name="' + domid + '">';
        domHtml += '<option value="0">Seçiniz</option>';
        const len = arr.length;
        for (var i = 0; i < len; i++) {
            var item = JSON.parse(arr[i]);
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == item.subeid ? 'selected' : '' : '';
            domHtml += '<option value="' + item.subeid + '" ' + selected + ' >' + item.isim + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    },
    generateSendikaSelectArrayDefaultValue: function (arr, domid, defaultValue) {
        var domHtml = '<select id ="' + domid + '" name="' + domid + '">';
        domHtml += '<option value="0">Seçiniz</option>';
        const len = arr.length;
        for (var i = 0; i < len; i++) {
            var item = JSON.parse(arr[i]);
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == item.sendikaid ? 'selected' : '' : '';
            domHtml += '<option value="' + item.sendikaid + '" ' + selected + ' >' + item.isim + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    },
    generateCalisanArrayDefaultValue: function (calisanlar, domid, defaultValue) {
        var domHtml = '<select id="' + domid + '" name="' + domid + '" style="width: auto;">';
        domHtml += '';
        const len = calisanlar.length;
        for (var i = 0; i < len; i++) {
            var item = JSON.parse(calisanlar[i]);
            const selected = (typeof defaultValue !== 'undefined') ? defaultValue == item.uyeid ? 'selected' : '' : '';
            domHtml += '<option value="' + item.uyeid + '" ' + selected + ' >' + item.isim + ' '+item.soyisim + '</option>';
        }
        domHtml += '</select>';
        return domHtml;
    }
};
module.exports = domGenerator;