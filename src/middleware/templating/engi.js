var setting = require('./../../setting');
var fs = require('fs');

//var dir = process.cwd();
var dir = setting.proj.Path;
var engine = {
    startWatch: function (cb) {
        console.log('watching folder : ' + dir + 'client');
        try{
            var opt = {
                recursive: true
            };
            fs.watch(dir + '/client', opt, function (eventType, filename) {
                if (!global.fsTimeout) {
                    global.fsTimeout = setTimeout(function () {
                        engine.init(function () {
                            global.fsTimeout = null;
                            console.log('html pages cache resetted');
                            cb && cb();
                        }, 5000);

                    });
                }
            });
        }catch (e) {
            console.log(e);
        }

    },
    init: function (cb) {
        global.temps = {};
        var funcArr = [];
        //start page
        funcArr.push(
            engine.homepage,
            engine.login);

        //education section
        funcArr.push(
            engine.homepageEducation,
            engine.educationCategory,
            engine.educationCategoryAddEdit,
            engine.educationDetail,
            engine.educationTeachers,
            engine.educationTeacherDetail,
            engine.educationItemList,
            engine.educationItemDetail,
            engine.educationCreate);

        //general section miscs
        funcArr.push(
            engine.footer,
            engine.header,
            engine.headerCss,
            engine.bottomScripts,
            engine.bottomScriptsDetay,
            engine.bottomScriptsHareketDetay);

        //sube section
        funcArr.push(
            engine.sube,
            engine.subeDetay,
            engine.subeEdit,
            engine.subeIsyeri,
            engine.subeIstatistik,
            engine.subeyonetimEx,
            engine.subeyonetimEdit,
            engine.subeGirisCikis,
            engine.subeGiriscikisisimli,
            engine.subetespit,
            engine.subeSendikalar,
            engine.subeTemsilciler,
            engine.subeUyeListesi);

        //uye section
        funcArr.push(
            engine.uyeler,
            engine.uyeDetay,
            engine.yeniuye);

        //hareket section
        funcArr.push(engine.hareketDetay);

        //isyeri section
        funcArr.push(
            engine.isyeri,
            engine.isyeriDetay,
            engine.isyeriEdit,
            engine.isyeriUyeler,
            engine.isyeriGirisCikis,
            engine.isyeriGirisCikisIsimli,
            engine.isyeriOrgut,
            engine.isyeriTespit,
            engine.tespitdetay,
            engine.isyeriTemsilci,
            engine.isyeriTemsilciEdit,
            engine.isyeriisverentemsilcileri);

        //tespit
        funcArr.push(engine.tespit);

        //genelraporlar
        funcArr.push(
            engine.genelraporlarsube,
            engine.genelraporlaristatistik,
            engine.genelraporlarisimligiriscikis,
            engine.genelraporlargiriscikis,
            engine.genelraporlarkayitlistesi,
            engine.genelraporlarkayitdefteri,
            engine.genelraporlarorgbaslangic,
            engine.genelraporlaruyeliklistesi,
            engine.genelraporlarsonhareketler);


        //sendika
        funcArr.push(
            engine.sendikalar,
            engine.sendikadetay,
            engine.sendikatespit);

        global.core.callMethods(funcArr, 0, function () {
            engine.pre.init(cb);
        });
    },
    render: function (req, res, html, cb) {
        res.writeHead(200, setting.TheHeaderHtml);
        html = html.replace(new RegExp('{{obj.headerCss}}', 'g'), global.temps.headerCss);
        html = html.replace(new RegExp('{{obj.header}}', 'g'), global.temps.header);
        html = html.replace(new RegExp('{{obj.footer}}', 'g'), global.temps.footer);
        html = html.replace(new RegExp('{{obj.bottomscripts}}', 'g'), global.temps.bottomScripts);
        html = html.replace(new RegExp('{{obj.bottomscriptsdetay}}', 'g'), global.temps.bottomScriptsDetay);

        res.write(html);
        res.end();
        cb && cb (html);
    },
    header: function (cb) {
        fs.readFile(dir + '/client/partial/header.tmp', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.header = content;
            cb && cb();
        });
    },
    headerCss: function (cb) {
        fs.readFile(dir + '/client/partial/headerCss.tmp', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.headerCss = content;
            cb && cb();
        });
    },
    footer: function (cb) {
        fs.readFile(dir + '/client/partial/footer.tmp', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.footer = content;
            cb && cb();
        });
    },
    bottomScripts: function (cb) {
        fs.readFile(dir + '/client/partial/bottomscripts.tmp', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.bottomScripts = content;
            cb && cb();
        });
    },
    bottomScriptsDetay: function (cb) {
        fs.readFile(dir + '/client/partial/bottomscriptsdetay.tmp', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.bottomScriptsDetay = content;
            cb && cb();
        });
    },
    bottomScriptsHareketDetay: function (cb) {
        fs.readFile(dir + '/client/partial/bottomhareketdetayscripts.tmp', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.bottomScriptsHareketDetay = content;
            cb && cb();
        });
    },
    homepage: function (cb) {
        fs.readFile(dir + '/client/index.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.homepage = content;
            cb && cb();
        });
    },
    homepageEducation: function (cb) {
        fs.readFile(dir + '/client/egitim_index.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.homepageEducation = content;
            cb && cb();
        });
    },
    educationCategory: function (cb) {
        fs.readFile(dir + '/client/egitimtipleri.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationCategoryList = content;
            cb && cb();
        });
    },
    educationCreate: function (cb) {
        fs.readFile(dir + '/client/egitimolustur.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationCreate = content;
            cb && cb();
        });
    },
    educationCategoryAddEdit: function(cb){
        fs.readFile(dir + '/client/egitimtipleriEdit.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationCategoryAddEdit = content;
            cb && cb();
        });
    },
    educationDetail: function (cb){
        //egitimtipiDetay
        fs.readFile(dir + '/client/egitimtipiDetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationCategoryDetail = content;
            cb && cb();
        });
    },
    educationTeachers: function (cb){
        //öğretmenler
        fs.readFile(dir + '/client/egitimciler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationTeachers = content;
            cb && cb();
        });
    },
    educationTeacherDetail: function (cb){
        //egitimcidetay
        fs.readFile(dir + '/client/egitimcidetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationTeacherDetail = content;
            cb && cb();
        });
    },
    educationItemList: function (cb){
        fs.readFile(dir + '/client/egitimler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationItemList = content;
            cb && cb();
        });
    },
    educationItemDetail: function (cb){
        fs.readFile(dir + '/client/egitimDetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.educationItemDetail = content;
            cb && cb();
        });
    },
    login: function (cb) {
        fs.readFile(dir + '/client/m_login.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.login = content;
            cb && cb();
        });
    },
    isyeri: function (cb) {
        fs.readFile(dir + '/client/isyeri.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeri = content;
            cb && cb();
        });
    },
    isyeriDetay: function (cb) {
        fs.readFile(dir + '/client/isyeridetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriDetay = content;
            cb && cb();
        });
    },
    isyeriEdit: function (cb) {
        fs.readFile(dir + '/client/isyeriedit.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriEdit = content;
            cb && cb();
        });
    },
    isyeriUyeler: function (cb) {
        fs.readFile(dir + '/client/isyeriuyeler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriUyeler = content;
            cb && cb();
        });
    },
    isyeriGirisCikis: function (cb) {
        fs.readFile(dir + '/client/isyerigiriscikis.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriGirisCikis = content;
            cb && cb();
        });
    },
    isyeriGirisCikisIsimli: function (cb) {
        fs.readFile(dir + '/client/isyerigiriscikisisimli.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriGirisCikisIsimli = content;
            cb && cb();
        });
    },
    isyeriOrgut: function (cb) {
        fs.readFile(dir + '/client/isyeriorgut.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriOrgut = content;
            cb && cb();
        });
    },
    isyeriTemsilci: function (cb) {
        fs.readFile(dir + '/client/isyeritemsilci.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriTemsilci = content;
            cb && cb();
        });
    },
    isyeriTemsilciEdit: function (cb) {
        fs.readFile(dir + '/client/isyeritemsilciedit.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeritemsilciedit = content;
            if (typeof cb === "function") {
                cb();
            }
        });
    },
    isyeriTespit: function (cb) {
        fs.readFile(dir + '/client/isyeritespit.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriTespit = content;
            cb && cb();
        });
    },
    sube: function (cb) {
        fs.readFile(dir + '/client/subeler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.sube = content;
            cb && cb();
        });
    },
    subeDetay: function (cb) {
        fs.readFile(dir + '/client/subedetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subeDetay = content;
            cb && cb();
        });
    },
    subeEdit: function (cb) {
        fs.readFile(dir + '/client/subeedit.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subeEdit = content;
            cb && cb();
        });
    },
    subeIsyeri: function (cb) {
        fs.readFile(dir + '/client/subeisyeri.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subeIsyeri = content;
            cb && cb();
        });
    },
    subeIstatistik: function (cb) {
        fs.readFile(dir + '/client/subeistatistik.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subeIstatistik = content;
            cb && cb();
        });
    },
    subeyonetimEx: function (cb) {
        fs.readFile(dir + '/client/subeyonetim.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subeyonetim = content;
            cb && cb();
        });
    },
    subeyonetimEdit: function (cb) {
        fs.readFile(dir + '/client/subeyonetimedit.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subeyonetimedit = content;
            cb && cb();
        });
    },
    subeGirisCikis: function (cb) {
        fs.readFile(dir + '/client/subegiriscikis.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subegiriscikis = content;
            cb && cb();
        });
    },
    subeGiriscikisisimli: function (cb) {
        fs.readFile(dir + '/client/subegiriscikisisimli.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subegiriscikisisimli = content;
            cb && cb();
        });
    },
    subetespit: function (cb) {
        fs.readFile(dir + '/client/subetespitler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subetespit = content;
            cb && cb();
        });
    },
    subeSendikalar: function (cb) {
        fs.readFile(dir + '/client/subesendikalar.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subesendika = content;
            cb && cb();
        });
    },
    subeTemsilciler: function (cb) {
        fs.readFile(dir + '/client/subetemsilciler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subetemsilciler = content;
            cb && cb();
        });
    },
    subeUyeListesi: function (cb) {
        fs.readFile(dir + '/client/subeuyelistesi.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.subeuyelistesi = content;
            cb && cb();
        });
    },
    uyeler: function (cb) {
        fs.readFile(dir + '/client/uyeler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.uyeler = content;
            cb && cb();
        });
    },
    uyeDetay: function (cb) {
        fs.readFile(dir + '/client/uyedetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.uyedetay = content;
            cb && cb();
        });
    },
    yeniuye: function (cb) {
        fs.readFile(dir + '/client/yeniuye.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.yeniuye = content;
            cb && cb();
        });
    },
    hareketDetay: function (cb) {
        fs.readFile(dir + '/client/hareketdetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.hareketdetay = content;
            cb && cb();
        });
    },
    tespit: function (cb) {
        fs.readFile(dir + '/client/tespitler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.tespitler = content;
            cb && cb();
        });
    },
    tespitdetay: function (cb) {
        fs.readFile(dir + '/client/tespitdetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.tespitdetay = content;
            cb && cb();
        });
    },
    genelraporlarsube: function (cb) {
        fs.readFile(dir + '/client/genelraporlarsube.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlarsube = content;
            cb && cb();
        });
    },
    genelraporlaristatistik: function (cb) {
        fs.readFile(dir + '/client/genelraporlaristatistik.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlaristatistik = content;
            cb && cb();
        });
    },
    genelraporlarisimligiriscikis: function (cb) {
        fs.readFile(dir + '/client/genelraporlargiriscikisisimli.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlarisimligiriscikis = content;
            cb && cb();
        });
    },
    genelraporlargiriscikis: function (cb) {
        fs.readFile(dir + '/client/genelraporlargiriscikis.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlargiriscikis = content;
            cb && cb();
        });
    },
    genelraporlarkayitlistesi: function (cb) {
        fs.readFile(dir + '/client/genelraporlarkayitlistesi.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlarkayitlistesi = content;
            cb && cb();
        });
    },
    genelraporlarkayitdefteri: function (cb) {
        fs.readFile(dir + '/client/genelraporlarkayitdefteri.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlarkayitdefteri = content;
            cb && cb();
        });
    },
    genelraporlarorgbaslangic: function (cb) {
        fs.readFile(dir + '/client/genelraporlarorgbaslangic.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlarorgbaslangic = content;
            cb && cb();
        });
    },
    genelraporlaruyeliklistesi: function (cb) {
        fs.readFile(dir + '/client/genelraporlaruyeliklistesi.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlaruyeliklistesi = content;
            cb && cb();
        });
    },
    genelraporlarsonhareketler: function (cb) {
        fs.readFile(dir + '/client/genelraporlarsonhareketler.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.genelraporlarsonhareketler = content;
            cb && cb();
        });
    },
    isyeriisverentemsilcileri: function (cb) {
        fs.readFile(dir + '/client/isyeriisverentemsilcileri.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.isyeriisverentemsilcileri = content;
            cb && cb();
        });
    },
    sendikalar: function (cb) {
        fs.readFile(dir + '/client/sendikalar.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.sendikalar = content;
            cb && cb();
        });
    },
    sendikadetay: function (cb) {
        fs.readFile(dir + '/client/sendikadetay.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.sendikadetay = content;
            cb && cb();
        });
    },
    sendikatespit: function (cb)    {
        fs.readFile(dir + '/client/sendikatespit.html', 'utf-8', function (err, content) {
            if (err) {
                console.log(err);
            }
            global.temps.sendikatespit = content;
            cb && cb();
        });
    },
    pre: {
        init: function (cb) {
            global.preRender = {};
            var funcArr = [];
            funcArr[0] = engine.pre.preUsermenu;
            funcArr[1] = engine.pre.preProjTitle;

            global.core.callMethods(funcArr, 0, function () {
                engine.pre.preRender(cb);
            });
        },
        preRender: function (cb) {
            for (var prop in global.temps) {
                global.temps[prop] = global.temps[prop].replace(new RegExp('{{pre.userMenu}}', 'g'), global.preRender.usermenu);
                global.temps[prop] = global.temps[prop].replace(new RegExp('{{obj.projTitle}}', 'g'), global.preRender.projTitle);
            }
            cb && cb();
        },
        preUsermenu: function (cb) {
            fs.readFile(dir + '/client/preload/usermenu.tmp', 'utf-8', function (err, content) {
                if (err) {
                    console.log(err);
                }
                global.preRender.usermenu = content;
                cb && cb();
            });
        },
        preProjTitle: function (cb) {
            global.preRender.projTitle = setting.proj.Title;
            cb && cb();
        }
    }
};
module.exports = engine;
