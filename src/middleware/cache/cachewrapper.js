//var cacheTool = require('./../../core/redisCache');
var setting = require('./../../setting');
//todo manage caching with configuration

var theAllCache={};
var cacheManager = {
  init: function (cb) {
    global.cache = {};
    global.cache.key = {};

    this.Set();
    this.Get();
    this.KeyforUserPermission();
    this.resetServerCache();
    cb && cb();
  },
  Get: function () {
    global.cache.get = function (key, cb) {
      cb && cb(theAllCache[key]);
      //cacheTool.get(key, cb);
    }
  },
  Set: function () {
    global.cache.set = function (key, value) {
      theAllCache[key] = value;
      //cacheTool.set(key, value);
    };
    global.cache.setEx = function (key, value) {
      theAllCache[key] = value;
      //cacheTool.setex(key, value);
    };
  },
  KeyforUserPermission: function () {
    global.cache.key.generateKeyForUserPermission = function (userid) {
      return userid + ':perm';
    };
  },
  resetServerCache: function () {
    global.cache.resetServer = function (cb) {
      theAllCache = {};
      cb && cb();
      /*
      cacheTool.send('FLUSHALL', function () {

        cb && cb();
      });
      */

    };
  }

};

module.exports = cacheManager;