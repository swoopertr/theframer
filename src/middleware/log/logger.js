var pg = require('./../../Work/Data/Postgre');
var logger = {
  init : function (cb) {
    //this.savelogDefinition();
    //this.saveErrDefinition();
    if (typeof cb === "function") {
      cb();
    }
  },
  savelogDefinition:function () {
    global.writeLog = function (log, user) {
      pg.query("INSERT INTO logger(\"logType\",description,createddate, \"UserId\") VALUES ('log', '" + log + "', DEFAULT,"+user.Id+");",function (data) {
        console.log('Log : ' + data);
      });
    }
  },
  saveErrDefinition:function () {
   global.writeErr = function(err,user){
     pg.query("INSERT INTO logger(\"logType\",description,createddate, \"UserId\") VALUES ('err', '" + JSON.stringify(err) + "', DEFAULT, " + user.Id + ");",function (data) {
      console.log(err + ' data:'+data);
     });
   }
  }
};
module.exports =logger;