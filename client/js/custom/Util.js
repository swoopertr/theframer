//generate select
function GenerateSelectByName(enums, domid, defaultValue) {
    var domHtml = '<select id="' + domid + '" name="' + domid + '" style="width: auto;" class="span5" ' + ((defaultValue) ? 'prevVal="' + defaultValue + '"' : '') + '>';
    domHtml += '<option value="0">Seçiniz</option>';
    for (var key in enums) {
        var selected = (typeof defaultValue !== 'undefined') ? defaultValue == key ? 'selected' : '' : '';
        domHtml += '<option value="' + key + '" ' + selected + '>' + enums[key] + '</option>';
    }
    domHtml += '</select>';
    return domHtml;
}

//set cookie by name
function setCookie(name, value, expires, path, domain, secure) {
    document.cookie = name + "=" + escape(value) +
        ((expires) ? "; expires=" + expires : "") +
        ((path) ? "; path=" + path : "") +
        ((domain) ? "; domain=" + domain : "") +
        ((secure) ? "; secure" : "");
}

//read cookie by name
function getCookie(name) {
    var cookie = " " + document.cookie;
    var search = " " + name + "=";
    var setStr = null;
    var offset = 0;
    var end = 0;
    if (cookie.length > 0) {
        offset = cookie.indexOf(search);
        if (offset != -1) {
            offset += search.length;
            end = cookie.indexOf(";", offset)
            if (end == -1) {
                end = cookie.length;
            }
            setStr = unescape(cookie.substring(offset, end));
        }
    }
    return (setStr);
}

//delete cookie by name
function delCookie(name) {
    document.cookie = name + "=" + "; expires=Thu, 01 Jan 1970 00:00:01 GMT";
}

//is js scripts loaded
function isScriptAlreadyIncluded(src) {
    var scripts = document.getElementsByTagName("script");
    for (var i = 0; i < scripts.length; i++)
        if (scripts[i].getAttribute('src') == src) return true;
    return false;
}

//load js scripts dynamically
function loadScriptAsync(url, cb, async) {
    try {
        if (isScriptAlreadyIncluded(url)) {
            cb();
        } else {
            var script = document.createElement("script");
            script.type = "text/javascript";
            script.async = (typeof async === "undefined") ? false : async;
            if (script.readyState) {
                script.onreadystatechange = function () {
                    if (script.readyState == "loaded" || script.readyState == "complete") {
                        script.onreadystatechange = null;
                        if (cb && typeof cb === "function") {
                            cb();
                        }
                    }
                };
            } else {
                script.onload = function () {
                    if (cb && typeof cb === "function") {
                        cb();
                    }
                };
            }
            script.src = url;
            (document.getElementsByTagName('head')[0] || document.getElementsByTagName('body')[0]).appendChild(script);
        }
    } catch (e) {
        console.log(e);
    }
}

//reads querystring parameters
function getParameterByName(name, url) {
    if (!url) url = window.location.href;
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function handleInput(e) {
    var ss = e.target.selectionStart;
    var se = e.target.selectionEnd;
    e.target.value = e.target.value.turkishToUpper();
    e.target.selectionStart = ss;
    e.target.selectionEnd = se;
}

String.prototype.turkishToUpper = function(){
    var string = this;
    var letters = { "i": "İ", "ş": "Ş", "ğ": "Ğ", "ü": "Ü", "ö": "Ö", "ç": "Ç", "ı": "I" };
    string = string.replace(/(([iışğüçö]))/g, function(letter){ return letters[letter]; })
    return string.toUpperCase();
}

String.prototype.turkishToLower = function(){
    var string = this;
    var letters = { "İ": "i", "I": "ı", "Ş": "ş", "Ğ": "ğ", "Ü": "ü", "Ö": "ö", "Ç": "ç" };
    string = string.replace(/(([İIŞĞÜÇÖ]))/g, function(letter){ return letters[letter]; })
    return string.toLowerCase();
}

var isLeapYear = function (year) {
    return (((year % 4 === 0) && (year % 100 !== 0)) || (year % 400 === 0));
};

var getDaysInMonth = function (year, month) {
    return [31, (isLeapYear(year) ? 29 : 28), 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][month];
};



var months_tr=['OCAK', 'ŞUBAT', 'MART', 'NİSAN', 'MAYIS', 'HAZİRAN', 'TEMMUZ', 'AĞUSTOS', 'EYLÜL', 'EKİM', 'KASIM', 'ARALIK'];
var weekdays_tr=['PAZAR', 'PAZARTESİ','SALI', 'ÇARŞAMBA', 'PERŞEMBE', 'CUMA','CUMARTESİ'];

var dateToString_tr= function (theDate){
    var result = '';    
    if (theDate){
        //ok
    }else{  
        theDate=new Date();
    }
    var year = theDate.getFullYear();
    var month_tr = months_tr[theDate.getMonth()];
    var day_tr = weekdays_tr[theDate.getDay()];
    var dateDay = theDate.getDate() 
    result = day_tr + ', ' + dateDay + ' ' + month_tr + ' ' + year;
    return result;
}

var openInNewTab = function (url) {
    Object.assign(document.createElement('a'), {target: '_blank', href: url}).click();
};

var Sendika = {
    checkScriptLoaded: isScriptAlreadyIncluded,
    loadScriptAsync: loadScriptAsync,
    openInNewTab: openInNewTab,
    makeUpper: handleInput,
    getQSbyName: getParameterByName,
    dateToString_tr:dateToString_tr,
    log: function (log) {
        console.log(log);
    },
    cookies: {
        get: function (key) {
            return getCookie(key);
        },
        set: function (key, value) {
            setCookie(key, value);
        },
        delete: function (key) {
            delCookie(key);
        }
    },
    Enums: {
        paging: {
            NoPaging: 0,
            Previous: 1,
            Next: 2
        },
        vazife: {
            1: 'Baskan',
            2: 'sekreter',
            3: 'MaliSekreter',
            4: 'OrgutlenmeSekreteri',
            5: 'EgitimSekreteri',
            23: 'BasYaySek',
            24: 'SosyalIsSek',
            6: 'YKYdk1',
            7: 'YKYdk2',
            8: 'YKYdk3',
            9: 'YKYdk4',
            10: 'YKYdk5',
            25: 'YKYdk6',
            26: 'YKYdk7',
            11: 'DenK1',
            12: 'DenK2',
            13: 'DenK3',
            14: 'DenKYdk1',
            15: 'DenKYdk2',
            16: 'DenKYdk3',
            17: 'DisK1',
            18: 'DisK2',
            19: 'DisK3',
            20: 'DisKYdk1',
            21: 'DisKYdk2',
            22: 'DisKYdk3'
        },
        vazifeTemsilciler: {
            1: 'BasTemsilci',
            2: 'Temsilci',
            15: 'Temsilci2',
            16: 'Temsilci3',
            17: 'Temsilci4',
            18: 'Temsilci5',
            19: 'Temsilci6',
            3: 'Disiplin1',
            4: 'Disiplin2',
            5: 'DisiplinY1',
            6: 'DisiplinY2',
            7: 'Izin_Kurulu1',
            8: 'ISG_Kurulu1',
            9: 'Izin_Kurulu2',
            10: 'Izin_KuruluY1',
            11: 'Izin_KuruluY2',
            12: 'ISG_Kurulu2',
            13: 'ISG_KuruluY1',
            14: 'ISG_KuruluY2'
        },
        hareketCinsi: {
            1: 'Kayıt',
            2: 'İstifa',
            3: 'Madde 17',
            4: 'Madde 25',
            5: 'Emekli',
            6: 'İhraç',
            7: 'Kapsam Dışı'
        },
        orgutDurum: {
            1: 'Devam',
            2: 'İstifa',
            3: 'Kapandı',
            4: 'Karşı Sendika',
            5: 'Örgütsüz',
            6: 'İşlem Dışı',
            7: 'Dosya Kalktı',
            8: 'Örgütleniyor',
            9: 'Mahkemede'
        },
        tasaron: {
            1: 'Yok',
            2: 'Var'
        },
        status: {
            0: 'Pasif',
            1: 'Aktif'
        },
        cinsiyet :{
            1: 'Erkek',
            2: 'Kadın'
        }
    }
};