
$(document).ready(function(){
	
	$('input[type=checkbox],input[type=radio],input[type=file]').uniform();
	
	$('select').select2();
    $('.colorpicker').colorpicker();

    var currentDate = new Date();
    $('.datepicker').datepicker("setValue", currentDate);
});
