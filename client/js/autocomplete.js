function autocomplete(inp, arr, prop, idField, maxItemCount) {
    var currentFocus;
    if (typeof maxItemCount === 'undefined'){
        maxItemCount = 5;
    }

    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        closeAllLists();
        if (!val) { return false;}
        currentFocus = -1;
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");

        this.parentNode.appendChild(a);

        for (i = 0; i < arr.length; i++) {

            if (arr[i][prop].substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                if (maxItemCount < document.getElementById(inp.id+'autocomplete-list').getElementsByTagName('div').length){
                    continue;
                }

                b = document.createElement("DIV");
                b.innerHTML = "<strong>" + arr[i][prop].substr(0, val.length) + "</strong>";
                b.innerHTML += arr[i][prop].substr(val.length);
                b.innerHTML += "<input type='hidden' selectid='"+arr[i][idField]+"' value='" + arr[i][prop] + "'>";
                b.addEventListener("click", function(e) {
                    inp.value = this.getElementsByTagName("input")[0].value;
                    inp.setAttribute('selectedid', this.getElementsByTagName("input")[0].getAttribute('selectid'));
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });

    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {// down
            currentFocus++;
            addActive(x);
        } else if (e.keyCode == 38) {// up
            currentFocus--;
            addActive(x);
        } else if (e.keyCode == 13) {// enter
            e.preventDefault();
            if (currentFocus > -1) {
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        if (!x) return false;
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }

    document.addEventListener("click", function (e) {
        closeAllLists(e.target);
    });
}