var setting = require('./src/setting');
var http  = require('http');
var RequestHandler = require('./src/Work/WebHandler');
var enums = require('./src/DataAccess/Const/Enums');
var defineCache = require('./src/middleware/cache/cachewrapper');
var templateStorage = require('./src/middleware/templating/engi');
var logEngine = require('./src/middleware/log/logger');


//define core functions
var core = require('./src/core/core');
core.init(function () {
  //data enums
  enums.init(function () {
    //cache
    defineCache.init(function () {
      //clean old caches
      global.cache.resetServer(function () {
        //template storage
        templateStorage.init(function () {
          templateStorage.startWatch();
          //logging
          logEngine.init( function () {

            //todo cluster forking needed ==> cluster made by pm2
            http.createServer( function (req, res) {
              RequestHandler.handler(req, res);
            }).listen(setting.ServerPort);
            console.log('server start :) go => http://localhost:' + setting.ServerPort);

            setTimeout(async function (){
             /* var url = await ngrok.connect(setting.ngrok);
              var mailContent = 'link : ' + url+ '\n';
              mailContent += 'Username : ' + setting.ngrokUser.username +'\n';
              mailContent += 'Password : ' + setting.ngrokUser.password +'\n';
              console.log('ngrok url: '+ url);
*/
              //global.core.sendMailer("tunc.kiral@gmail.com","Program başladı", mailContent);

            }, 5000);

          });

        });
      });
    });
  });
});

process.on('exit',async function(code){
  //await ngrok.disconnect();
  //await ngrok.kill();
});

/*
process.on('uncaughtException',function (err, detail) {
  console.log({err});
  global.core.sendMailer('tunc.kiral@gmail.com', 'sendika programında hata oldu', 'hata içeriği : ' + err.stack , async function () {
    //await ngrok.disconnect();
    //await ngrok.kill();
    process.exitCode=1;
    process.exit();
  });
});
*/

